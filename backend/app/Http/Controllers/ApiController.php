<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

/**
 * @SWG\Swagger(
 *     schemes={"http","https"},
 *     basePath="/api/v1",
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="API Documentation",
 *         description="This is a sample description for current API",
 *         termsOfService="",
 *         @SWG\Contact(
 *             email="hello@eyesimple.us"
 *         ),
 *         @SWG\License(
 *             name="Private License",
 *             url="http://eyesimple.us"
 *         )
 *     ),
 *     @SWG\ExternalDocumentation(
 *         description="Find out more about us",
 *         url="http://eyesimple.us"
 *     )
 * )
 */

class ApiController extends Controller
{
	/**
	 * @SWG\Get(
	 *   path="/getUsers",
	 *   summary="Display users from database",
	 *   description="-",
	 *   tags={"user"},
	 *   @SWG\Response(
	 *     response=200,
	 *     description="A list with users",
	 *	   @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/User")
     *     ),
	 *   ),
	 *   @SWG\Response(
	 *     response="400",
	 *     description="Invalid Id value"
	 *   )
	 * )
	 */
    public function getUsers(){
		$users = \App\User::all();
		return $users;
	}
	
	/**
	 * @SWG\Get(
	 *   path="/getUserInfo",
	 *   summary="Display user info from database",
	 *   description="-",
	 *   tags={"user"},
	 *   @SWG\Parameter(
     *       name="id",
     *       in="query",
     *       description="Id to filter by",
     *       required=true,
     *       type="integer",
	 *       format="int64",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
	 *   @SWG\Response(
	 *     response=200,
	 *     description="Getting user info",
	 *	   @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/User")
     *     ),
	 *   ),
	 *   @SWG\Response(
	 *     response="400",
	 *     description="Invalid Id value"
	 *   )
	 * )
	 */
	public function getUserInfo(Request $request){
		$user = \App\User::find($request['id']);
		return $user;
	}
	
	/**
	 * @SWG\Post(
	 *   path="/updateUserInfo",
	 *   summary="Updating user info from application",
	 *   description="-",
	 *   tags={"user"},
	 *   @SWG\Parameter(
     *       name="user_id",
     *       in="formData",
     *       description="User ID to update",
     *       required=true,
     *       type="integer",
	 *       format="int64",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
	 *   @SWG\Parameter(
     *       name="user_data",
     *       in="formData",
     *       description="User data to update. Field name must be the same with the one on database.",
     *       required=true,
     *       type="array",
	 *       format="string",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
	 *   @SWG\Response(
	 *     response=200,
	 *     description="User data has been updated",
	 *	   @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/User")
     *     ),
	 *   ),
	 *   @SWG\Response(
	 *     response="400",
	 *     description="Invalid Id value"
	 *   )
	 * )
	 */
	public function updateUserInfo(Request $request){
		// $user = \App\User::find($request['id']);
		
		// print_r($request);die;
		
		// print_r($request['user_data'][0]);
		
		// list($key, $val) = explode(',', $request['user_data']);
		// $arr = array($key => $val);
		
		return $request['user_data'];
	}
}
