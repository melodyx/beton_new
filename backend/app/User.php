<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Activitylog\Traits\LogsActivity;

class User extends Authenticatable
{
	use LogsActivity;
	protected static $logAttributes = ['name', 'email'];
	
	const ACTIVE = 'ACTIVE';
	const INACTIVE = 'INACTIVE';
	const PENDING = 'PENDING';
	const BANNED = 'BANNED';
	const SUSPENDED = 'SUSPENDED';
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
	
	public function medias(){
		return $this->hasMany('\App\Media', 'user_id');
	}
}
