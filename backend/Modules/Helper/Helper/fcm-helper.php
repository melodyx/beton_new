<?php


function pushNotification($to, $notificationTitle, $notificationBody, $data){
    $apiKey = 'AAAAzvvsirI:APA91bFh5D-qjiOnVLIlMUZ7MieBoN7FzIZ2imlP0P70VnYEs8vR-_ptsqJqNuUGHJgavffCMl8oeElVzmGJ0wqaDQgO7bGSbZvfgucKJiCE9S5yOqFdfq1quAA8v1bdXryr0lMb_Sw_FfsNMx9K-9kmCLCLua_4Ug';

    if($to){
        $notification = array();
        // Set notification title message
        if(empty($notificationTitle) && !is_string($notificationTitle)){
            $notification['title'] = '';
        }else{
            $notification['title'] = $notificationTitle;
        }
        // Set notification body message
        if(empty($notificationTitle) && !is_string($notificationTitle)){
            $notification['body'] = '';
        }else{
            $notification['body'] = $notificationBody;
        }
        // TODO: for now it's hardcoded to always use default sound
        $notification['sound'] = 'default';

        $fields = array
        (
            'notification' => $notification,
            'to' 	=> $to,
            'data'	=> $data,
        );


        $headers = array
        (
            'Authorization: key=' . $apiKey,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );
        curl_close( $ch );
        return $result;
    } else{
        return false;
    }

}
