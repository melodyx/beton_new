<?php

namespace Modules\Helper\Http\Controllers;

use Nwidart\Modules\Routing\Controller;

class HelperController extends Controller {

	public function index()
	{
		return view('helper::index');
	}

}
