@extends('layouts.app')
@section('header')

    @parent
	
	<?php $success_message = session('success'); if($success_message){?>
		<script>
		jQuery(document).ready(function(){
			alert('<?php echo $success_message; ?>');
		});
		</script>
	<?php session()->forget('success'); } ?>
	

@endsection
@section('content')
				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Datatables</span> - Customers</h4>
						</div>

					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="{{route('dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
							<li class="active">Customers</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->
				<div class="content">
					<div class="row">
						<div class="panel panel-flat">
						<table class="table datatable-basic">
						<thead>
							<tr>
								<th style="text-align:center;">Customer ID</th>
								<th style="text-align:center;">Transaction ID</th>
								<th style="text-align:center;">Person No</th>
								<th style="text-align:center;">Name</th>
								<th style="text-align:center;">Address</th>
								<th style="text-align:center;">Phone</th>
								<th style="text-align:center;">Status</th>
								<th style="text-align:center;">Action</th>
							</tr>
						</thead>
						<tbody>	
							<?php if(isset($customers)){ ?>
								<?php foreach($customers as $customer){?>
								<tr>
									<td style="text-align:center;"><?php if($customer->id){echo $customer->id;}else{ echo '-'; } ?></td>
									<td style="text-align:center;"><?php if($customer->transactionid){echo $customer->transactionid;}else{ echo '-'; } ?></td>
									<td style="text-align:center;"><?php if(isset($customer->personno)){echo $customer->personno;}else{ echo '-'; } ?></td>
									<td style="text-align:center;"><?php if(isset($customer->name)){echo $customer->name;}else{ echo '-'; } ?></td>
									<td style="text-align:center;"><?php if(isset($customer->addressline1)){echo $customer->addressline1; echo $customer->city? ' - '.$customer->city:'';}else{ echo '-'; } ?></td>
									<td style="text-align:center;"><?php if(isset($customer->phone)){echo $customer->phone;}else{ echo '-'; } ?></td>
									<td style="text-align:center;"><?php if(isset($customer->customertypeid)){echo $customer->customertypeid;}else{ echo '-'; } ?></td>
									<td>
										<?php if($current_user_role == 'sales') { ?>
											<ul class="icons-list">
												<li >
													<a href="#" class="dropdown-toggle" data-toggle="dropdown">
														<i class="icon-cog7"></i>
														<span class="caret"></span>
													</a>
													<ul class="dropdown-menu dropdown-menu-right">
														<li class="dropdown-header">Options</li>
														<li><a class="change-priority" href="{{Route('customers.editPriority',$customer->id)}}""><i class="icon-pencil7"></i>Change Priority</a></li>
													</ul>
												</li>
											</ul>
										<?php }else{?>
											<ul class="icons-list">
												<li >
													<a href="#" class="dropdown-toggle" data-toggle="dropdown">
														<i class="icon-cog7"></i>
														<span class="caret"></span>
													</a>
													<ul class="dropdown-menu dropdown-menu-right">
														<li class="dropdown-header">Options</li>
														<li><a href="{{route('customers.edit',$customer->id)}}"><i class="icon-pencil7"></i>Edit entry</a></li>
														<li><a href="{{route('customers.editAddress',$customer->id)}}"><i class="icon-pencil7"></i>Edit address</a></li>
														<li><a href="{{ route('customers.destroy', $customer->id)}}" data-method="delete" data-token="{{ csrf_token() }}"><i class="icon-bin"></i>Remove entry</a></li>
													</ul>
												</li>
											</ul>
										<?php } ?>
									</td>
								</tr>
								<?php } ?>
							<?php } ?>
						</tbody>
						</table>	
						</div>
					</div>
				</div>

@endsection