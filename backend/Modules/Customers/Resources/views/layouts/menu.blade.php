<?php $user = \Sentinel::getUser(); ?>
<?php if($user != \Sentinel::inRole('sales')){ ?>
<li>
	<a><i class="icon-users"></i> <span>Customers</span></a>
	<ul>
			<li><a href="{{route('customers.index')}}">Customers view</a></li>
			<li><a href="{{route('customers.create')}}">Add Customer</a></li>
			<li><a href="{{route('customers.uploadCustomers')}}">Upload Customer</a></li>
	</ul>
</li>
<?php } ?>