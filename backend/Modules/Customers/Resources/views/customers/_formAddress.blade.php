<?php
	use \Illuminate\Support\Facades\Input;
	if(!isset($method)){
		$method = 'post';
	}

?>
<script type="text/javascript" src="<?php echo asset('assets/js/plugins/forms/styling/uniform.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo asset('assets/js/pages/form_layouts.js') ?>"></script>


{{ Form::open(array('action' => $action, 'method' => $method, 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) }}
<fieldset class="content-group">
							<legend class="text-bold">Billing Address</legend>
								<div class="form-group">
									<label class="control-label col-lg-2">Destination Name</label>
									<div class="col-lg-10">
										{{Form::text('billing_destination_name', isset($customerBillingAddress->destination_name)? $customerBillingAddress->destination_name:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Email Address</label>
									<div class="col-lg-10">
										{{Form::text('billing_email', isset($customerBillingAddress->email)? $customerBillingAddress->email:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Phone</label>
									<div class="col-lg-10">
										{{Form::text('billing_phone', isset($customerBillingAddress->phone)? $customerBillingAddress->phone:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Address</label>
									<div class="col-lg-10">
										{{Form::text('billing_address', isset($customerBillingAddress->address)? $customerBillingAddress->address:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">City</label>
									<div class="col-lg-10">
										{{Form::text('billing_city', isset($customerBillingAddress->city)? $customerBillingAddress->city:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">State</label>
									<div class="col-lg-10">
										{{Form::text('billing_state', isset($customerBillingAddress->state)? $customerBillingAddress->state:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Country</label>
									<div class="col-lg-10">
										{{Form::text('billing_country', isset($customerBillingAddress->country)? $customerBillingAddress->country:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Zip code</label>
									<div class="col-lg-10">
										{{Form::number('billing_zip_code', isset($customerBillingAddress->zip_code)? $customerBillingAddress->zip_code:null, array('class' => 'form-control'))}}
									</div>
								</div>

								<legend class="text-bold">Shipping Address</legend>
								<div class="form-group">
									<label class="control-label col-lg-2">Destination Name</label>
									<div class="col-lg-10">
										{{Form::text('shipping_destination_name', isset($customerShippingAddress->destination_name)? $customerShippingAddress->destination_name:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Email Address</label>
									<div class="col-lg-10">
										{{Form::text('shipping_email', isset($customerShippingAddress->email)? $customerShippingAddress->email:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Phone</label>
									<div class="col-lg-10">
										{{Form::text('shipping_phone', isset($customerShippingAddress->phone)? $customerShippingAddress->phone:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Address</label>
									<div class="col-lg-10">
										{{Form::text('shipping_address', isset($customerShippingAddress->address)? $customerShippingAddress->address:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">City</label>
									<div class="col-lg-10">
										{{Form::text('shipping_city', isset($customerShippingAddress->city)? $customerShippingAddress->city:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">State</label>
									<div class="col-lg-10">
										{{Form::text('shipping_state', isset($customerShippingAddress->state)? $customerShippingAddress->state:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Country</label>
									<div class="col-lg-10">
										{{Form::text('shipping_country', isset($customerShippingAddress->country)? $customerShippingAddress->country:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Zip code</label>
									<div class="col-lg-10">
										{{Form::number('shipping_zip_code', isset($customerShippingAddress->zip_code)? $customerShippingAddress->zip_code:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="text-right">
									<button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
								</div>
						</fieldset>
{{ Form::close() }}