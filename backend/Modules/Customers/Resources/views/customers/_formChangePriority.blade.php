<?php
	use \Illuminate\Support\Facades\Input;
	if(!isset($method)){
		$method = 'post';
	}

?>
<script type="text/javascript" src="<?php echo asset('assets/js/plugins/forms/styling/uniform.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo asset('assets/js/pages/form_layouts.js') ?>"></script>


{{ Form::open(array('action' => $action, 'method' => $method, 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) }}
<fieldset class="content-group">
							<legend class="text-bold"><?php if(isset($customer)){ echo 'Edit customer'; }else{ echo 'Create customer';}?></legend>
  								<div class="form-group">
  									<label class="control-label col-lg-2">Priority</label>
									<div class="col-lg-10">
										{{Form::select('customerupdatetypeid', $priority,$customer->customertypeid, array('style' => 'display:inline-block;', 'class' => 'form-control'))}}
									</div>
  								</div>
  								<div class="form-group">
  									<label class="control-label col-lg-2">Bonus</label>
									<div class="col-lg-10">
										{{Form::number('bonus', null, array('style' => 'display:inline-block;', 'class' => 'form-control discount'))}} %
									</div>
  								</div>
								<div class="text-right">
									<button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
								</div>
						</fieldset>
{{ Form::close() }}