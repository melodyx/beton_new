<?php
	use \Illuminate\Support\Facades\Input;
	if(!isset($method)){
		$method = 'post';
	}

?>
<script type="text/javascript" src="<?php echo asset('assets/js/plugins/forms/styling/uniform.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo asset('assets/js/pages/form_layouts.js') ?>"></script>


{{ Form::open(array('action' => $action, 'method' => $method, 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) }}
<fieldset class="content-group">
							<legend class="text-bold"><?php if(isset($customer)){ echo 'Edit customer'; }else{ echo 'Create customer';}?></legend>
								<div class="form-group">
									<label class="control-label col-lg-2">Transaction id</label>
									<div class="col-lg-10">
										{{Form::number('transactionid', isset($customer->transactionid)? $customer->transactionid:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Person No</label>
									<div class="col-lg-10">
										{{Form::text('personno', isset($customer->personno)? $customer->personno:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Person Type</label>
									<div class="col-lg-10">
										{{Form::text('persontype', isset($customer->persontype)? $customer->persontype:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Balance</label>
									<div class="col-lg-10">
										{{Form::text('balance', isset($customer->balance)? $customer->balance:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Email</label>
									<div class="col-lg-10">
										{{Form::text('email', isset($customer->email)? $customer->email:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Name</label>
									<div class="col-lg-10">
										{{Form::text('name', isset($customer->name)? $customer->name:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Address</label>
									<div class="col-lg-10">
										{{Form::text('addressline1', isset($customer->addressline1)? $customer->addressline1:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">City</label>
									<div class="col-lg-10">
										{{Form::text('city', isset($customer->city)? $customer->city:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">State Prov</label>
									<div class="col-lg-10">
										{{Form::text('stateprov', isset($customer->stateprov)? $customer->stateprov:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Zip Code</label>
									<div class="col-lg-10">
										{{Form::number('zipcode', isset($customer->zipcode)? $customer->zipcode:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Country</label>
									<div class="col-lg-10">
										{{Form::text('country', isset($customer->country)? $customer->country:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Phone</label>
									<div class="col-lg-10">
										{{Form::text('phone', isset($customer->phone)? $customer->phone:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Contact</label>
									<div class="col-lg-10">
										{{Form::text('contact', isset($customer->contact)? $customer->contact:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<?php if(isset($customer->customertypeid)){?>
									<div class="form-group">
										<label class="control-label col-lg-2">status</label>
										<div class="col-lg-10">
											<?php $row = array(\Modules\Customers\Entities\Customer::GENERAL => \Modules\Customers\Entities\Customer::GENERAL, \Modules\Customers\Entities\Customer::PRIORITY => \Modules\Customers\Entities\Customer::PRIORITY, \Modules\Customers\Entities\Customer::BLACKLIST => \Modules\Customers\Entities\Customer::BLACKLIST); ?>
											{{Form::select('updatetypeid',$row, isset($customer->customertypeid)?$customer->customertypeid:null, array('class' => 'form-control'))}}
										</div>
									</div>
								<?php } ?>
								<div class="text-right">
									<button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
								</div>
						</fieldset>
{{ Form::close() }}