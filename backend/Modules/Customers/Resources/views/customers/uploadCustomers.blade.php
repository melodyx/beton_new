@extends('layouts.app')

@section('header')
    @parent
    <!-- JS Extra for form -->
	<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBdmLJ80f1HUSAG0YxQthnZ2hd_p60nnT4&v=3.exp"></script>

 <script type="text/javascript" src="{{asset('assets/js/maps/google/basic/basic.js')}}"></script>
 <script type="text/javascript" src="{{asset('assets/js/maps/google/basic/geolocation.js')}}"></script>
 <script type="text/javascript" src="{{asset('assets/js/maps/google/basic/coordinates.js')}}"></script>
 <script type="text/javascript" src="{{asset('assets/js/maps/google/basic/click_event.js')}}"></script>

 
 <?php if(isset($email_validation)){ ?>
	<script>
	jQuery(document).ready(function(){
		alert('<?php echo $email_validation?>');
	});
	</script>
 <?php } ?>
 
 
 <script type="text/javascript" src="{{asset('assets/js/maps/google/markers/animation.js')}}"></script>
 <script type="text/javascript" src="{{asset('assets/js/maps/google/markers/symbols_predefined.js')}}"></script>
 <script type="text/javascript" src="{{asset('assets/js/maps/google/markers/symbols_custom.js')}}"></script>
@endsection
@section('content')
<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Datatables</span> - Basic</h4>
						</div>

					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="{{route('dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="{{route('customers.index')}}">Customers</a></li>
							<li class="active">Create</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->
				<div class="content">
				<div class="panel panel-flat">
				<div class="panel-body">
					@include('customers::customers._formUploadXml', ['action' => '\Modules\Customers\Http\Controllers\CustomersController@storeXml'])
				</div>
				</div>
				</div>
@endsection