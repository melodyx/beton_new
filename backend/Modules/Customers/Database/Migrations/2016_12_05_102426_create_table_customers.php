<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCustomers extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('transactionid');
            $table->string('personno',255);
            $table->string('persontype',255);
            $table->decimal('balance',14,2);
            $table->string('name',255);
            $table->longText('addressline1');
            $table->longText('addressline2');
            $table->string('city',255);
            $table->string('stateprov',255);
            $table->integer('zipcode');
            $table->string('country',255);
            $table->string('phone',255);
            $table->string('contact',255);
            $table->string('fax',255);
            $table->string('email',255);
            $table->string('webpage',255);
            $table->integer('suspended');
            $table->integer('billtoonly');
            $table->integer('tax1exemptionno');
            $table->integer('tax2exemptionno');
            $table->string('currencyid',255);
            $table->string('termsid',255);
            $table->integer('allowbackorders');
            $table->longText('notes');
            $table->string('taxtype',255);
            $table->longText('taxaddress1');
            $table->longText('taxaddress2');
            $table->longText('characterreserved1');
            $table->longText('characterreserved2');
            $table->longText('characterreserved3');
            $table->longText('characterreserved4');
            $table->longText('characterreserved5');
            $table->longText('characterreserved6');
            $table->longText('characterreserved7');
            $table->longText('characterreserved8');
            $table->longText('characterreserved9');
            $table->longText('characterreserved10');
            $table->integer('numericreserved1');
            $table->integer('numericreserved2');
            $table->integer('numericreserved3');
            $table->date('datereserved1');
            $table->date('datereserved2');
            $table->string('customertypeid',255);
            $table->longText('personmessage');
            $table->integer('printstatement');
            $table->integer('creditlimit');
            $table->string('pricelevel',255);
            $table->string('defaultdisc',255);
            $table->integer('creditlimitdays');
            $table->string('status',255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customers');
    }

}
