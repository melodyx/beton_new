<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCustomerTransactionAddress extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_transactions_address', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('customer_id');
            $table->string('destination_name',255);
            $table->string('email',255);
            $table->string('phone',255);
            $table->longText('address');
            $table->string('city',255);
            $table->string('state',255);
            $table->string('country',255);
            $table->integer('zip_code');
            $table->string('address_type',255);
            $table->string('ref_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customer_transactions_address');
    }

}
