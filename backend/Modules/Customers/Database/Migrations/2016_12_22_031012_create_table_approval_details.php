<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableApprovalDetails extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('approval_details', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('approval_id');
            $table->longText('description');
            $table->integer('disc_revision');
            $table->longText('change_reason');
            $table->integer('author_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('approval_details');
    }

}
