<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableApproval extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('approvals', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('customer_id');
            $table->string('priority',255);
            $table->string('priority_update',255);
            $table->longText('change_reason');
            $table->integer('gm_id');
            $table->integer('gm_approval');
            $table->longText('gm_reason');
            $table->integer('bm_id');
            $table->integer('bm_approval');
            $table->longText('bm_reason');
            $table->decimal('bonus',14,2);
            $table->decimal('bonus_update',14,2);
            $table->string('status',255);
            $table->integer('author');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('approvals');
    }

}
