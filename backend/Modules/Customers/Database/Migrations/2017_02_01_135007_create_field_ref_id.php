<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldRefId extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_transactions_address', function(Blueprint $table)
        {
            if (!Schema::hasColumn('customer_transactions_address', 'ref_id'))
            {
                $table->string('ref_id',255);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_transactions_address', function(Blueprint $table)
        {
            if (Schema::hasColumn('customer_transactions_address', 'ref_id'))
            {
                $table->dropColumn('ref_id');
            }

        });
    }

}
