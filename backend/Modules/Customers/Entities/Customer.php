<?php

namespace Modules\Customers\Entities;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{

	const UNKNOWN = 'unknown';
	const APPROVED = 'approved';
	const BLACKLIST = 'blacklist';
	const PRIORITY = 'Prioritas';
	const GENERAL = 'Umum';

    protected $fillable = [];
    protected $table = 'customers';
    /*public function customer(){
    	$this->belongsTo('\Modules\Users\Entities\User','user_id');
    }*/

    public function approval(){
    	return $this->hasMany('\Modules\Customers\Entities\Approval','customer_id');
    }

    public function orders(){
        return $this->hasMany('\Modules\Transactions\Entities\Order','customer_id');
    }

    public function invoices(){
        return $this->hasMany('\Modules\Invoices\Entities\Invoice','customer_id');
    }

    public function customerAddresses(){
        return $this->hasMany('\Modules\Customers\Entities\CustomerAddress','customer_id');
    }

    public static function saveCustomer($id, $data){
        
        \DB::beginTransaction();
        try {
            if(is_null($id)){
                $customer = new Customer();
                $customer->customertypeid = Customer::GENERAL;
                $customer->status = Customer::APPROVED;
            }else{
                $customer = Customer::find($id);
                $customer->customerupdatetypeid = $data['updatetypeid'];
            }
            
            $customer->transactionid = $data['transactionid'];
            $customer->personno = $data['personno'];
            $customer->persontype = $data['persontype'];
            $customer->balance = $data['balance'];
            $customer->email = $data['email'];
            $customer->name = $data['name'];
            $customer->addressline1 = $data['addressline1'];
            $customer->city = $data['city'];
            $customer->stateprov = $data['stateprov'];
            $customer->zipcode = $data['zipcode'];
            $customer->country = $data['country'];
            $customer->phone = $data['phone'];
            $customer->contact = $data['contact'];
            $customer->save();

        } catch (\Exception $e) {
            \DB::rollback();
            return redirect()->route('customers.index')->with('success',$e->getMessage());
        }
        \DB::commit();

        return $customer;
    }
}
