<?php

namespace Modules\Customers\Entities;

use Illuminate\Database\Eloquent\Model;

class ApprovalDetail extends Model
{
    protected $fillable = [];

    protected $table = 'approval_details';

    public function approval(){
    	return $this->belongsTo('\Modules\Customers\Entities\Approval','approval_id');
    }

    public function author(){
    	return $this->belongsTo('\Modules\Users\Entities\User','author_id');
    }
}
