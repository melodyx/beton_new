<?php

namespace Modules\Customers\Entities;

use Illuminate\Database\Eloquent\Model;

class CustomerAddress extends Model
{
	const BILLING = 'billing';
	const SHIPPING = 'shipping';

    protected $fillable = ['customer_id','destination_name','email','phone','address','city','state','country','zip_code','address_type'];
    protected $table = 'customer_transactions_address';

    public function customer(){
    	return $this->belongsTo('\Modules\Customers\Entities\Customer','customer_id');
    }

    public static function saveAddress($billingId,$shippingId,$data){
    	if(is_null($billingId)){
    		$customerBillingAddress = new CustomerAddress();
    		$customerShippingAddress = new CustomerAddress();
    		$customerBillingAddress->customer_id = $data['customer_id'];
    		$customerShippingAddress->customer_id = $data['customer_id'];
    		$customerBillingAddress->address_type = CustomerAddress::BILLING;
    		$customerShippingAddress->address_type = CustomerAddress::SHIPPING;
    	}else{
    		$customerBillingAddress = CustomerAddress::find($billingId);
    		$customerShippingAddress = CustomerAddress::find($shippingId);
    	}
    	
    	$customerBillingAddress->destination_name = $data['billing_destination_name'];
    	$customerBillingAddress->email = $data['billing_email'];
    	$customerBillingAddress->phone = $data['billing_phone'];
    	$customerBillingAddress->address = $data['billing_address'];
    	$customerBillingAddress->city = $data['billing_city'];
    	$customerBillingAddress->state = $data['billing_state'];
    	$customerBillingAddress->country = $data['billing_country'];
    	$customerBillingAddress->zip_code = $data['billing_zip_code'];
    	$customerBillingAddress->save();

    	$customerShippingAddress->destination_name = $data['shipping_destination_name'];
    	$customerShippingAddress->email = $data['shipping_email'];
    	$customerShippingAddress->phone = $data['shipping_phone'];
    	$customerShippingAddress->address = $data['shipping_address'];
    	$customerShippingAddress->city = $data['shipping_city'];
    	$customerShippingAddress->state = $data['shipping_state'];
    	$customerShippingAddress->country = $data['shipping_country'];
    	$customerShippingAddress->zip_code = $data['shipping_zip_code'];
    	$customerShippingAddress->save();

    	$addresses['billing_address'] = $customerBillingAddress;
    	$addresses['shipping_address'] = $customerShippingAddress;

    	return $addresses;
    }
}
