<?php

namespace Modules\Customers\Entities;

use Illuminate\Database\Eloquent\Model;

class Approval extends Model
{
	const UNAPPROVE = 'unapprove';
	const APPROVED = 'approved';
	const REJECTED = 'rejected';

    protected $fillable = [];

    public function customer(){
    	return $this->belongsTo('\Modules\Customers\Entities\Customer','customer_id');
    }

    public function author(){
    	return $this->belongsTo('\Modules\Users\Entities\User','author');
    }

    public function approvalDetails(){
        return $this->hasMany('\Modules\Customers\Entities\ApprovalDetail','approval_id');
    }


}
