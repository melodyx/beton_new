<?php

Route::group(['middleware' => 'web', 'namespace' => 'Modules\Customers\Http\Controllers'], function()
{
	//Route::get('/', 'CustomersController@index');
	Route::get('customers/uploadCustomers', 'CustomersController@uploadCustomers')->name('customers.uploadCustomers');
	Route::post('customers/storeXml', 'CustomersController@storeXml');
	Route::resource('customers','CustomersController');
	Route::get('customers/editPriority/{id}','CustomersController@editPriority')->name('customers.editPriority');
	Route::put('customers/storePriority/{id}','CustomersController@storePriority')->name('customers.storePriority');
	Route::get('customers/createAddress/{id}','CustomersController@createAddress')->name('customers.createAddress');
	Route::put('customers/storeAddress/{id}','CustomersController@storeAddress')->name('customers.storeAddress');
	Route::get('customers/editAddress/{id}','CustomersController@editAddress')->name('customers.editAddress');
	Route::put('customers/updateAddress/{billingId}/{shippingId}','CustomersController@updateAddress')->name('customers.updateAddress');

	Route::group(['middleware' => ['apiAuth'], 'prefix' => 'api/v1'], function() {
		Route::get('/customers/getAll', 'ApiController@getAll');
		Route::post('/customers/register', 'ApiController@register');
		Route::post('/customers/registerAddress', 'ApiController@registerAddress');
		Route::get('/customers/searchCustomer', 'ApiController@searchCustomer');
		Route::get('/customers/approvalChangeProgress', 'ApiController@approvalChangeProgress');
		Route::get('/customers/checkPriorityChangeProgress', 'ApiController@checkPriorityChangeProgress');
		Route::post('/customers/changePriority', 'ApiController@changePriority');
		Route::get('/customers/listCustomerNeedApproval', 'ApiController@listCustomerNeedApproval');
        Route::post('/customers/approveChange', 'ApiController@approveChange');
    });
});