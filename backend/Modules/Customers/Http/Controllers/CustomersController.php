<?php

namespace Modules\Customers\Http\Controllers;

use Nwidart\Modules\Routing\Controller;
use Modules\Users\Entities\User;
use Modules\Customers\Entities\Customer;
use Modules\Customers\Entities\CustomerAddress;
use Modules\Customers\Entities\Approval;
use Illuminate\Http\Request;

class CustomersController extends Controller {

	public function index()
	{
		//$sentinelsData = \Sentinel::findRoleBySlug('customer')->users()->get();
		
		/*$x = 0;
		foreach($sentinelsData as $sentinelData){
			$customers[$x]['userData'] = $sentinelData;
			$customers[$x]['details'] = CustomerDetail::where('user_id')->first();
			$x++;
		}*/
		//Checking role for access customer page
		$current_user_role = $this->checkRole();
		if($current_user_role == 'notLogin'){
			return redirect()->route('login')->with('success','you dont have any authorization for access this page');
		}
		//=====================================
		$customers = Customer::all();
		return view('customers::index',['customers' => $customers, 'current_user_role' => $current_user_role]);
	}

	public function create(){
		//Checking role for access customer page
		$current_user_role = $this->checkRole();
		if($current_user_role == 'notLogin'){
			return redirect()->route('login')->with('success','you dont have any authorization for access this page');
		}
		//=====================================

		return view('customers::customers.create',['current_user_role' => $current_user_role]);
	}

	public function store(Request $request){
		$data = $request->all();
		
		$customer = Customer::saveCustomer(null,$data);
		return redirect()->route('customers.createAddress',$customer->id)->with('success','success add new custome, please add billing and shipping address');
		
	}

	public function edit($id){
		//Checking role for access customer page
		$current_user_role = $this->checkRole();
		if($current_user_role == 'notLogin' || $current_user_role == 'sales'){
			return redirect()->route('login')->with('success','you dont have any authorization for access this page');
		}
		//=====================================

		$customer = Customer::find($id);

		return view('customers::customers.edit',['customer' => $customer, 'current_user_role' => $current_user_role]);
	}

	public function update($id, Request $request){
		$data = $request->all();
		//dd($data);
		$customer = Customer::saveCustomer($id,$data);

		return redirect()->route('customers.index')->with('success','success update customer');
	}

	public function destroy($id){
		$customer = Customer::find($id);
		$customer->delete();
		//update activity log
			$current_user = \Sentinel::getUser();
			$log = array(
				'desc' => $current_user->id.' deleting customer '.$customer->name
			);
			event(new \App\Events\UpdateData($customer, $log));

		 return array(
            'url' => route('customers.index'),
            'message' => 'Something went wrong while trying to remove the customer!'
        );
	}


	public function editPriority($id){
		//Checking role for access customer page
		$current_user_role = $this->checkRole();
		if($current_user_role == 'notLogin'){
			return redirect()->route('login')->with('success','you dont have any authorization for access this page');
		}
		//=====================================

		$customer = Customer::with(array('approval'=>function($query){
			$query->where('status', '=', Approval::UNAPPROVE);
		}))->find($id);
		
		if($customer->approval->count()){
			return redirect()->route('customers.index')->with('success','this customer still have unapprove change priority');
		}

		$priority = array(
			Customer::GENERAL => Customer::GENERAL,
			Customer::PRIORITY => Customer::PRIORITY,
			Customer::BLACKLIST => Customer::BLACKLIST
		);

		return view('customers::customers.changePriority',['priority' => $priority, 'customer' => $customer]);
	}

	public function storePriority($id, Request $request){
		\DB::beginTransaction();
		try {
			$data = $request->all();
			$customer = Customer::find($id);

			$approval = new Approval();
			$approval->customer_id = $customer->id;
			$approval->priority = $customer->customertypeid;
			$approval->priority_update = $data['customerupdatetypeid'];
			$approval->bonus = $data['bonus'];
			$approval->status = Approval::UNAPPROVE;
			$approval->save();
		}catch (\Exception $e){
			\DB::rollback();
			return redirect()->route('customers.index')->with('success','failed');
		}
		\DB::commit();

		return redirect()->route('customers.index')->with('success','success update customer');
	}

	public function uploadCustomers(){
		//Checking role for access customer page
		$current_user_role = $this->checkRole();
		if($current_user_role == 'notLogin' || $current_user_role == 'sales'){
			return redirect()->route('login')->with('success','you dont have any authorization for access this page');
		}
		//=====================================

		return view('customers::customers.uploadCustomers');
	}


	public function createAddress($id){
		//Checking role for access customer page
		$current_user_role = $this->checkRole();
		if($current_user_role == 'notLogin' || $current_user_role == 'sales'){
			return redirect()->route('login')->with('success','you dont have any authorization for access this page');
		}
		//=====================================
		return view('customers::customers.createAddress',['customer_id' => $id]);
	}

	public function storeAddress($id,request $request){
		$data = $request->all();
		$data['customer_id'] = $id;
		$addresses= CustomerAddress::saveAddress(null,null,$data);

		return redirect()->route('customers.index')->with('success','successfully add new address to customer');
	}

	public function editAddress($id){
		//Checking role for access customer page
		$current_user_role = $this->checkRole();
		if($current_user_role == 'notLogin' || $current_user_role == 'sales'){
			return redirect()->route('login')->with('success','you dont have any authorization for access this page');
		}
		//=====================================

		$customer = Customer::with('customerAddresses')->find($id);
		$customerBillingAddress = array();
		$customerShippingAddress = array();
		foreach($customer->customerAddresses as $address){
			if($address->address_type == CustomerAddress::BILLING){
				$customerBillingAddress = $address;
			}elseif($address->address_type == CustomerAddress::SHIPPING){
				$customerShippingAddress = $address;
			}
		}
		return view('customers::customers.editAddress',['customerBillingAddress' => $customerBillingAddress, 'customerShippingAddress' => $customerShippingAddress]);
	}

	public function updateAddress($billingId, $shippingId, request $request){
		$data = $request->all();
		$addresses= CustomerAddress::saveAddress($billingId,$shippingId,$data);
		return redirect()->route('customers.index')->with('success','successfully edit address customer');
	}

	public function storeXml(Request $request){

		\DB::beginTransaction();
		try{
			$data = $request->all();

			$xml=simplexml_load_file($data['xml_file']);
			$cust = $xml->TRANSACTIONS;

			foreach($cust->CUSTOMER as $val){
				$validationCheck = Customer::where('transactionid','=',$val->TRANSACTIONID)->first();
				if(!isset($validationCheck)){
					$customer = new Customer();
					$customer->transactionid = $val->TRANSACTIONID;
					$customer->personno = $val->PERSONNO;
					$customer->persontype = $val->PERSONTYPE;
					$customer->balance = $val->BALANCE;
					$customer->name = $val->NAME;
					$customer->addressline1 = $val->ADDRESSLINE1;
					$customer->addressline2 = $val->ADDRESSLINE2;
					$customer->city = $val->CITY;
					$customer->stateprov = $val->STATEPROV;
					$customer->zipcode = $val->ZIPCODE;
					$customer->country = $val->COUNTRY;
					$customer->phone = $val->PHONE;
					$customer->contact = $val->CONTACT;
					$customer->fax = $val->FAX;
					$customer->email = $val->EMAIL;
					$customer->webpage = $val->WEBPAGE;
					$customer->suspended = $val->SUSPENDED;
					$customer->billtoonly = $val->BILLTOONLY;
					$customer->tax1exemptionno = $val->TAX1EXEMPTIONNO;
					$customer->tax2exemptionno = $val->TAX2EXEMPTIONO;
					$customer->currencyid = $val->CURRENCYID;
					$customer->termsid = $val->TERMSID;
					$customer->allowbackorders = $val->ALLOWBACKORDERS;
					$customer->notes = $val->NOTES;
					$customer->taxtype = $val->TAXTYPE;
					$customer->taxaddress1 = $val->TAXADDRESS1;
					$customer->taxaddress2 = $val->TAXADDRESS2;
					$customer->characterreserved1 = $val->CHARACTERRESERVED1;
					$customer->characterreserved2 = $val->CHARACTERRESERVED2;
					$customer->characterreserved3 = $val->CHARACTERRESERVED3;
					$customer->characterreserved4 = $val->CHARACTERRESERVED4;
					$customer->characterreserved5 = $val->CHARACTERRESERVED5;
					$customer->characterreserved6 = $val->CHARACTERRESERVED6;
					$customer->characterreserved7 = $val->CHARACTERRESERVED7;
					$customer->characterreserved8 = $val->CHARACTERRESERVED8;
					$customer->characterreserved9 = $val->CHARACTERRESERVED9;
					$customer->characterreserved10 = $val->CHARACTERRESERVED10;
					$customer->numericeserved1 = $val->NUMERICERESERVED1;
					$customer->numericereserved2 = $val->NUMERICERESERVED2;
					$customer->numericereserved3 = $val->NUMERICERESERVED3;
					$customer->datereserved1 = $val->DATERESERVED1;
					$customer->datereserved2 = $val->DATERESERVED2;
					$customer->customertypeid = $val->CUSTOMERTYPEID;
					$customer->personmessage = $val->PERSONMESSAGE;
					$customer->printstatement = $val->PRINTSTATEMENT;
					$customer->creditlimit = $val->CREDITLIMIT;
					$customer->pricelevel = $val->PRICELEVEL;
					$customer->defaultdisc = $val->DEFAULTDISC;
					$customer->creditlimitdays = $val->CREDITLIMITDAYS;
					$customer->status = Customer::APPROVED;
					$customer->save();
				}
			}

			$customers = Customer::all();
			foreach($customers as $customerCheck){
				if($customerCheck->characterreserved1){
					$customerSearch = Customer::where('personno','=',$customerCheck->characterreserved1)->first();
					$addressSearch = CustomerAddress::where('customer_id','=',$customerCheck->id)->first();
					
					// when customer not exists on customer_transaction_address
					if(isset($customerSearch) && !isset($addressSearch)){
						$shippingAddress = new CustomerAddress();
						$shippingAddress->customer_id = $customerCheck->id;
						/*$shippingAddress->destination_name = $customerSearch->name;
						$shippingAddress->email = $customerSearch->email;
						$shippingAddress->phone = $customerSearch->phone;
						$shippingAddress->address = $customerSearch->addressline1;
						$shippingAddress->state = $customerSearch->stateprov;
						$shippingAddress->city = $customerSearch->city;
						$shippingAddress->country = $customerSearch->country;
						$shippingAddress->zip_code = $customerSearch->zipcode;
						$shippingAddress->address_type = CustomerAddress::SHIPPING;*/
						
						// get customer personno based on characterreserved1, and save to customer_transaction_address
						$shippingAddress->ref_id = $customerSearch->id;
						
						
						$shippingAddress->save();

					}elseif(isset($addressSearch)){
						unset($addressSearch);
					}
				}
			}

		} catch (\Exception $e) {
			\DB::rollback();
			dd($e->getMessage());
		}
			\DB::commit();

			return redirect()->route('customers.index')->with('success','successfully upload customer data');
	}


	public function checkRole(){
		$user = \Sentinel::getUser();
		if($user){
            $current_user['superadmin'] = \Sentinel::inRole('superadmin');
            $current_user['admin'] = \Sentinel::inRole('admin');
            $current_user['sales'] = \Sentinel::inRole('sales');
            $current_user['gm'] = \Sentinel::inRole('gm');
            $current_user['bm'] = \Sentinel::inRole('bm');
            $current_user['debtCollector'] = \Sentinel::inRole('debtCollector');
            $current_user['customer'] = \Sentinel::inRole('customer');
			
			if($current_user['superadmin'] == true){
				return 'superadmin';
			}elseif($current_user['admin'] == true){
				return 'admin';
			}elseif($current_user['sales'] == true){
				return 'sales';
			}elseif($current_user['gm'] == true){
				return 'gm';
			}elseif($current_user['bm'] == true){
				return 'bm';
			}elseif($current_user['debtCollector'] == true){
				return 'debtCollector';
			}

		}else{
			return 'notLogin';
		}
	}		
}		

