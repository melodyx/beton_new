<?php

namespace Modules\Customers\Http\Controllers;

use Nwidart\Modules\Routing\Controller;
use Chrisbjr\ApiGuard\Http\Controllers\ApiGuardController;
use Modules\Users\Entities\User;
use Modules\Customers\Entities\Customer;
use Modules\Customers\Entities\CustomerAddress;
use Modules\Customers\Entities\Approval;
use Modules\Customers\Entities\ApprovalDetail;
use Modules\Invoices\Entities\Invoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Validator;
use Sentinel;
use Reminder, Mail;

class ApiController extends ApiGuardController {


    /**
     * @SWG\Get(
     *   path="/customers/getAll",
     *   summary="List of all customer with discount",
     *   description="-",
     *   tags={"customers"},
     *   @SWG\Response(
     *     response=200,
     *     description="True / False after submit",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Error Message",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   )
     * )
     */
    public function getAll(){
        try{

            $response = array('status' => 'error', 'code' => 400, 'data' => false);

            $datas = Customer::with(array('approval' => function($query){
                $query->where('status','=',Approval::APPROVED);
            },'invoices' => function($query){
                $query->where('status','=',Invoice::INDEBT);
            },'invoices.payments'))->orderBy('name','asc')->get();
            
            $x = 0;
            foreach($datas as $data){
                $customers[$x]['customerData'] = $data;
                foreach($data->approval as $approval){
                    $customers[$x]['bonus'] = $approval->bonus;
                }
                $x++;
            }

            if(isset($customers)){
                $response['status'] = 'success';
                $response['code'] = '200';
                $response['data'] = $customers;
            }else{
                $response['data'] = 'no customer data';
            }
            
        }catch(\Exception $e) {
            $response = array('status' => 'error', 'code' => 500, 'data' => $e->getMessage());
        }

        return Response::json($response);
    }

    /**
     * @SWG\Post(
     *   path="/customers/register",
     *   summary="Register new customer",
     *   description="-",
     *   tags={"customers"},
     *   @SWG\Parameter(
     *       name="transactionid",
     *       in="query",
     *       description="transaction id for checking author",
     *       required=false,
     *       type="integer",
     *       format="integer, transaction id",
     *       @SWG\Items(type="integer"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="personno",
     *       in="query",
     *       description="Person no to register",
     *       required=false,
     *       type="integer",
     *       format="integer, person no",
     *       @SWG\Items(type="integer"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="persontype",
     *       in="query",
     *       description="Person type to register",
     *       required=false,
     *       type="integer",
     *       format="integer, person type",
     *       @SWG\Items(type="integer"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="balance",
     *       in="query",
     *       description="Balance to register",
     *       required=false,
     *       type="string",
     *       format="string, balance",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="email",
     *       in="query",
     *       description="email to register",
     *       required=false,
     *       type="string",
     *       format="string, person type",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="name",
     *       in="query",
     *       description="Name to register",
     *       required=true,
     *       type="string",
     *       format="string, name",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="address",
     *       in="query",
     *       description="Address to register",
     *       required=false,
     *       type="string",
     *       format="string, address",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="city",
     *       in="query",
     *       description="City to register",
     *       required=true,
     *       type="string",
     *       format="string, city",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="stateprov",
     *       in="query",
     *       description="State prov to register",
     *       required=false,
     *       type="string",
     *       format="string, stateprov",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="zipcode",
     *       in="query",
     *       description="Zip code to register",
     *       required=false,
     *       type="integer",
     *       format="integer, zip code",
     *       @SWG\Items(type="integer"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="country",
     *       in="query",
     *       description="Country to register",
     *       required=false,
     *       type="string",
     *       format="string, country",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="phone",
     *       in="query",
     *       description="Phone to register",
     *       required=false,
     *       type="string",
     *       format="string, phone",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="contact",
     *       in="query",
     *       description="Contact person to register",
     *       required=false,
     *       type="string",
     *       format="string, country",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="customertypeid",
     *       in="query",
     *       description="Customer type id to register",
     *       required=true,
     *       type="string",
     *       format="string, customertypeid",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="True / False after submit",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Error Message",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   )
     * )
     */
    public function register(Request $request){
        \DB::beginTransaction();
        try{
            $data = $request->all();
            $response = array('status' => 'error', 'code' => 400, 'data' => false);

            $rules = array(
                'transactionid'         => 'numeric',
                'personno'     => 'numeric',
                'persontype'     => 'numeric',
                'balance'     => 'string',
                'email'     => 'string',
                'name'     => 'string',
                'address'   =>  'string',
                'city'  =>  'string',
                'stateprov' =>  'string',
                'zipcode'   =>  'numeric',
                'country'   =>  'string',
                'phone' =>  'string',
                'contact'   =>  'string',
                'customertypeid' => 'string'
            );

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                $response['data'] = $validator->errors()->all();
            } else {

                $customer = new Customer();
                if(isset($data['transactionid'])) $customer->transactionid = $data['transactionid'];
                if(isset($data['personno'])) $customer->personno = $data['personno'];
                if(isset($data['persontype'])) $customer->persontype = $data['persontype'];
                if(isset($data['balance'])) $customer->balance = $data['balance'];
                if(isset($data['email'])) $customer->email = $data['email'];
                if(isset( $data['name'])) $customer->name = $data['name'];
                if(isset($data['address'])) $customer->addressline1 = $data['address'];
                if(isset($data['city'])) $customer->city = $data['city'];
                if(isset($data['stateprov'])) $customer->stateprov = $data['stateprov'];
                if(isset($data['zipcode'])) $customer->zipcode = $data['zipcode'];
                if(isset($data['country'])) $customer->country = $data['country'];
                if(isset($data['phone'])) $customer->phone = $data['phone'];
                if(isset($data['contact'])) $customer->contact = $data['contact'];
                if(isset($data['customertypeid'])) $customer->customertypeid = $data['customertypeid'];
                $customer->status = Customer::APPROVED;
                $customer->save();
                
                $response['status'] = 'success';
                $response['code'] = '200';
                $response['data'] = $customer;
            }
            
        }catch(\Exception $e) {
            \DB::rollback();
            $response = array('status' => 'error', 'code' => 500, 'data' => $e->getMessage());
        }
        \DB::commit();

        return Response::json($response);
    }

    /**
     * @SWG\Post(
     *   path="/customers/registerAddress",
     *   summary="Register new customer",
     *   description="-",
     *   tags={"customers"},
     *   @SWG\Parameter(
     *       name="customer_id",
     *       in="query",
     *       description="Customer id for register address",
     *       required=true,
     *       type="integer",
     *       format="integer, customer id",
     *       @SWG\Items(type="integer"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="destination_name",
     *       in="query",
     *       description="Destination name for register address",
     *       required=false,
     *       type="string",
     *       format="string, destination name",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="email",
     *       in="query",
     *       description="email for register address",
     *       required=false,
     *       type="string",
     *       format="string, email address",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="phone",
     *       in="query",
     *       description="phone for register address",
     *       required=false,
     *       type="string",
     *       format="string, phone",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="address",
     *       in="query",
     *       description="address for register address",
     *       required=false,
     *       type="string",
     *       format="string, address",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="city",
     *       in="query",
     *       description="city for register address",
     *       required=false,
     *       type="string",
     *       format="string, city",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="state",
     *       in="query",
     *       description="state for register address",
     *       required=false,
     *       type="string",
     *       format="string, state",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="country",
     *       in="query",
     *       description="country for register address",
     *       required=false,
     *       type="string",
     *       format="string, country",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="zip_code",
     *       in="query",
     *       description="zip code for register address",
     *       required=false,
     *       type="integer",
     *       format="integer, zip code",
     *       @SWG\Items(type="integer"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="address_type",
     *       in="query",
     *       description="address type for register address",
     *       required=true,
     *       type="string",
     *       format="string, address type",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="True / False after submit",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Error Message",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   )
     * )
     */
    public function registerAddress(Request $request){
        \DB::beginTransaction();
        try {
            $data = $request->all();
			unset($data['api_key']);

            $response = array('status' => 'error', 'code' => 400, 'data' => false);

            $customers = array();
            $customer = Customer::find($data['customer_id']);
            $customers['profile'] = $customer;
			if($data['address_type'] == CustomerAddress::BILLING){
				$checkAddress = CustomerAddress::where('customer_id','=',$data['customer_id'])->where('address_type','=',$data['address_type'])->first();
			}elseif($data['address_type'] == CustomerAddress::SHIPPING){
				$checkAddress = CustomerAddress::where('customer_id','=',$data['customer_id'])->where('destination_name','=',$data['destination_name'])->where('city','=',$data['city'])->first();
				if(isset($checkAddress)){
					$response['data'] = 'Shipping address data already registered';
					return Response::json($response);
				}
			}
			
            if(isset($checkAddress)){
                /*$checkAddress->customer_id = $data['customer_id'];
                $checkAddress->destination_name = $data['destination_name'];
                $checkAddress->email = $data['email'];
                $checkAddress->phone = $data['phone'];
                $checkAddress->address = $data['address'];
                $checkAddress->city = $data['city'];
                $checkAddress->state = $data['state'];
                $checkAddress->country = $data['country'];
                $checkAddress->zip_code = $data['zip_code'];
                $checkAddress->address_type = $data['address_type'];
                $checkAddress->save();*/
				CustomerAddress::where('customer_id','=',$data['customer_id'])->where('address_type','=',$data['address_type'])->update($data);
				$customerAddress = CustomerAddress::where('customer_id','=',$data['customer_id'])->where('address_type','=',$data['address_type'])->get();
                $customers['new_address_data'] = $customerAddress;
            }else{
                /*$customerAddress = new CustomerAddress();
                $customerAddress->customer_id = $data['customer_id'];
                $customerAddress->destination_name = $data['destination_name'];
                $customerAddress->email = $data['email'];
                $customerAddress->phone = $data['phone'];
                $customerAddress->address = $data['address'];
                $customerAddress->city = $data['city'];
                $customerAddress->state = $data['state'];
                $customerAddress->country = $data['country'];
                $customerAddress->zip_code = $data['zip_code'];
                $customerAddress->address_type = $data['address_type'];
                $customerAddress->save();*/
				CustomerAddress::insert($data);
				$customerAddress = CustomerAddress::where('customer_id','=',$data['customer_id'])->where('address_type','=',$data['address_type'])->get();
                $customers['new_address_data'] = $customerAddress;
            }
            $response['status'] = 'success';
            $response['code'] = '200';
            $response['data'] = $customers;
        } catch (Exception $e) {
            \DB::rollback();
            $response['data'] = $e->getMessage();
        }
        \DB::commit();
        return Response::json($response);
    }

     /**
     * @SWG\get(
     *   path="/customers/searchCustomer",
     *   summary="getting customer data base on customer id",
     *   description="-",
     *   tags={"customers"},
     *   @SWG\Parameter(
     *       name="customer_id",
     *       in="query",
     *       description="Customer id for change",
     *       required=true,
     *       type="integer",
     *       format="integer, customer id",
     *       @SWG\Items(type="integer"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="True / False after submit",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Error Message",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   )
     * )
     */
     public function searchCustomer(Request $request){
        \DB::beginTransaction();
        try{
            $data = $request->all();
            $response = array('status' => 'error', 'code' => 400, 'data' => false);
            $rules = array(
                'customer_id'     => 'numeric|required'
            );

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                $response['data'] = $validator->errors()->all();
            } else {
                /*$customer = Customer::with(array('approval' => function($query){
                $query->where('status','=',Approval::APPROVED)->orWhere('status','=',Approval::REJECTED);
            },'invoices' => function($query){
                $query->where('status','=',Invoice::INDEBT);
            },'invoices.payments'))->find($data['customer_id']);*/
                
                $customer = Customer::with(array('invoices' => function($query){
                        $query->where('status','=',Invoice::INDEBT);
                    },'invoices.payments','customerAddresses'))->find($data['customer_id']);

                foreach($customer->customerAddresses as &$customerAddress){
                    if(!is_null($customerAddress->ref_id)){
                        $findCustomer = Customer::where('id','=',$customerAddress->ref_id)->first();
                        $customerAddress->destination_name = $findCustomer->name;
                        $customerAddress->email = $findCustomer->email;
                        $customerAddress->phone = $findCustomer->phone;
                        $customerAddress->address = $findCustomer->addressline1;
                        $customerAddress->city = $findCustomer->city;
                        $customerAddress->state = $findCustomer->stateprov;
                        $customerAddress->country = $findCustomer->country;
                        $customerAddress->zip_code= $findCustomer->zipcode;
                    }
                }
                //$customer->defaultdisc = 'Rp '.$customer->defaultdisc;

                $customers['customerData'] = array();
                $customers['historyApprovals'] = array();

                $customerHistoryApproval = Customer::with(array('approval.approvalDetails' => function($query){
                    $query->orderBy('created_at','desc');
                }))->find($data['customer_id']);

                // $response['data'] = $customerHistoryApproval->approval;
                // return Response::json($response);

                $historyApprovals = array();

                $x = 0;
                if(isset($customerHistoryApproval->approval)){
                    foreach($customerHistoryApproval->approval as $approval){
                        if($approval->approvalDetails->count()){
                            foreach($approval->approvalDetails as $approvalDetail){
                                $historyApprovals[$x] = $approvalDetail;
                            }
                            $historyApprovals[$x]->original_priority = $approval->priority;
                            $historyApprovals[$x]->priority_update = $approval->priority_update;
                            $x++;
                        }
                    }
                $customers['historyApprovals'] = $historyApprovals;
                }
                //$x = 0;
                $customers['customerData'] = $customer;
                $customers['bonus'] = $customer->bonus;

                

                if(isset($customers)){
                    $response['status'] = 'success';
                    $response['code'] = '200';
                    $response['data'] = $customers;
                }else{
                    $response['data'] = 'no customer data';
                }
            }            

            
        }catch(\Exception $e) {
            \DB::rollback();
            $response = array('status' => 'error', 'code' => 500, 'data' => $e->getMessage());
        }
        \DB::commit();

        return Response::json($response);
     }

     /**
     * @SWG\get(
     *   path="/customers/approvalChangeProgress",
     *   summary="getting approval progress base on customer id",
     *   description="-",
     *   tags={"customers"},
     *   @SWG\Parameter(
     *       name="customer_id",
     *       in="query",
     *       description="Customer id for change",
     *       required=true,
     *       type="integer",
     *       format="integer, customer id",
     *       @SWG\Items(type="integer"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="True / False after submit",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Error Message",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   )
     * )
     */
     public function approvalChangeProgress(Request $request){
        \DB::beginTransaction();
        try{
            $data = $request->all();
            $response = array('status' => 'error', 'code' => 400, 'data' => false);
            $rules = array(
                'customer_id'     => 'numeric|required'
            );

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                $response['data'] = $validator->errors()->all();
            } else {
                $customer = Customer::with(array('approval' => function($query){
                $query->where('status','=',Approval::UNAPPROVE);
            },'invoices' => function($query){
                $query->where('status','=',Invoice::INDEBT);
            },'invoices.payments'))->find($data['customer_id']);
                
                if($customer->approval->count()){
                    if($customer->approval[0]->bm_approval > 0 && $customer->approval[0]->gm_approval > 0){
                        $customers['approval_status'] = Approval::APPROVED;
                    }elseif($customer->approval[0]->gm_approval > 0){
                        $customers['approval_status'] = 'gm approve';
                    }elseif($customer->approval[0]->bm_approval > 0){
                        $customers['approval_status'] = 'bm approve';
                    }else{
                        $customers['approval_status'] = 'no progress';
                    }
                }else{
                    if($customer->approval[0]->status == Approval::REJECTED){
                        $customers['approval_status'] = Approval::REJECTED;
                    }else{
                        $customers['approval_status'] = Approval::APPROVED;;
                    }
                }

                $customers['customerData'] = $customer;
                $customers['bonus'] = $customer->bonus;

                

                if(isset($customers)){
                    $response['status'] = 'success';
                    $response['code'] = '200';
                    $response['data'] = $customers;
                }else{
                    $response['data'] = 'no customer data';
                }
            }            

            
        }catch(\Exception $e) {
            \DB::rollback();
            $response = array('status' => 'error', 'code' => 500, 'data' => $e->getMessage());
        }
        \DB::commit();

        return Response::json($response);
     }

     /**
     * @SWG\get(
     *   path="/customers/checkPriorityChangeProgress",
     *   summary="Checking before change priority for prevent other change when current change not approved yet",
     *   description="-",
     *   tags={"customers"},
     *   @SWG\Parameter(
     *       name="customer_id",
     *       in="query",
     *       description="Customer id for change",
     *       required=true,
     *       type="integer",
     *       format="integer, sales id",
     *       @SWG\Items(type="integer"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="True / False after submit",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Error Message",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   )
     * )
     */
     public function checkPriorityChangeProgress(Request $request){
        \DB::beginTransaction();
        try{
            $data = $request->all();
            $response = array('status' => 'error', 'code' => 400, 'data' => false);
            $rules = array(
                'customer_id'     => 'numeric|required'
            );

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                $response['data'] = $validator->errors()->all();
            } else {
                $customers = Customer::with(array('approval'=>function($query){
                    $query->where('status', '=', Approval::UNAPPROVE);
                }))->find($data['customer_id']);
                
                if(!$customers->approval->count()){
                    $priority = array(
                    Customer::GENERAL => Customer::GENERAL,
                    Customer::PRIORITY => Customer::PRIORITY,
                    Customer::BLACKLIST => Customer::BLACKLIST
                );
                $customer['customerData'] = $customers;
                $customer['priority'] = $priority;

                $response['status'] = 'success';
                $response['code'] = '200';
                $response['data'] = $customer;
                }else{
                    $response['data'] = 'this customer still have unapprove change priority';
                }

            }            

            
        }catch(\Exception $e) {
            \DB::rollback();
            $response = array('status' => 'error', 'code' => 500, 'data' => $e->getMessage());
        }
        \DB::commit();

        return Response::json($response);
     }

    /**
     * @SWG\Post(
     *   path="/customers/changePriority",
     *   summary="Change priority for customer priority",
     *   description="-",
     *   tags={"customers"},
     *   @SWG\Parameter(
     *       name="user_id",
     *       in="query",
     *       description="users id for checking author",
     *       required=true,
     *       type="integer",
     *       format="integer, sales id",
     *       @SWG\Items(type="integer"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="customer_id",
     *       in="query",
     *       description="Customer id for change",
     *       required=true,
     *       type="integer",
     *       format="integer, sales id",
     *       @SWG\Items(type="integer"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="priority_update",
     *       in="query",
     *       description="new priority status",
     *       required=true,
     *       type="string",
     *       format="string, sales id",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="change_reason",
     *       in="query",
     *       description="reason for changing priority",
     *       required=true,
     *       type="string",
     *       format="string, priority change reason",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="bonus",
     *       in="query",
     *       description="bonus percentage",
     *       required=true,
     *       type="integer",
     *       format="integer, sales id",
     *       @SWG\Items(type="integer"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="True / False after submit",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Error Message",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   )
     * )
     */
    public function changePriority(Request $request){
        \DB::beginTransaction();
        try{
            $data = $request->all();
            $response = array('status' => 'error', 'code' => 400, 'data' => false);

            $rules = array(
                'user_id'         => 'numeric|required',
                'customer_id'     => 'numeric|required',
                'priority_update'     => 'string|required',
                'change_reason'     => 'string|required',
                'bonus' => 'numeric'
            );

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                $response['data'] = $validator->errors()->all();
            } else {
                $customer = Customer::find($data['customer_id']);
                $checkingApproval = Approval::where('customer_id','=',$data['customer_id'])->where('status','=',Approval::UNAPPROVE)->first();
                    /*$response['data'] = $checkingApproval;
                    return Response::json($response);*/
                if(!isset($checkingApproval)){
                    $approval = new Approval();
                    $approval->customer_id = $customer->id;
                    $approval->priority = $customer->customertypeid;
                    $approval->priority_update = $data['priority_update'];
                    $approval->change_reason = $data['change_reason'];
                    $approval->bonus = $data['bonus'];
                    $approval->status = Approval::UNAPPROVE;
                    $approval->author = $data['user_id'];
                    $approval->save();

                    $approvalDetail = new ApprovalDetail();
                    $approvalDetail->approval_id = $approval->id;
                    $approvalDetail->description = 'Sales atas nama '.\Sentinel::findById($approval->author)->first_name.' mengajukan perubahan tipe pelanggan atas nama '.$customer->name;
                    $approvalDetail->disc_revision = $data['bonus'];
                    $approvalDetail->change_reason = $data['change_reason'];
                    $approvalDetail->author_id = $approval->author;
                    $approvalDetail->save();
                    
                    $approvals['approval'] = $approval;
                    $approvals['approvalDetail'] = $approvalDetail;

                    $response['status'] = 'success';
                    $response['code'] = '200';
                    $response['data'] = $approvals;

                    // Populate data for push notification
                    $usersData = User::all();


                    $users = array();
                    foreach($usersData as $userData){
                        $user = \Sentinel::findById($userData->id);
                        if($user->inRole('gm') || $user->inRole('bm')){
                            $users[] = $userData;
                        }
                    }

                    /*$response['data'] = $users;
                    return Response::json($response);*/

                    $notification = [
                        'title' => 'Pengajuan Perubahan',
                        'body'  => 'Sales '.\Sentinel::findById($approval->author)->first_name.' mengajukan perubahan tipe pelanggan atas nama '.$customer->name,
                    ];
                    $data = [
                        'title' => 'Pengajuan perubahan',
                        'subtitle' => 'Pengajuan perubahan dalam proses',
                        'message' => 'Pengajuan perubahan tipe pelanggan atas nama '.$customer->name.' dalam proses',
                        'data'  => [
                            'approval' => $approval,
                        ]
                    ];
                    foreach($users as $row){
                        pushNotification($row->fcm_token, $notification['title'], $notification['body'], $data);
                    }
                    
                    
                }else{
                    $response['data'] = 'this customer still have unapprove change';
                }
            }
        }catch(\Exception $e) {
            \DB::rollback();
            $response = array('status' => 'error', 'code' => 500, 'data' => $e->getMessage());
        }
        \DB::commit();

        return Response::json($response);
    }


    /**
     * @SWG\Get(
     *   path="/customers/listCustomerNeedApproval",
     *   summary="list of customers change priority needed approval",
     *   description="-",
     *   tags={"customers"},
     *   @SWG\Parameter(
     *       name="user_id",
     *       in="query",
     *       description="users id for checking author",
     *       required=true,
     *       type="integer",
     *       format="integer, sales id",
     *       @SWG\Items(type="integer"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="True / False after submit",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Error Message",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   )
     * )
     */
    public function listCustomerNeedApproval(Request $request){
        try{
            $data = $request->all();
            $response = array('status' => 'error', 'code' => 400, 'data' => false);

            $user = \Sentinel::findUserById($data['user_id']);
            if($user->inRole('bm')){
                $approvals = Approval::with(array('customer'))->where('status','=',Approval::UNAPPROVE)->where('bm_approval','=',0)->get();
            }elseif($user->inRole('gm')){
                $approvals = Approval::with(array('customer'))->where('status','=',Approval::UNAPPROVE)->where('bm_id','<>',0)->get();
            }

            if(isset($approvals) && $approvals->count()){
                $customer = array();
                $x = 0;
                foreach($approvals as $approval){
                    $customers[$x]['customerData'] = $approval->customer;
                    $customers[$x]['bonus'] = $approval->bonus;
                    $customers[$x]['change_reason'] = $approval->change_reason;
                    $customers[$x]['original_priority'] = $approval->priority;
                    $customers[$x]['priority_update'] = $approval->priority_update;
                    $customers[$x]['requested_by'] = \Sentinel::findById($approval->author);
                    $customers[$x]['history_approval'] = Approval::where('customer_id','=',$approval->customer_id)->where('status','=',Approval::REJECTED)->orWhere('status','=',Approval::APPROVED)->orderBy('created_at','asc')->get();
                    $x++;
                }
            }

            if(isset($customers)){
                $response['status'] = 'success';
                $response['code'] = '200';
                $response['data'] = $customers;
            }else{
                $response['data'] = 'no customer need approval';
            }
            
        }catch(\Exception $e) {
            $response = array('status' => 'error', 'code' => 500, 'data' => $e->getMessage());
        }

        return Response::json($response);
    }

    /**
     * @SWG\Post(
     *   path="/customers/approveChange",
     *   summary="for approving change",
     *   description="-",
     *   tags={"customers"},
     *   @SWG\Parameter(
     *       name="user_id",
     *       in="query",
     *       description="users id for checking author",
     *       required=true,
     *       type="integer",
     *       format="integer, sales id",
     *       @SWG\Items(type="integer"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="customer_id",
     *       in="query",
     *       description="customer id for for approve change",
     *       required=true,
     *       type="integer",
     *       format="integer, customer id",
     *       @SWG\Items(type="integer"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="approval",
     *       in="query",
     *       description="approval for change",
     *       required=true,
     *       type="integer",
     *       format="integer, approval",
     *       @SWG\Items(type="integer"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="reason",
     *       in="query",
     *       description="reason for change",
     *       required=false,
     *       type="string",
     *       format="string, reason",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
      *   @SWG\Parameter(
     *       name="bonus_update",
     *       in="query",
     *       description="reason for change",
     *       required=false,
     *       type="integer",
     *       format="integer, reason",
     *       @SWG\Items(type="integer"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="True / False after submit",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Error Message",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   )
     * )
     */
    public function approveChange(Request $request){
       \DB::beginTransaction();
        try{
            $data = $request->all();

            $response = array('status' => 'error', 'code' => 400, 'data' => false);
            $rules = array(
                'user_id'         => 'numeric|required',
                'customer_id'     => 'numeric|required',
                'approval'     => 'numeric|required',
                'reason'     => 'string',
                'bonus_update' => 'numeric'
            );

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                $response['data'] = $validator->errors()->all();
            } else {
                $user = \Sentinel::findUserById($data['user_id']);

                $approval = Approval::with('customer')->where('customer_id','=',$data['customer_id'])->where('status','=',Approval::UNAPPROVE)->first();

                $approvalDetail = new ApprovalDetail();

                $approvalDetail->approval_id = $approval->id;

                if(isset($approval)){
                    if($user->inRole('gm')){
                        if($approval->bm_id != 0){
                            $approval->gm_id = $user->id;
                            $approval->gm_approval = $data['approval'];

                             $approvalDetail->description = 'GM atas nama '.$user->first_name.' menyetujui perubahan tipe pelanggan';

                            if(isset($data['reason'])){
                                $approval->gm_reason = $data['reason'];
                                $approvalDetail->change_reason = $data['reason'];
                            }

                            $usersData = User::all();
                            $users = array();
                            foreach($usersData as $userData){
                                $user = \Sentinel::findById($userData->id);
                                if($user->inRole('sales')){
                                    $users[] = $userData;
                                }
                            }

                            if(isset($data['bonus_update'])){
                                $approval->status = Approval::REJECTED;
                                $approvalDetail->disc_revision = $data['bonus_update'];
                                $approvalDetail->description = 'GM atas nama '.$user->first_name.' menolak perubahan tipe pelanggan';

                                $notification = [
                                    'title' => 'Konfirmasi Perubahan',
                                    'body'  => 'General Manager '.\Sentinel::findById($approval->gm_id)->first_name.' menolak perubahan tipe pelanggan atas nama '.$approval->customer->name,
                                ];
                                $data = [
                                    'title' => 'Konfirmasi Perubahan',
                                    'subtitle' => 'Konfirmasi perubahan dalam proses',
                                    'message' => 'Pengajuan perubahan tipe pelanggan atas nama '.$approval->customer->name.' telah ditolak oleh General Manager '.\Sentinel::findById($approval->gm_id)->first_name,
                                    'data'  => [
                                        'approval' => $approval,
                                    ]
                                ];

                            }else{
                                if($data['approval'] == 0){
                                    $approval->status = Approval::REJECTED;
                                    $approvalDetail->description = 'GM atas nama '.$user->first_name.' menolak perubahan tipe pelanggan';
                                    $notification = [
                                        'title' => 'Konfirmasi Perubahan',
                                        'body'  => 'General Manager '.\Sentinel::findById($approval->gm_id)->first_name.' menolak perubahan tipe pelanggan atas nama '.$approval->customer->name,
                                    ];
                                    $data = [
                                        'title' => 'Konfirmasi Perubahan',
                                        'subtitle' => 'Konfirmasi perubahan dalam proses',
                                        'message' => 'Pengajuan perubahan tipe pelanggan atas nama '.$approval->customer->name.' telah ditolak oleh General Manager '.\Sentinel::findById($approval->gm_id)->first_name,
                                        'data'  => [
                                            'approval' => $approval,
                                        ]
                                    ];
                                }else{
                                    $approval->status = Approval::APPROVED;
                                    $approval->customer->customertypeid = $approval->priority_update;
                                    $approval->customer->defaultdisc = $approval->bonus;

                                    $notification = [
                                        'title' => 'Konfirmasi Perubahan',
                                        'body'  => 'General Manager '.\Sentinel::findById($approval->gm_id)->first_name.' menyetujui perubahan tipe pelanggan atas nama '.$approval->customer->name,
                                    ];
                                    $data = [
                                        'title' => 'Konfirmasi Perubahan',
                                        'subtitle' => 'Konfirmasi perubahan dalam proses',
                                        'message' => 'Pengajuan perubahan tipe pelanggan atas nama '.$approval->customer->name.' telah disetujui oleh General Manager '.\Sentinel::findById($approval->gm_id)->first_name,
                                        'data'  => [
                                            'approval' => $approval,
                                        ]
                                    ];
                                }
                            }
                                $approval->save();
                                $approval->customer->save();
                                $approvalDetail->save();

                                $approvals['approval'] = $approval;
                                $approvals['approvalDetail'] = $approvalDetail;

                                $response['status'] = 'success';
                                $response['code'] = '200';
                                $response['data'] = $approvals;
                        }else{
                            $response['data'] = 'this change not approve yet by Branch Manager';
                        }

                    }elseif($user->inRole('bm')){

                            $approval->bm_id = $user->id;
                            $approval->bm_approval = $data['approval'];
                            
                            $approvalDetail->description = 'BM atas nama '.$user->first_name.' menyetujui perubahan tipe pelanggan';

                            if(isset($data['reason'])){
                                $approval->bm_reason = $data['reason'];
                                $approvalDetail->change_reason = $data['reason'];
                            }

                            $usersData = User::all();
                            $users = array();
                            foreach($usersData as $userData){
                                $user = \Sentinel::findById($userData->id);
                                if($user->inRole('gm') || $user->inRole('sales')){
                                    $users[] = $userData;
                                }
                            }

                            if(isset($data['bonus_update'])){
                                $approval->status = Approval::REJECTED;
                                $approvalDetail->disc_revision = $data['bonus_update'];
                                $approvalDetail->description = 'BM atas nama '.$user->first_name.' menolak perubahan tipe pelanggan';
                                $notification = [
                                    'title' => 'Konfirmasi Perubahan',
                                    'body'  => 'Branch Manager '.\Sentinel::findById($approval->bm_id)->first_name.' menolak perubahan tipe pelanggan atas nama '.$approval->customer->name,
                                ];
                                $data = [
                                    'title' => 'Konfirmasi Perubahan',
                                    'subtitle' => 'Konfirmasi perubahan dalam proses',
                                    'message' => 'Pengajuan perubahan tipe pelanggan atas nama '.$approval->customer->name.' telah ditolak oleh Branch Manager '.\Sentinel::findById($approval->bm_id)->first_name,
                                    'data'  => [
                                        'approval' => $approval,
                                    ]
                                ];
                            }else{
                                if($data['approval'] == 0){
                                    $approval->status = Approval::REJECTED;
                                    $approvalDetail->description = 'BM atas nama '.$user->first_name.' menolak perubahan tipe pelanggan';
                                    $notification = [
                                        'title' => 'Konfirmasi Perubahan',
                                        'body'  => 'Branch Manager '.\Sentinel::findById($approval->bm_id)->first_name.' menolak perubahan tipe pelanggan atas nama '.$approval->customer->name,
                                    ];
                                    $data = [
                                        'title' => 'Konfirmasi Perubahan',
                                        'subtitle' => 'Konfirmasi perubahan dalam proses',
                                        'message' => 'Pengajuan perubahan tipe pelanggan atas nama '.$approval->customer->name.' telah ditolak oleh Branch Manager '.\Sentinel::findById($approval->bm_id)->first_name,
                                        'data'  => [
                                            'approval' => $approval,
                                        ]
                                    ];
                                }else{
                                    $notification = [
                                        'title' => 'Konfirmasi Perubahan',
                                        'body'  => 'Branch Manager '.\Sentinel::findById($approval->bm_id)->first_name.' menyetujui perubahan tipe pelanggan atas nama '.$approval->customer->name,
                                    ];
                                    $data = [
                                        'title' => 'Konfirmasi Perubahan',
                                        'subtitle' => 'Konfirmasi perubahan dalam proses',
                                        'message' => 'Pengajuan perubahan tipe pelanggan atas nama '.$approval->customer->name.' telah disetujui oleh Branch Manager '.\Sentinel::findById($approval->bm_id)->first_name,
                                        'data'  => [
                                            'approval' => $approval,
                                        ]
                                    ];
                                }
                            }
                            $approval->save();
                            $approvalDetail->save();

                            $approvals['approval'] = $approval;
                            $approvals['approvalDetail'] = $approvalDetail;

                            $response['status'] = 'success';
                            $response['code'] = '200';
                            $response['data'] = $approvals;
                    }else{
                        $response['data'] = 'User dont have any authority to approve current change';
                    }
                }else{
                        $response['data'] = 'Customer approval not found';
                }
                
            }
                        
        }catch(\Exception $e) {
            \DB::rollback();
            $response = array('status' => 'error', 'code' => 500, 'data' => $e->getMessage());
        }
        \DB::commit();
        foreach($users as $row){
            pushNotification($row->fcm_token, $notification['title'], $notification['body'], $data);
        }
        return Response::json($response);
    }

    
}
