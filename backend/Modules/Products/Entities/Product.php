<?php

namespace Modules\Products\Entities;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [];

    public static function saveProduct($id,$data){
    	\DB::beginTransaction();
		try {
			if(is_null($id)){
				$product = new Product();
			}else{
				$product = Product::find($id);
			}
			$product->series = $data['series'];
			$product->name = $data['name'];
			$product->description = $data['description'];
			$product->short_description = $data['short_description'];
			$product->category_id = $data['category_id'];
			if(isset($data['feat_image'])){
				$product->feat_image = $data['feat_image'];
			}
			$product->save();
		}catch (\Exception $e){
			\DB::rollback();
			return redirect()->route('products.index')->with('success', $e->getMessage());
		}
		\DB::commit();

		return $product;
    }

    public function orderDetails(){
    	return $this->hasMany('\Modules\Transactions\Entities\OrderDetail','product_id');
    }

    public function productCategory(){
    	return $this->belongsTo('\Modules\Products\Entities\ProductCategory','category_id');
    }
}
