<?php

namespace Modules\Products\Entities;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $fillable = [];
    protected $table = 'products_category';

    public static function saveCategory($id, $data){
		\DB::beginTransaction();
		try {
			if(is_null($id)){
				$category = new ProductCategory();
			}else{
				$category = ProductCategory::find($id);
			}
			$category->name = $data['name'];

			if($data['slug']){
				$slug = strtolower(str_replace(' ', '_', $data['slug']));
			}else{
				$slug = strtolower(str_replace(' ', '_', $data['name']));
			}

			if(is_null($id)){
				$slugCategory = ProductCategory::where('slug','=',$slug)->first();
				if(isset($slugCategory)){
					$category->slug = $slug.'_1';
				}else{
					$category->slug = $slug;
				}
			}else{
				if($slug == $category->getOriginal('slug')){
					$category->slug = $slug;
				}else{
					$slugCategory = ProductCategory::where('slug','=',$slug)->first();
					if(isset($slugCategory)){
						$category->slug = $slug.'_1';
					}else{
						$category->slug = $slug;
					}
				}
			}

			if(isset($data['parent'])){
				$category->parent = $data['parent'];
			}

			$category->feat_image = $data['feat_image'];
			$category->save();
		} catch (\Exception $e) {
			\DB::rollback();
			return false;
		}
		\DB::commit();
		return $category;
	}

	public function products(){
		return $this->hasMany('\Modules\Transactions\Entities\Product','category_id');
	}
}
