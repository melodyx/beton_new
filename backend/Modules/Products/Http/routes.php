<?php

Route::group(['middleware' => 'web', 'namespace' => 'Modules\Products\Http\Controllers'], function()
{
	//Route::get('/', 'ProductsController@index');
	Route::get('products/createCategory','ProductsController@createCategory')->name('products.createCategory');
	Route::post('products/storeCategory','ProductsController@storeCategory')->name('products.storeCategory');
	Route::get('products/editCategory/{id}','ProductsController@editCategory')->name('products.editCategory');
	Route::put('products/updateCategory/{id}','ProductsController@updateCategory')->name('products.updateCategory');
	Route::delete('products/destroyCategory/{id}','ProductsController@destroyCategory')->name('products.destroyCategory');
	
	Route::get('products/uploadFile','ProductsController@uploadFile')->name('products.uploadFile');
	Route::post('products/storeFile','ProductsController@storeFile')->name('products.storeFile');
	Route::resource('products','ProductsController');

	Route::group(['middleware' => ['apiAuth'], 'prefix' => 'api/v1'], function() {
		Route::get('/products/getAll','ApiController@getAll');
	});
});