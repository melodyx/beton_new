<?php

namespace Modules\Products\Http\Controllers;

use Nwidart\Modules\Routing\Controller;
use Modules\Products\Entities\Product;
use Modules\Products\Entities\ProductCategory;
use Illuminate\Http\Request;

class ProductsController extends Controller {

	public function index()
	{
		//Checking role for access customer page
		$current_user_role = $this->checkRole();
		if($current_user_role == 'notLogin'){
			return redirect()->route('login')->with('success','you dont have any authorization for access this page');
		}
		//=====================================
		$products = Product::all();
		return view('products::index',['products' => $products, 'current_user_role' => $current_user_role]);
	}

	public function create(){
		//Checking role for access customer page
		$current_user_role = $this->checkRole();
		if($current_user_role == 'notLogin' || $current_user_role == 'sales'){
			return redirect()->route('login')->with('success','you dont have any authorization for access this page');
		}
		//=====================================
		$productsCategory = ProductCategory::all();
		return view('products::products.create',['current_user_role' => $current_user_role, 'productsCategory' => $productsCategory]);
	}

	public function store(Request $request){
		$data = $request->all();

		$product = Product::saveProduct(null,$data);

		return redirect()->route('products.index')->with('success','successfully added new products');
	}

	public function edit($id){
		//Checking role for access customer page
		$current_user_role = $this->checkRole();
		if($current_user_role == 'notLogin' || $current_user_role == 'sales'){
			return redirect()->route('login')->with('success','you dont have any authorization for access this page');
		}
		//=====================================
		$product = Product::find($id);
		$productsCategory = ProductCategory::all();

		return view('products::products.edit',['product' => $product,'current_user_role' => $current_user_role, 'productsCategory' => $productsCategory]);
	}

	public function update($id, Request $request){
		$data = $request->all();
		$product = Product::saveProduct($id,$data);
		return redirect()->route('products.index')->with('success','successfully added new products');
	}

	public function destroy($id){
		$product = Product::find($id);
		$product->delete();
		//update activity log
			$current_user = \Sentinel::getUser();
			$log = array(
				'desc' => $current_user->id.' deleting product '.$product->name
			);
			event(new \App\Events\UpdateData($product, $log));

		 return array(
            'url' => route('products.index'),
            'message' => 'Something went wrong while trying to remove the product!'
        );
	}

	public function uploadFile(){
		return view('products::products.uploadFile');
	}

	public function storeFile(Request $request){
		$data = $request->all();
		\DB::beginTransaction();
		try {
			if(isset($data['csv_file'])){
			$handle = fopen($data['csv_file'], 'r');
			}
			$csvArray = array();
			if(isset($handle)){
				while (($csv = fgetcsv($handle, 1000, ",")) !== FALSE) {

					$checkProduct = Product::where('series','=',$csv[0])->where('name','=',$csv[1])->first();
					if(!isset($checkProduct)){
						$product = new Product();
						$product->series = $csv[0];
						$product->name = $csv[1];
						if($csv[2]){
							$categoryName = strtolower($csv[2]);
							$category = ProductCategory::where('slug','=',$categoryName)->first();
							$product->category_id = $category->id;	
						}
						$product->save();
					}else{
						if($csv[2]){
							$categoryName = strtolower($csv[2]);
							$category = ProductCategory::where('slug','=',$categoryName)->first();
							$checkProduct->category_id = $category->id;
							$checkProduct->save();	
						}
					}
				}
			}
		} catch (\Exception $e) {
			\DB::rollback();
			return redirect()->route('products.index')->with('succeess','failed add data product');
		}
		\DB::commit();

		return redirect()->route('products.index')->with('success','successfully upload data products');
		
	}

	public function createCategory(){
		$current_user_role = $this->checkRole();
		if($current_user_role == 'notLogin' || $current_user_role == 'sales'){
			return redirect()->route('login')->with('success','you dont have authorization for access this page');
		}

		$categoriesList = ProductCategory::all();
		return view('products::products.createCategory',['categoriesList' => $categoriesList]);
	}

	public function storeCategory(Request $request){
		$data = $request->all();
		$category = ProductCategory::saveCategory(null, $data);
		
		// update activity log
		$current_user = \Sentinel::getUser();
		$log = array(
				'desc' => $current_user->id.' Adding category '.$data['name']
		);
		event(new \App\Events\UpdateData($category, $log));

		return redirect()->route('products.createCategory')->with('success', 'Category has been successfully added');
	}

	public function editCategory($id){
		// checking role
		$current_user_role = $this->checkRole();
		if($current_user_role == 'notLogin' || $current_user_role == 'sales'){
			return redirect()->route('login')->with('success','you dont have authorization for access this page');
		}
		// =======================
		$categoriesList = ProductCategory::all();
		$category = ProductCategory::find($id);
		return view('products::products.editCategory', ['category' => $category, 'categoriesList' => $categoriesList]);
	}

	public function updateCategory($id,Request $request){
		$data = $request->all();
		//dd($data);
		$category = ProductCategory::saveCategory($id, $data);

		return redirect()->route('products.createCategory')->with('success', 'Category has been successfully added');
	}

	public function destroyCategory($id){
		\DB::beginTransaction(); //Start transaction!
		try{
			$category = ProductCategory::find($id);
			$category->delete();
		}catch(\Exception $e){
			\DB::rollback();	
		}
		\DB::commit();
		// update activity log
		$current_user = \Sentinel::getUser();
		$log = array(
				'desc' => $current_user->id.' Deleting product '.$category->name
		);
		event(new \App\Events\UpdateData($category, $log));
		return array(
            'url' => route('products.createCategory'),
            'message' => 'Something went wrong while trying to remove the product!'
        );
	}


	public function checkRole(){
		$user = \Sentinel::getUser();
		if($user){
            $current_user['superadmin'] = \Sentinel::inRole('superadmin');
            $current_user['admin'] = \Sentinel::inRole('admin');
            $current_user['sales'] = \Sentinel::inRole('sales');
            $current_user['gm'] = \Sentinel::inRole('gm');
            $current_user['bm'] = \Sentinel::inRole('bm');
            $current_user['debtCollector'] = \Sentinel::inRole('debtCollector');
            $current_user['customer'] = \Sentinel::inRole('customer');
			
			if($current_user['superadmin'] == true){
				return 'superadmin';
			}elseif($current_user['admin'] == true){
				return 'admin';
			}elseif($current_user['sales'] == true){
				return 'sales';
			}elseif($current_user['gm'] == true){
				return 'gm';
			}elseif($current_user['bm'] == true){
				return 'bm';
			}elseif($current_user['debtCollector'] == true){
				return 'debtCollector';
			}

		}else{
			return 'notLogin';
		}
	}		

}
