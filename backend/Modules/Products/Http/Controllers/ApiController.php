<?php

namespace Modules\Products\Http\Controllers;

use League\Flysystem\Exception;
use Modules\Products\Entities\Product;
use Nwidart\Modules\Routing\Controller;
use Chrisbjr\ApiGuard\Http\Controllers\ApiGuardController;
use Modules\Users\Entities\User;
use Modules\Customers\Entities\Customer;
use Modules\Transactions\Entities\Order;
use Modules\Transactions\Entities\OrderDetail;
use Modules\Transactions\Entities\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Spatie\Activitylog\Models\Activity;
use Validator;
use Sentinel;
use Reminder, Mail;
use Socialite;
use Hash;

class ApiController extends ApiGuardController {

	
  
    /**
     * @SWG\Get(
     *   path="/products/getAll",
     *   summary="get all product info",
     *   description="-",
     *   tags={"products"},
     *   @SWG\Response(
     *     response=200,
     *     description="True / False after submit",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Product")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Error Message",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Product")
     *     ),
     *   )
     * )
     */
    public function getAll(){
        try{

            $products = Product::with('productCategory')->get();

            /*foreach($products as &$product){
                if(isset($product->productCategory->slug)){
                    $product->product_category = $product->productCategory->slug;
                }
            }*/
            $response['status'] = 'success';
            $response['code'] = '200';
            $response['data'] = $products;
                
        }catch(\Exception $e) {
            $response = array('status' => 'error', 'code' => 500, 'data' => $e->getMessage());
            return Response::json($response);
        }

        return Response::json($response);
    }
     


}
