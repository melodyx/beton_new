<?php
	use \Illuminate\Support\Facades\Input;
	if(!isset($method)){
		$method = 'post';
	}

?>
<script type="text/javascript" src="<?php echo asset('assets/js/plugins/forms/styling/uniform.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo asset('assets/js/pages/form_layouts.js') ?>"></script>


{{ Form::open(array('action' => $action, 'method' => $method, 'class' => 'form-horizontal')) }}
<fieldset class="content-group">
							<legend class="text-bold"><?php if(isset($category)){ echo 'Edit category'; }else{ echo 'Create category';}?></legend>
								<div class="form-group">
									<label class="control-label col-lg-2">Category name</label>
									<div class="col-lg-10">
										{{Form::text('name', isset($category->name)? $category->name:null, array('class' => 'form-control', 'required' => 'required'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Slug</label>
									<div class="col-lg-10">
										{{Form::text('slug', isset($category->slug)? $category->slug:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<?php 
									$row = array();
									$row[0] = '';
									foreach($categoriesList as $categoryList){
										if(isset($category)){
											if($categoryList->id != $category->id){
												$row[$categoryList->id] = $categoryList->name;
											}
										}else{
											$row[$categoryList->id] = $categoryList->name;
										}
									}
								?>
								<?php if($row){ ?>
								<div class="form-group">
									<label class="control-label col-lg-2">Parent</label>
									<div class="col-lg-10">
										{{Form::select('parent', $row, isset($category->parent)? $category->parent:'0', array('class' => 'form-control'))}}
									</div>
								</div>
								<?php } ?>
								<div class="form-group">
									{{ Form::label('feat_image', 'Feature Image', array('class' => 'control-label col-lg-2')) }}
									<div class="col-lg-10">
										<div class="media no-margin-top">
											<div class="media-left">
												<a href="#"><img src="{{(!empty($category->feat_image)?$category->feat_image:'/assets/images/placeholder.jpg')}}" style="width: 58px; height: 58px;" class="img-rounded" alt="Your avatar"></a>
											</div>
											<div class="media-body">
												@include('media._popup', ['target_id' => 'mypopup'])
												<a href="{{ route('media.list') }}" class="btn btn-default btn-sm" data-toggle="modal" data-type="medialib" data-field="feat_image" data-target="#mypopup">Upload Avatar</a>
												{{ Form::hidden('feat_image', (!empty($category->feat_image)?$category->feat_image:''), array('id' => 'feat_image')) }}
												<!--<span class="help-block">Accepted formats: gif, png, jpg. Max file size 2Mb</span>-->
											</div>
										</div>
									</div>
  								</div>
								<div class="text-right">
									<button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
								</div>
						</fieldset>
{{ Form::close() }}