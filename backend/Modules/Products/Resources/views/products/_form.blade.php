<?php
	use \Illuminate\Support\Facades\Input;
	if(!isset($method)){
		$method = 'post';
	}

?>
<script type="text/javascript" src="<?php echo asset('assets/js/plugins/forms/styling/uniform.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo asset('assets/js/pages/form_layouts.js') ?>"></script>


{{ Form::open(array('action' => $action, 'method' => $method, 'class' => 'form-horizontal')) }}
<fieldset class="content-group">
							<legend class="text-bold"><?php if(isset($product)){ echo 'Edit product'; }else{ echo 'Create product';}?></legend>
								<div class="form-group">
									<label class="control-label col-lg-2">Product Series</label>
									<div class="col-lg-10">
										{{Form::text('series', isset($product->series)? $product->series:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Product name</label>
									<div class="col-lg-10">
										{{Form::text('name', isset($product->name)? $product->name:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Description</label>
									<div class="col-lg-10">
										{{Form::textarea('description', isset($product->description)? $product->description:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Short Description</label>
									<div class="col-lg-10">
										{{Form::text('short_description', isset($product->short_description)? $product->short_description:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Category</label>
									<div class="col-lg-10">
										<?php
											$row = array();
											foreach($productsCategory as $productCategory){
												$row[$productCategory->id] = $productCategory->name;
											}
										?>
										{{Form::select('category_id', $row, isset($product->category_id)? $product->category_id:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="form-group">
									{{ Form::label('feat_image', 'Feature Image', array('class' => 'control-label col-lg-2')) }}
									<div class="col-lg-10">
										<div class="media no-margin-top">
											<div class="media-left">
												<a href="#"><img src="{{(!empty($product->feat_image)?$product->feat_image:'/assets/images/placeholder.jpg')}}" style="width: 58px; height: 58px;" class="img-rounded" alt="Your avatar"></a>
											</div>
											<div class="media-body">
												@include('media._popup', ['target_id' => 'mypopup'])
												<a href="{{ route('media.list') }}" class="btn btn-default btn-sm" data-toggle="modal" data-type="medialib" data-field="feat_image" data-target="#mypopup">Upload Avatar</a>
												{{ Form::hidden('feat_image', (!empty($product->feat_image)?$product->feat_image:''), array('id' => 'feat_image')) }}
												<!--<span class="help-block">Accepted formats: gif, png, jpg. Max file size 2Mb</span>-->
											</div>
										</div>
									</div>
  								</div>
								<div class="text-right">
									<button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
								</div>
						</fieldset>
{{ Form::close() }}