@extends('layouts.app')

@section('header')
    @parent
    <!-- JS Extra for form -->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBdmLJ80f1HUSAG0YxQthnZ2hd_p60nnT4&v=3.exp"></script>

 <script type="text/javascript" src="{{asset('assets/js/maps/google/basic/basic.js')}}"></script>
 <script type="text/javascript" src="{{asset('assets/js/maps/google/basic/geolocation.js')}}"></script>
 <script type="text/javascript" src="{{asset('assets/js/maps/google/basic/coordinates.js')}}"></script>
 <script type="text/javascript" src="{{asset('assets/js/maps/google/basic/click_event.js')}}"></script>

 

 
 <script type="text/javascript" src="{{asset('assets/js/maps/google/markers/animation.js')}}"></script>
 <script type="text/javascript" src="{{asset('assets/js/maps/google/markers/symbols_predefined.js')}}"></script>
 <script type="text/javascript" src="{{asset('assets/js/maps/google/markers/symbols_custom.js')}}"></script>


<?php $success_message = session('success'); if($success_message){?>
		<script>
		jQuery(document).ready(function(){
			alert('<?php echo $success_message; ?>');
		});
		</script>
	<?php session()->forget('success'); } ?>

@endsection
@section('content')
<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Datatables</span> - Basic</h4>
						</div>

						<div class="heading-elements">
							<div class="heading-btn-group">
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Products</span></a>
							</div>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="{{route('dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="{{route('products.index')}}">Products</a></li>
							<li class="active">Add Category</li>
						</ul>

						<ul class="breadcrumb-elements">
							<li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-gear position-left"></i>
									Settings
									<span class="caret"></span>
								</a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
									<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
									<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->
				<div class="content">
					<div class="row">
						<div class="col-md-6">
							<div class="panel panel-flat">
								<div class="panel-body">
									@include('products::products._formCreateCategory', ['action' => '\Modules\Products\Http\Controllers\ProductsController@storeCategory', 'method' => 'post'])
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="panel panel-flat">
								<div class="panel-body">
								<legend>Category List</legend>
									<table class="table datatable-basic">
										<thead>
											<tr>
												<th style="text-align:center;">Name</th>
												<th style="text-align:center;">Slug</th>
												<th style="text-align:center;">Parent id</th>
												<th style="text-align:center;">Action</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach($categoriesList as $categoryList){?>
											<tr>
												<td style="text-align:center;"><?php echo $categoryList->name; ?></td>
												<td style="text-align:center;"><?php echo $categoryList->slug; ?></td>
												<td style="text-align:center;"><?php echo $categoryList->parent; ?></td>
												<td>
												<ul class="icons-list">
													<li class="dropdown">
														<a href="#" class="dropdown-toggle" data-toggle="dropdown">
															<i class="icon-menu9"></i>
														</a>

														<ul class="dropdown-menu dropdown-menu-right">
															<li><a href="{{ route('products.editCategory', ['id' => $categoryList->id]) }}"><i class="icon-cog5"></i> Edit Category </a></li>
															<li><a href="{{ route('products.destroyCategory', ['id' => $categoryList->id]) }}" data-method="delete" data-token="{{ csrf_token() }}"><i class="icon-cog5"></i> Delete Category </a></li>
														</ul>
													</li>
												</ul>
												</td>
											</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
@endsection
