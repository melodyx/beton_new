@extends('layouts.app')

@section('header')
    @parent
    <!-- JS Extra for form -->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBdmLJ80f1HUSAG0YxQthnZ2hd_p60nnT4&v=3.exp"></script>

 <script type="text/javascript" src="{{asset('assets/js/maps/google/basic/basic.js')}}"></script>
 <script type="text/javascript" src="{{asset('assets/js/maps/google/basic/geolocation.js')}}"></script>
 <script type="text/javascript" src="{{asset('assets/js/maps/google/basic/coordinates.js')}}"></script>
 <script type="text/javascript" src="{{asset('assets/js/maps/google/basic/click_event.js')}}"></script>

 

 
 <script type="text/javascript" src="{{asset('assets/js/maps/google/markers/animation.js')}}"></script>
 <script type="text/javascript" src="{{asset('assets/js/maps/google/markers/symbols_predefined.js')}}"></script>
 <script type="text/javascript" src="{{asset('assets/js/maps/google/markers/symbols_custom.js')}}"></script>
@endsection
@section('content')
<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Datatables</span> - Products</h4>
						</div>
						
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="{{route('dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="{{route('products.index')}}">Products</a></li>
							<li class="active">Create</li>
						</ul>

						<ul class="breadcrumb-elements">
							<li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-gear position-left"></i>
									Settings
									<span class="caret"></span>
								</a>
							</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->
				<div class="content">
				<div class="panel panel-flat">
				<div class="panel-body">
					@include('products::products._form', ['action' => '\Modules\Products\Http\Controllers\ProductsController@store'])
				</div>
				</div>
				</div>
@endsection