<?php $user = \Sentinel::getUser(); ?>
<?php //if($user != \Sentinel::inRole('sales')){ ?>
<?php if ($user->hasAccess(['products']) || $user->hasAccess('admin.login')){ ?>
<li>
	<a><i class="icon-users"></i><span>Products</span></a>
	<ul>
			<li><a href="{{route('products.index')}}">Products view</a></li>
			<li><a href="{{route('products.create')}}">Add Product</a></li>
			<li><a href="{{route('products.uploadFile')}}">Upload File</a></li>
			<li><a href="{{route('products.createCategory')}}">Add Category</a></li>
	</ul>
</li>
<?php } ?>