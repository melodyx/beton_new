@extends('layouts.app')
@section('header')

    @parent
	
	<?php $success_message = session('success'); if($success_message){?>
		<script>
		jQuery(document).ready(function(){
			alert('<?php echo $success_message; ?>');
		});
		</script>
	<?php session()->forget('success'); } ?>
	

@endsection
@section('content')
				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Datatables</span> - Products</h4>
						</div>

					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="{{route('dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
							<li class="active">Products</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->
				<div class="content">
					<div class="row">
						<div class="panel panel-flat">
						<table class="table datatable-basic">
						<thead>
							<tr>
								<th style="text-align:center;">Id</th>
								<th style="text-align:center;">Product Series</th>
								<th style="text-align:center;">Name</th>
								<th style="text-align:center;">Short Description</th>
								<th style="text-align:center;">Action</th>
							</tr>
						</thead>
						<tbody>	
							<?php if(isset($products)){ ?>
								<?php foreach($products as $product){?>
								<tr>
									<td style="text-align:center;"><?php if($product->id){echo $product->id;}else{ echo '-'; } ?></td>
									<td style="text-align:center;"><?php if($product->series){echo $product->series;}else{ echo '-'; } ?></td>
									<td style="text-align:center;"><?php if(isset($product->name)){echo $product->name;}else{ echo '-'; } ?></td>
									<td style="text-align:center;"><?php if(isset($product->short_description)){echo $product->short_description;}else{ echo '-'; } ?></td>
									<td>
										<?php if($current_user_role != 'sales') { ?>
											<ul class="icons-list">
												<li >
													<a href="#" class="dropdown-toggle" data-toggle="dropdown">
														<i class="icon-cog7"></i>
														<span class="caret"></span>
													</a>
													<ul class="dropdown-menu dropdown-menu-right">
														<li class="dropdown-header">Options</li>
														<li><a href="{{route('products.edit',$product->id)}}"><i class="icon-pencil7"></i>Edit entry</a></li>
														<li><a href="{{ route('products.destroy', $product->id)}}" data-method="delete" data-token="{{ csrf_token() }}"><i class="icon-bin"></i>Remove entry</a></li>
													</ul>
												</li>
											</ul>
										<?php } ?>
									</td>
								</tr>
								<?php } ?>
							<?php } ?>
						</tbody>
						</table>	
						</div>
					</div>
				</div>

@endsection