<?php

namespace Modules\Products\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ProductCategoryTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
		\DB::table('products_category')->insert([
			'name' => 'semen',
			'slug'              =>  'semen',
            'parent'              =>  '0',
            'created_at'        =>  Carbon::now()->format('Y-m-d H:i:s')
		]);
		\DB::table('products_category')->insert([
			'name' => 'panel',
			'slug'              =>  'panel',
            'parent'              =>  '0',
            'created_at'        =>  Carbon::now()->format('Y-m-d H:i:s')
		]);
		\DB::table('products_category')->insert([
			'name' => 'beton',
			'slug'              =>  'beton',
            'parent'              =>  '0',
            'created_at'        =>  Carbon::now()->format('Y-m-d H:i:s')
		]);
		\DB::table('products_category')->insert([
			'name' => 'pallet',
			'slug'              =>  'pallet',
            'parent'              =>  '0',
            'created_at'        =>  Carbon::now()->format('Y-m-d H:i:s')
		]);
		// $this->call("OthersTableSeeder");
	}
}
