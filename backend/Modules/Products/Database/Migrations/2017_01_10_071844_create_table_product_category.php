<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProductCategory extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_category', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name',255);
            $table->string('slug',255);
            $table->integer('parent');
            $table->string('feat_image',255);
            $table->timestamps();
        });

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    }
    
    public function down()
    {
        Schema::drop('products_category');
    }

}
