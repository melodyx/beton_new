<?php

Route::group(['middleware' => 'web', 'namespace' => 'Modules\Reports\Http\Controllers'], function()
{
	//Route::get('/', 'ReportsController@index');

	Route::get('reports/sellingReports','ReportsController@sellingReports')->name('reports.sellingReports');
	Route::get('reports/sellingDetails/{id}','ReportsController@sellingDetails')->name('reports.sellingDetails');
	Route::get('reports/invoiceReports','ReportsController@invoiceReports')->name('reports.invoiceReports');
	Route::get('reports/invoiceReportDetails/{id}','ReportsController@invoiceReportDetails')->name('reports.invoiceReportDetails');
});