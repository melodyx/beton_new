<?php

namespace Modules\Reports\Http\Controllers;

use Nwidart\Modules\Routing\Controller;
use Modules\Transactions\Entities\Order;
use Modules\Transactions\Entities\Payment;
use Modules\Invoices\Entities\Invoice;

class ReportsController extends Controller {

	public function index()
	{
		return view('reports::index');
	}

	public function sellingReports(){
		$sellingReports = Order::with('orderDetails','expedition')->where('status','=',Order::DONE)->get();

		foreach($sellingReports as &$sellingReport){
			if($sellingReport->orderDetails){
				$totalPrice = 0;
				foreach($sellingReport->orderDetails as $orderDetail){
					$totalPrice = $totalPrice + $orderDetail->price;
				}
				$sellingReport->totalPrice = $totalPrice;
			}
		}
		
		return view('reports::reports.sellingReports',['sellingReports' => $sellingReports]);
	}


	public function sellingDetails($id){
		$sellingReport = Order::with('orderDetails.product','expedition','customer')->find($id);
		return view('reports::reports.sellingDetails',['sellingReport' => $sellingReport]);
	}

	public function invoiceReports(){
		//$invoiceReports = Invoice::with('schedules','payments','customer')->where('status','=',Invoice::PAID)->get();

		$invoicePayments = Payment::with(array('schedule.debtCollector','invoice.customer'))->get();
		//dd($invoicePayments);
		return view('reports::reports.invoiceReports',['invoicePayments' => $invoicePayments]);
	}

	public function invoiceReportDetails($id){
		$invoiceReport = Invoice::with(array('schedules','payments.schedule.debtCollector','customer'))->find($id);

		return view('reports::reports.invoiceReportDetails',['invoiceReport' => $invoiceReport]);
	}

}
