<?php $user = \Sentinel::getUser(); ?>

<?php if ($user->hasAccess(['salesReports']) || $user->hasAccess(['invoiceReports']) || $user->hasAccess('admin.login')){ ?>
<li>
	<a><i class="icon-users"></i> <span>Reports</span></a>
	<ul>
		<?php if($user->hasAccess(['salesReports']) || $user->hasAccess('admin.login')){ ?>
			<li><a href="{{route('reports.sellingReports')}}">Selling Report</a></li>
		<?php } ?>
		<?php if($user->hasAccess(['invoiceReports']) || $user->hasAccess('admin.login')){ ?>
			<li><a href="{{route('reports.invoiceReports')}}">Invoice Report</a></li>
		<?php } ?>
	</ul>
</li>
<?php } ?>
