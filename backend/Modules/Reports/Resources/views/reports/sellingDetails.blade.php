@extends('layouts.app')
@section('header')

    @parent
	
	<?php $success_message = session('success'); if($success_message){?>
		<script>
		jQuery(document).ready(function(){
			alert('<?php echo $success_message; ?>');
		});
		</script>
	<?php session()->forget('success'); } ?>
	
	<style>
		.clear{clear:both;}
	</style>

@endsection
@section('content')
				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Datatables</span> - Report Details</h4>
						</div>

					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="{{route('dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
							<li class="active">Selling Report Details</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->
				<div class="content">
					<div class="panel panel-flat">
						<div class="panel-body">
							<div class="row">
								<legend>Order Data</legend>
								<div class="form-group">
									<label class="control-label col-lg-2">Order Id</label>
									<div class="col-lg-10">
										: <?php if($sellingReport->id){
											echo $sellingReport->id;
										}?>
									</div>
									<div class="clear"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Destination Name</label>
									<div class="col-lg-10">
										: <?php if($sellingReport->destination_name){
											echo $sellingReport->destination_name;
										}?>
									</div>
									<div class="clear"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Destination Address</label>
									<div class="col-lg-10">
										: <?php if($sellingReport->address){
											echo $sellingReport->address;
										}?>
									</div>
									<div class="clear"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Destination City</label>
									<div class="col-lg-10">
										: <?php if($sellingReport->city){
											echo $sellingReport->city;
										}?>
									</div>
									<div class="clear"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Sales ID</label>
									<div class="col-lg-10">
										: <?php if($sellingReport->author_id){
											echo $sellingReport->author_id;
										}?>
									</div>
									<div class="clear"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Sales Name</label>
									<div class="col-lg-10">
										: <?php if($sellingReport->author){
											echo $sellingReport->author->first_name.' '.$sellingReport->author->last_name;
										}?>
									</div>
									<div class="clear"></div>
								</div>
							</div>
							<div class="row">
								<legend>Order Details</legend>
								<table class="table datatable-basic">
									<thead>
										<tr>
											<th style="text-align:center;">ID</th>
											<th style="text-align:center;">Product name</th>
											<th style="text-align:center;">Quantity</th>
											<th style="text-align:center;">Price</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach($sellingReport->orderDetails as $orderDetail){ ?>
										<tr>
											<td style="text-align:center;"><?php if($orderDetail->id){echo $orderDetail->id; }else{echo '-'; } ?></td>
											<td style="text-align:center;"><?php if($orderDetail->product->name){echo $orderDetail->product->name; }else{echo '-'; } ?></td>
											<td style="text-align:center;"><?php if($orderDetail->product_qty){echo $orderDetail->product_qty; }else{echo '-'; } ?></td>
											<td style="text-align:center;"><?php if($orderDetail->price){echo number_format($orderDetail->price,2); }else{echo '-'; } ?></td>
										</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
							<div class="row">
								<legend>Customer Data</legend>
								<div class="form-group">
									<label class="control-label col-lg-2">Customer Name</label>
									<div class="col-lg-10">
										: <?php if($sellingReport->customer->name){
											echo $sellingReport->customer->name;
										}?>
									</div>
									<div class="clear"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Customer Address</label>
									<div class="col-lg-10">
										: <?php if($sellingReport->customer->addressline1){
											echo $sellingReport->customer->addressline1;
										}?>
									</div>
									<div class="clear"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Customer Phone</label>
									<div class="col-lg-10">
										: <?php if($sellingReport->customer->phone){
											echo $sellingReport->customer->phone;
										}?>
									</div>
									<div class="clear"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Customer Status</label>
									<div class="col-lg-10">
										: <?php if($sellingReport->customer->customertypeid){
											echo $sellingReport->customer->customertypeid;
										}?>
									</div>
									<div class="clear"></div>
								</div>
							</div>
						</div>
					</div>
				</div>

@endsection