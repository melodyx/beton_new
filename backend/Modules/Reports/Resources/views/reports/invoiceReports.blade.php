@extends('layouts.app')
@section('header')

    @parent
	
	<?php $success_message = session('success'); if($success_message){?>
		<script>
		jQuery(document).ready(function(){
			alert('<?php echo $success_message; ?>');
		});
		</script>
	<?php session()->forget('success'); } ?>
	

@endsection
@section('content')
				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Datatables</span> - Report</h4>
						</div>

					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="{{route('dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
							<li class="active">Invoice Reports</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->
				<div class="content">
					<div class="row">
						<div class="panel panel-flat">
						<table class="table datatable-basic">
						<thead>
							<tr>
								<th style="text-align:center;">Debt Collector</th>
								<th style="text-align:center;">Invoice ID</th>
								<th style="text-align:center;">Customer Name</th>
								<th style="text-align:center;">Total Debt</th>
								<th style="text-align:center;">Payment Method</th>
								<th style="text-align:center;">Paid Value</th>
								<th style="text-align:center;">Date</th>
							</tr>
						</thead>
						<tbody>	
							<?php if(isset($invoicePayments)){ ?>
								<?php foreach($invoicePayments as $invoicePayment){?>
								<tr>
									<td style="text-align:center;"><?php if($invoicePayment->schedule->debtCollector){echo $invoicePayment->schedule->debtCollector->first_name.' '.$invoicePayment->schedule->debtCollector->last_name;}else{ echo '-'; } ?></td>
									<td style="text-align:center;"><?php if($invoicePayment->invoice->id){echo $invoicePayment->invoice->id;}else{ echo '-'; } ?></td>
									<td style="text-align:center;"><?php if($invoicePayment->invoice->customer->name){echo $invoicePayment->invoice->customer->name;}else{ echo '-'; } ?></td>
									<td style="text-align:center;"><?php if($invoicePayment->invoice->total_debt){echo number_format($invoicePayment->invoice->total_debt,2);}else{ echo '-'; } ?></td>
									<td style="text-align:center;"><?php if($invoicePayment->payment_method){echo $invoicePayment->payment_method;}else{ echo '-'; } ?></td>
									<td style="text-align:center;"><?php if($invoicePayment->payment){echo number_format($invoicePayment->payment,2);}else{ echo '-'; } ?></td>
									<td style="text-align:center;"><?php if($invoicePayment->created_at){echo $invoicePayment->created_at;}else{ echo '-'; } ?></td>
								</tr>
								<?php } ?>
							<?php } ?>
						</tbody>
						</table>
						<?php /*<table class="table datatable-basic">
						<thead>
							<tr>
								<th style="text-align:center;">Invoice Id</th>
								<th style="text-align:center;">Invoice Number</th>
								<th style="text-align:center;">Customer Name</th>
								<th style="text-align:center;">Total Debt</th>
								<th style="text-align:center;">Total Paid</th>
								<th style="text-align:center;">Action</th>
							</tr>
						</thead>
						<tbody>	
							<?php if(isset($invoiceReports)){ ?>
								<?php foreach($invoiceReports as $invoiceReport){?>
								<tr>
									<td style="text-align:center;"><?php if($invoiceReport->id){echo $invoiceReport->id;}else{ echo '-'; } ?></td>
									<td style="text-align:center;"><?php if($invoiceReport->invoice_number){echo $invoiceReport->invoice_number;}else{ echo '-'; } ?></td>
									<td style="text-align:center;"><?php if($invoiceReport->customer){echo $invoiceReport->customer->name;}else{ echo '-'; } ?></td>
									<td style="text-align:center;"><?php if($invoiceReport->total_debt){echo $invoiceReport->total_debt;}else{ echo '-'; } ?></td>
									<td style="text-align:center;"><?php if($invoiceReport->total_paid){echo $invoiceReport->total_paid;}else{ echo '-'; } ?></td>
									<td style="text-align:center;">
										<ul class="icons-list">
											<li>
												<a href="#" class="dropdown-toggle" data-toggle="dropdown">
													<i class="icon-cog7"></i>
													<span class="caret"></span>
												</a>
												<ul class="dropdown-menu dropdown-menu-right">
													<li class="dropdown-header">Options</li>
													<li><a href="{{route('reports.invoiceReportDetails',$invoiceReport->id)}}"><i class="icon-pencil7"></i>Details</a></li>
												</ul>
											</li>
										</ul>
									</td>
								</tr>
								<?php } ?>
							<?php } ?>
						</tbody>
						</table> */?>
						</div>
					</div>
				</div>

@endsection