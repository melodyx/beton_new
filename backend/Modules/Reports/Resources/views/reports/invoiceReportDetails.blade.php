@extends('layouts.app')
@section('header')

    @parent
	
	<?php $success_message = session('success'); if($success_message){?>
		<script>
		jQuery(document).ready(function(){
			alert('<?php echo $success_message; ?>');
		});
		</script>
	<?php session()->forget('success'); } ?>
	
	<style>
		.clear{clear:both;}
	</style>

@endsection
@section('content')
				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Datatables</span> - Report Details</h4>
						</div>

					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="{{route('dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
							<li class="active">Invoice Report Details</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->
				<div class="content">
					<div class="panel panel-flat">
						<div class="panel-body">
							<div class="row">
								<legend>Invoice Data</legend>
								<div class="form-group">
									<label class="control-label col-lg-2">Invoice Id</label>
									<div class="col-lg-10">
										: <?php if($invoiceReport->id){
											echo $invoiceReport->id;
										}?>
									</div>
									<div class="clear"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Invoice Number</label>
									<div class="col-lg-10">
										: <?php if($invoiceReport->invoice_number){
											echo $invoiceReport->invoice_number;
										}?>
									</div>
									<div class="clear"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Customer Name</label>
									<div class="col-lg-10">
										: <?php if($invoiceReport->customer){
											echo $invoiceReport->customer->name;
										}?>
									</div>
									<div class="clear"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Total Debt</label>
									<div class="col-lg-10">
										: <?php if($invoiceReport->total_debt){
											echo number_format((float)$invoiceReport->total_debt,2);
										}?>
									</div>
									<div class="clear"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Total Paid</label>
									<div class="col-lg-10">
										: <?php if($invoiceReport->total_paid){
											echo number_format((float)$invoiceReport->total_paid,2);
										}?>
									</div>
									<div class="clear"></div>
								</div>
							</div>
							<div class="row">
								<legend>Paymnent Details</legend>
								<table class="table datatable-basic">
									<thead>
										<tr>
											<th style="text-align:center;">Payment ID</th>
											<th style="text-align:center;">Debt Collector</th>
											<th style="text-align:center;">Payment Method</th>
											<th style="text-align:center;">Payment Value</th>
											<th style="text-align:center;">Date</th>
											<th style="text-align:center;">Reason</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach($invoiceReport->payments as $payment){ ?>
										<tr>
											<td style="text-align:center;"><?php if($payment->id){echo $payment->id; }else{echo '-'; } ?></td>
											<td style="text-align:center;"><?php if($payment->schedule->debtCollector){echo $payment->schedule->debtCollector->first_name.' '.$payment->schedule->debtCollector->last_name; }else{echo '-'; } ?></td>
											<td style="text-align:center;"><?php if($payment->payment_method){echo $payment->payment_method; }else{echo '-'; } ?></td>
											<td style="text-align:center;"><?php if($payment->payment){echo number_format($payment->payment,2); }else{echo '-'; } ?></td>
											<td style="text-align:center;"><?php if($payment->created_at){echo $payment->created_at; }else{echo '-'; } ?></td>
											<td style="text-align:center;"><?php if($invoiceReport->reason){echo $invoiceReport->reason; }else{echo '-'; } ?></td>
										</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
							<div class="row">
								<legend>Customer Data</legend>
								<div class="form-group">
									<label class="control-label col-lg-2">Customer Name</label>
									<div class="col-lg-10">
										: <?php if($invoiceReport->customer->name){
											echo $invoiceReport->customer->name;
										}?>
									</div>
									<div class="clear"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Customer Address</label>
									<div class="col-lg-10">
										: <?php if($invoiceReport->customer->addressline1){
											echo $invoiceReport->customer->addressline1;
										}?>
									</div>
									<div class="clear"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Customer Phone</label>
									<div class="col-lg-10">
										: <?php if($invoiceReport->customer->phone){
											echo $invoiceReport->customer->phone;
										}?>
									</div>
									<div class="clear"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Customer Status</label>
									<div class="col-lg-10">
										: <?php if($invoiceReport->customer->customertypeid){
											echo $invoiceReport->customer->customertypeid;
										}?>
									</div>
									<div class="clear"></div>
								</div>
							</div>
						</div>
					</div>
				</div>

@endsection