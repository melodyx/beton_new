@extends('layouts.app')
@section('header')

    @parent
	
	<?php $success_message = session('success'); if($success_message){?>
		<script>
		jQuery(document).ready(function(){
			alert('<?php echo $success_message; ?>');
		});
		</script>
	<?php session()->forget('success'); } ?>
	

@endsection
@section('content')
				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Datatables</span> - Report</h4>
						</div>

					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="{{route('dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
							<li class="active">Selling Reports</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->
				<div class="content">
					<div class="row">
						<div class="panel panel-flat">
						<table class="table datatable-basic">
						<thead>
							<tr>
								<th style="text-align:center;">Sales Id</th>
								<th style="text-align:center;">Sales Name</th>
								<th style="text-align:center;">Customer Name</th>
								<th style="text-align:center;">Destination Name</th>
								<th style="text-align:center;">Shipping Address</th>
								<th style="text-align:center;">Total Price</th>
								<th style="text-align:center;">Action</th>
							</tr>
						</thead>
						<tbody>	
							<?php if(isset($sellingReports)){ ?>
								<?php foreach($sellingReports as $sellingReport){?>
								<tr>
									<td style="text-align:center;"><?php if($sellingReport->author_id){echo $sellingReport->author_id;}else{ echo '-'; } ?></td>
									<td style="text-align:center;"><?php if($sellingReport->author){echo $sellingReport->author->first_name.' '.$sellingReport->author->last_name;}else{ echo '-'; } ?></td>
									<td style="text-align:center;"><?php if($sellingReport->customer){echo $sellingReport->customer->name;}else{ echo '-'; } ?></td>
									<td style="text-align:center;"><?php if($sellingReport->destination_name){echo $sellingReport->destination_name;}else{ echo '-'; } ?></td>
									<td style="text-align:center;"><?php if($sellingReport->address){echo $sellingReport->address.', '.$sellingReport->city;}else{ echo '-'; } ?></td>
									<td style="text-align:center;"><?php if($sellingReport->totalPrice){echo number_format($sellingReport->totalPrice,2);}else{ echo '-'; } ?></td>
									<td>
										<ul class="icons-list">
											<li >
												<a href="#" class="dropdown-toggle" data-toggle="dropdown">
													<i class="icon-cog7"></i>
													<span class="caret"></span>
												</a>
												<ul class="dropdown-menu dropdown-menu-right">
													<li class="dropdown-header">Options</li>
													<li><a href="{{route('reports.sellingDetails',$sellingReport->id)}}"><i class="icon-pencil7"></i>Details</a></li>
												</ul>
											</li>
										</ul>
									</td>
								</tr>
								<?php } ?>
							<?php } ?>
						</tbody>
						</table>	
						</div>
					</div>
				</div>

@endsection