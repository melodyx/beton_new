<?php

Route::group(['middleware' => 'web', 'prefix' => 'testdummy', 'namespace' => 'Modules\TestDummy\Http\Controllers'], function()
{
	Route::get('/', 'TestDummyController@index');
});