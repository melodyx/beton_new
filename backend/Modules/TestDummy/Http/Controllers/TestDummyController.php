<?php

namespace Modules\Testdummy\Http\Controllers;

use Nwidart\Modules\Routing\Controller;

class TestDummyController extends Controller {

	public function index()
	{
		return view('testdummy::index');
	}

}
