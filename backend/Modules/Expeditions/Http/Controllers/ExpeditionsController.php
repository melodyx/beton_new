<?php

namespace Modules\Expeditions\Http\Controllers;

use Nwidart\Modules\Routing\Controller;
use Modules\Expeditions\Entities\Expedition;
use Modules\Expeditions\Entities\ExpeditionCategory;
use Illuminate\Http\Request;


class ExpeditionsController extends Controller {

	public function index()
	{
		// checking role
		$current_user_role = $this->checkRole();
		if($current_user_role == 'notLogin'){
			return redirect()->route('login')->with('success','you dont have authorization for access this page');
		}
		// =======================
		$expeditions = array();
		$expeditions = Expedition::with('category')->get();
		//dd($expeditions);
		return view('expeditions::index',['expeditions' => $expeditions, 'current_user_role' => $current_user_role]);
	}

	public function create(){
		// checking role
		$current_user_role = $this->checkRole();
		if($current_user_role == 'notLogin'){
			return redirect()->route('login')->with('success','you dont have authorization for access this page');
		}
		// =======================

		$expeditionsCategory = ExpeditionCategory::all();
		//dd($expeditionsCategory);
		return view('expeditions::Expeditions.create',['expeditionsCategory' => $expeditionsCategory]);
	}

	public function store(Request $request){
		$data = $request->all();

		$expedition = Expedition::saveExpedition(null,$data);

		return redirect()->route('expeditions.index')->with('success','Successfully add new expediton');
	}

	public function edit($id){
		// checking role
		$current_user_role = $this->checkRole();
		if($current_user_role == 'notLogin'){
			return redirect()->route('login')->with('success','you dont have authorization for access this page');
		}
		// =======================
		$expedition = Expedition::find($id);
		$expeditionsCategory = ExpeditionCategory::all();

		return view('expeditions::Expeditions.edit',['expedition' => $expedition, 'expeditionsCategory' => $expeditionsCategory]);
	}

	public function update($id, Request $request){
		$data = $request->all();
		$expedition = Expedition::saveExpedition($id, $data);

		return redirect()->route('expeditions.index')->with('success','Successfully edit expediton');
	}

	public function createCategory(){
		$current_user_role = $this->checkRole();
		if($current_user_role == 'notLogin' || $current_user_role == 'sales'){
			return redirect()->route('login')->with('success','you dont have authorization for access this page');
		}

		$categoriesList = ExpeditionCategory::all();
		return view('expeditions::Expeditions.createCategory',['categoriesList' => $categoriesList]);
	}

	public function storeCategory(Request $request){
		$data = $request->all();
		$category = ExpeditionCategory::saveCategory(null, $data);
		
		// update activity log
		$current_user = \Sentinel::getUser();
		$log = array(
				'desc' => $current_user->id.' Adding category '.$data['name']
		);
		event(new \App\Events\UpdateData($category, $log));

		return redirect()->route('expeditions.createCategory')->with('success', 'Category has been successfully added');
	}

	public function editCategory($id){
		// checking role
		$current_user_role = $this->checkRole();
		if($current_user_role == 'notLogin' || $current_user_role == 'sales'){
			return redirect()->route('login')->with('success','you dont have authorization for access this page');
		}
		// =======================
		$categoriesList = ExpeditionCategory::all();
		$category = ExpeditionCategory::find($id);
		return view('expeditions::Expeditions.editCategory', ['category' => $category, 'categoriesList' => $categoriesList]);
	}

	public function updateCategory($id,Request $request){
		$data = $request->all();
		//dd($data);
		$category = ExpeditionCategory::saveCategory($id, $data);

		return redirect()->route('expeditions.createCategory')->with('success', 'Category has been successfully added');
	}

	public function destroyCategory($id){
		\DB::beginTransaction(); //Start transaction!
		try{
			$category = ExpeditionCategory::find($id);
			$category->delete();
		}catch(\Exception $e){
			\DB::rollback();	
		}
		\DB::commit();
		// update activity log
		$current_user = \Sentinel::getUser();
		$log = array(
				'desc' => $current_user->id.' Deleting product '.$category->name
		);
		event(new \App\Events\UpdateData($category, $log));
		return array(
            'url' => route('expeditions.createCategory'),
            'message' => 'Something went wrong while trying to remove the product!'
        );
	}

	public function uploadFile(){
		return view('expeditions::Expeditions.uploadFile');
	}

	public function storeFile(Request $request){
		$data = $request->all();
		\DB::beginTransaction();
		try {
			if(isset($data['csv_file'])){
			$handle = fopen($data['csv_file'], 'r');
			}
			$csvArray = array();
			if(isset($handle)){
				while (($csv = fgetcsv($handle, 1000, ",")) !== FALSE) {
					$checkExpedition = Expedition::where('destination','=',$csv[1])->first();
					if(!isset($checkExpedition)){
						$expedition = new Expedition();
						$expedition->destination = $csv[1];
						$expedition->kap = $csv[2];
						$expedition->dilivery_price = $csv[3];
						$expedition->loco_price = $csv[4];
						if($csv[5] == 'coltdiesel'){
							$expedition->category_id = 1;
						}elseif($csv[5] == 'tronton'){
							$expedition->category_id = 2;
						}
						$expedition->save();
					}else{
						$checkExpedition->dilivery_price = $csv[3];
						$checkExpedition->loco_price = $csv[4];
						$checkExpedition->save();
					}

				}
			}
		} catch (\Exception $e) {
			\DB::rollback();
			return redirect()->route('expeditions.index')->with('succeess','failed upload data expeditions');
		}
		\DB::commit();

		return redirect()->route('expeditions.index')->with('success','successfully upload data expeditions');
		
	}

	public function destroy($id){

		
		$expedition = Expedition::find($id);
		$expedition->delete();

		
		 return array(
            'url' => route('expeditions.index'),
            'message' => 'Something went wrong while trying to remove the product!'
        );
	}

	public function checkRole(){
		$user = \Sentinel::getUser();
		if($user){
            $current_user['superadmin'] = \Sentinel::inRole('superadmin');
            $current_user['admin'] = \Sentinel::inRole('admin');
            $current_user['sales'] = \Sentinel::inRole('sales');
            $current_user['gm'] = \Sentinel::inRole('gm');
            $current_user['bm'] = \Sentinel::inRole('bm');
            $current_user['debtCollector'] = \Sentinel::inRole('debtCollector');
            $current_user['customer'] = \Sentinel::inRole('customer');
			
			if($current_user['superadmin'] == true){
				return 'superadmin';
			}elseif($current_user['admin'] == true){
				return 'admin';
			}elseif($current_user['sales'] == true){
				return 'sales';
			}elseif($current_user['gm'] == true){
				return 'gm';
			}elseif($current_user['bm'] == true){
				return 'bm';
			}elseif($current_user['debtCollector'] == true){
				return 'debtCollector';
			}

		}else{
			return 'notLogin';
		}
	}	



}
