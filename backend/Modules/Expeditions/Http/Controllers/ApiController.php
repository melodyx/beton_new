<?php

namespace Modules\Expeditions\Http\Controllers;

use Nwidart\Modules\Routing\Controller;
use Chrisbjr\ApiGuard\Http\Controllers\ApiGuardController;
use Modules\Users\Entities\User;
use Modules\Customers\Entities\Customer;
use Modules\Expeditions\Entities\Expedition;
use Modules\Expeditions\Entities\ExpeditionCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Validator;
use Sentinel;
use Reminder, Mail;

class ApiController extends ApiGuardController {


    /**
     * @SWG\Get(
     *   path="/expeditions/getAll",
     *   summary="List of all customer with discount",
     *   description="-",
     *   tags={"expeditions"},
     *   @SWG\Response(
     *     response=200,
     *     description="True / False after submit",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Error Message",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   )
     * )
     */
    public function getAll(){
        try{

            $response = array('status' => 'error', 'code' => 400, 'data' => false);

            $coltDieselCat = ExpeditionCategory::where('slug','=','coltdiesel')->first();
            $expeditions = Expedition::where('category_id','=',$coltDieselCat->id)->get();

            if(isset($expeditions)){
                $response['status'] = 'success';
                $response['code'] = '200';
                $response['data'] = $expeditions;
            }else{
                $response['data'] = 'no expeditions data';
            }
            
        }catch(\Exception $e) {
            $response = array('status' => 'error', 'code' => 500, 'data' => $e->getMessage());
        }

        return Response::json($response);
    }

    

  

    
}
