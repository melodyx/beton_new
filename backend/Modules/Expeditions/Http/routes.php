<?php

Route::group(['middleware' => 'web', 'namespace' => 'Modules\Expeditions\Http\Controllers'], function()
{
	//Route::get('/', 'ExpeditionsController@index');
	Route::get('expeditions/createCategory','ExpeditionsController@createCategory')->name('expeditions.createCategory');
	Route::post('expeditions/storeCategory','ExpeditionsController@storeCategory')->name('expeditions.storeCategory');
	Route::get('expeditions/editCategory/{id}','ExpeditionsController@editCategory')->name('expeditions.editCategory');
	Route::put('expeditions/updateCategory/{id}','ExpeditionsController@updateCategory')->name('expeditions.updateCategory');
	Route::delete('expeditions/destroyCategory/{id}','ExpeditionsController@destroyCategory')->name('expeditions.destroyCategory');
	Route::get('expeditions/uploadFile','ExpeditionsController@uploadFile')->name('expeditions.uploadFile');
	Route::post('expeditions/storeFile','ExpeditionsController@storeFile')->name('expeditions.storeFile');
	Route::resource('expeditions','ExpeditionsController');

	Route::group(['middleware' => ['apiAuth'], 'prefix' => 'api/v1'], function() {
		Route::get('/expeditions/getAll','ApiController@getAll');
	});
});