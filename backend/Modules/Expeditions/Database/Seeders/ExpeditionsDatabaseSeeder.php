<?php

namespace Modules\Expeditions\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ExpeditionsDatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
		$this->call(ExpeditionsCategoryTableSeeder::class);
		// $this->call("OthersTableSeeder");
	}
}
