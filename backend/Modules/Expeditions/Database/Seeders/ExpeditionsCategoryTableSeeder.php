<?php

namespace Modules\Expeditions\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ExpeditionsCategoryTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
		\DB::table('expeditions_category')->insert([
			'name' => 'Colt Diesel',
			'slug'              =>  'coltdiesel',
            'parent'              =>  '0',
            'created_at'        =>  Carbon::now()->format('Y-m-d H:i:s')
		]);

		\DB::table('expeditions_category')->insert([
			'name' => 'Tronton',
			'slug'              =>  'tronton',
            'parent'              =>  '0',
            'created_at'        =>  Carbon::now()->format('Y-m-d H:i:s')
		]);
		// $this->call("OthersTableSeeder");
	}
}
