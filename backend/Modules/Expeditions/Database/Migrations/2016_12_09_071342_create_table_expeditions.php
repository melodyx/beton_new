<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableExpeditions extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expeditions', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('destination',255);
            $table->decimal('kap',10,3);
            $table->decimal('dilivery_price',14,2);
            $table->decimal('loco_price',14,2);
            $table->integer('category_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('expeditions');
    }

}
