<?php

namespace Modules\Expeditions\Entities;

use Illuminate\Database\Eloquent\Model;

class ExpeditionCategory extends Model
{
    protected $fillable = [];
    protected $table = 'expeditions_category';

    public function Expeditions(){
    	return view('\Modules\Expeditions\Entities\Expedition','category_id');
    }

    public static function saveCategory($id, $data){
		\DB::beginTransaction();
		try {
			if(is_null($id)){
				$category = new ExpeditionCategory();
			}else{
				$category = ExpeditionCategory::find($id);
			}
			$category->name = $data['name'];

			if($data['slug']){
				$slug = strtolower(str_replace(' ', '_', $data['slug']));
			}else{
				$slug = strtolower(str_replace(' ', '_', $data['name']));
			}

			if(is_null($id)){
				$slugCategory = ExpeditionCategory::where('slug','=',$slug)->first();
				if(isset($slugCategory)){
					$category->slug = $slug.'_1';
				}else{
					$category->slug = $slug;
				}
			}else{
				if($slug == $category->getOriginal('slug')){
					$category->slug = $slug;
				}else{
					$slugCategory = ExpeditionCategory::where('slug','=',$slug)->first();
					if(isset($slugCategory)){
						$category->slug = $slug.'_1';
					}else{
						$category->slug = $slug;
					}
				}
			}

			if(isset($data['parent'])){
				$category->parent = $data['parent'];
			}

			$category->feat_image = $data['feat_image'];
			$category->save();
		} catch (\Exception $e) {
			\DB::rollback();
			return false;
		}
		\DB::commit();
		return $category;
	}
}
