<?php

namespace Modules\Expeditions\Entities;

use Illuminate\Database\Eloquent\Model;

class Expedition extends Model
{
    protected $fillable = [];

    public function category(){
    	return $this->belongsTo('\Modules\Expeditions\Entities\ExpeditionCategory','category_id');
    }

    public static function saveExpedition($id, $data){
    	\DB::beginTransaction();
    	try {
    		if(is_null($id)){
    			$expedition = new Expedition();
    		}else{
    			$expedition = Expedition::find($id);
    		}
    		$expedition->destination = $data['destination'];
    		$expedition->kap = $data['kap'];
    		$expedition->dilivery_price = $data['dilivery_price'];
    		$expedition->loco_price = $data['loco_price'];
    		$expedition->category_id = $data['category_id'];
    		$expedition->save();

    	} catch (\Exception $e) {
    		\DB::rollback();
    		return redirect()->route('expeditions.index')->with('success',$e->getMessage());
    	}
    	\DB::commit();

    	return $expedition;
    }

}
