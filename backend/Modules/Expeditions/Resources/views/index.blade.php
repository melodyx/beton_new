@extends('layouts.app')
@section('header')

    @parent
	
	<?php $success_message = session('success'); if($success_message){?>
		<script>
		jQuery(document).ready(function(){
			alert('<?php echo $success_message; ?>');
		});
		</script>
	<?php session()->forget('success'); } ?>
	

@endsection
@section('content')
				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Datatables</span> - Expeditions</h4>
						</div>

					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="{{route('dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
							<li class="active">Expeditions</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->
				<div class="content">
					<div class="row">
						<div class="panel panel-flat">
						<table class="table datatable-basic">
						<thead>
							<tr>
								<th style="text-align:center;">Id</th>
								<th style="text-align:center;">Tujuan</th>
								<th style="text-align:center;">Kap</th>
								<th style="text-align:center;">Ongkos Kirim</th>
								<th style="text-align:center;">Harga Loco</th>
								<th style="text-align:center;">Category</th>
								<th style="text-align:center;">Action</th>
							</tr>
						</thead>
						<tbody>	
							<?php if(isset($expeditions)){ ?>
								<?php foreach($expeditions as $expedition){?>
								<tr>
									<td style="text-align:center;"><?php if($expedition->id){echo $expedition->id;}else{ echo '-'; } ?></td>
									<td style="text-align:center;"><?php if($expedition->destination){echo $expedition->destination;}else{ echo '-'; } ?></td>
									<td style="text-align:center;"><?php if(isset($expedition->kap)){echo $expedition->kap;}else{ echo '-'; } ?></td>
									<td style="text-align:center;"><?php if(isset($expedition->dilivery_price)){echo number_format($expedition->dilivery_price,2);}else{ echo '-'; } ?></td>
									<td style="text-align:center;"><?php if(isset($expedition->loco_price)){echo number_format($expedition->loco_price,2);}else{ echo '-'; } ?></td>
									<td style="text-align:center;"><?php if(isset($expedition->category->name)){echo $expedition->category->name;}else{ echo '-'; } ?></td>
									<td style="text-align:center;">	
										<?php if($current_user_role != 'sales') { ?>
											<ul class="icons-list">
												<li >
													<a href="#" class="dropdown-toggle" data-toggle="dropdown">
														<i class="icon-cog7"></i>
														<span class="caret"></span>
													</a>
													<ul class="dropdown-menu dropdown-menu-right">
														<li class="dropdown-header">Options</li>
														<li><a href="{{route('expeditions.edit',$expedition->id)}}"><i class="icon-pencil7"></i>Edit entry</a></li>
														<li><a href="{{ route('expeditions.destroy', $expedition->id)}}" data-method="delete" data-token="{{ csrf_token() }}"><i class="icon-bin"></i>Remove entry</a></li>
													</ul>
												</li>
											</ul>
										<?php } ?>
									</td>
								</tr>
								<?php } ?>
							<?php } ?>
						</tbody>
						</table>	
						</div>
					</div>
				</div>

@endsection