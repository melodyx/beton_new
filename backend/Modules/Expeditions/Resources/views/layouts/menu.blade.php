<?php $user = \Sentinel::getUser(); ?>
<?php
if ($user->hasAccess(['expeditions']) || $user->hasAccess('admin.login')){ ?>
<li><a><i class="icon-pencil3"></i>Expeditions</a>
	<ul>
		<li><a href="{{route('expeditions.index')}}">Expeditions List</a></li>
		<li><a href="{{route('expeditions.create')}}">Add Expedition</a></li>
		<li><a href="{{route('expeditions.createCategory')}}">Add Category</a></li>
		<li><a href="{{route('expeditions.uploadFile')}}">Upload file</a></li>
	</ul>
</li>
<?php } ?>
