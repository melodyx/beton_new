<?php
	use \Illuminate\Support\Facades\Input;
	if(!isset($method)){
		$method = 'post';
	}

?>
<script type="text/javascript" src="<?php echo asset('assets/js/plugins/forms/styling/uniform.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo asset('assets/js/pages/form_layouts.js') ?>"></script>


{{ Form::open(array('action' => $action, 'method' => $method, 'class' => 'form-horizontal')) }}
<fieldset class="content-group">
							<legend class="text-bold"><?php if(isset($expedition)){ echo 'Edit expedition'; }else{ echo 'Create expedition';}?></legend>
								<div class="form-group">
									<label class="control-label col-lg-2">Kota tujuan</label>
									<div class="col-lg-10">
										{{Form::text('destination', isset($expedition->destination)? $expedition->destination:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Kap</label>
									<div class="col-lg-10">
										{{Form::text('kap', isset($expedition->kap)? $expedition->kap:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Ongkos kirim</label>
									<div class="col-lg-10">
										{{Form::number('dilivery_price', isset($expedition->dilivery_price)? $expedition->dilivery_price:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Harga Loco</label>
									<div class="col-lg-10">
										{{Form::number('loco_price', isset($expedition->loco_price)? $expedition->loco_price:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Category</label>
									<div class="col-lg-10">
										<?php
											$row = array();
											foreach($expeditionsCategory as $expedtionCategory){
												$row[$expedtionCategory->id] = $expedtionCategory->name;
											}
										?>
										{{Form::select('category_id', $row, isset($expedition->category_id)? $expedition->category_id:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="text-right">
									<button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
								</div>
						</fieldset>
{{ Form::close() }}