<?php

namespace Modules\Users\Http\Controllers;

use League\Flysystem\Exception;
use Modules\Products\Entities\Product;
use Nwidart\Modules\Routing\Controller;
use Chrisbjr\ApiGuard\Http\Controllers\ApiGuardController;
use Modules\Users\Entities\User;
use Modules\Customers\Entities\Customer;
use Modules\Transactions\Entities\Order;
use Modules\Transactions\Entities\OrderDetail;
use Modules\Transactions\Entities\Payment;
use Modules\Schedules\Entities\Schedule;
use Modules\Invoices\Entities\Invoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Spatie\Activitylog\Models\Activity;
use Validator;
use Sentinel;
use Reminder, Mail;
use Socialite;
use Hash;
use URL;

class ApiController extends ApiGuardController {

	public function index()
	{
		return view('users::index');
	}

    /**
     * @SWG\Post(
     *   path="/auth/login",
     *   summary="Login endpoint for user",
     *   description="-",
     *   tags={"user"},
     *   @SWG\Parameter(
     *       name="email",
     *       in="formData",
     *       description="user email for login",
     *       required=true,
     *       type="integer",
     *       format="string, email address",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="password",
     *       in="formData",
     *       description="user password for login",
     *       required=true,
     *       type="string",
     *       format="string",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="fcm_token",
     *       in="formData",
     *       description="fcm token for login",
     *       required=true,
     *       type="string",
     *       format="string",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="True / False after submit",
     *	   @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/User")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="400",
     *     description="Wrong email/password combination.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/User")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Error Message",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/User")
     *     ),
     *   )
     * )
     */
    public function login(Request $request){
        \DB::beginTransaction();
        try{
            $data = $request->all();
            $response = array('status' => 'error', 'code' => 400, 'data' => false);
            // Validation
            $rules = array(
                'email'         => 'required|email',
                'password'      => 'required'
            );
            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                $response['data'] = $validator->errors()->all();
            } else {
                $user = Sentinel::findByCredentials($data);
                // Checking user role
                if(!$user){
                    $response['data'] ='Wrong email/password combination.';
                }else{
                    $resp = Sentinel::validateCredentials($user, $data);
                    if(!$resp){
                        $response['data'] ='Wrong email/password combination.';
                    }else{
                        if($user->status == 'INACTIVE'){
                            $response['data'] = 'Your Account is not activated!';
                        }else{
                            // Set FCM token (Firebase Cloud Messaging)
                            /*if(!empty($data['fcm_token']) && $user->fcm_token!=$data['fcm_token']){
                                $user->fcm_token = $data['fcm_token']?$data['fcm_token']:null;
                                $user->save();
                            }*/
                            $tmp = \Sentinel::authenticate($data);
                            $apiKey = \Chrisbjr\ApiGuard\Models\ApiKey::where('user_id', '=', $user->id)->whereNull('deleted_at')->first();
                            if($apiKey){
                                $apiKey = $apiKey;
                            }else{
                                $apiKey = \Chrisbjr\ApiGuard\Models\ApiKey::make($user->id);
                            }

							$users = User::find($user->id);
                            $users->fcm_token = $data['fcm_token'];
                            $users->save();

                            $users->apiKey = $apiKey;
                            if(\Sentinel::findById($users->id)->inRole('debtCollector')){
                                $users->ordersDebts = Order::with('customer','orderDetails')->where('status','=',Order::INPROGRESS)->get();
                                foreach($users->ordersDebts as &$orderDebt){
                                    $totalDebt = 0;
                                    foreach($orderDebt->orderDetails as $orderDetail){
                                        $totalDebt = $totalDebt + $orderDetail->price;
                                    }
                                    $orderDebt->totalDebt = $totalDebt;
                                }

                            }

                            if(\Sentinel::getUser()->inRole('sales')){
                                $users->role = 'sales';
                            }elseif(\Sentinel::getUser()->inRole('debtCollector')){
                                $users->role = 'debtCollector';
                            }elseif(\Sentinel::getUser()->inRole('customer')){
                                $users->role = 'customer';
                            }elseif(\Sentinel::getUser()->inRole('gm')){
                                $users->role = 'gm';
                            }elseif(\Sentinel::getUser()->inRole('bm')){
                                $users->role = 'bm';
                            }
                            $response['status'] = 'success';
                            $response['code'] = '200';
                            $response['data'] = $users;
                        }
                    }
                }
            }
            \DB::commit();
        }catch(\Exception $e) {
            \DB::rollback();
            $response = array('status' => 'error', 'code' => 500, 'data' => $e->getMessage());
        }

        return Response::json($response);
    }

    
    /**
     * @SWG\Post(
     *   path="/auth/forgot",
     *   summary="Forgot Password endpoint for user",
     *   description="-",
     *   tags={"user"},
     *   @SWG\Parameter(
     *       name="email",
     *       in="formData",
     *       description="user email for check",
     *       required=true,
     *       type="integer",
     *       format="string, email address",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="array of status",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/User")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="400",
     *     description="Email not found",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/User")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Fatal Error - reason will be supplied",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/User")
     *     ),
     *   ),
     * )
     */
    public function forgotPassword(Request $request){
        try{
            $data = $request->all();

            $response = array('status' => 'error', 'code' => 400, 'data' => false);

            $rules = array(
                'email'         => 'required|email',
            );

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                $response['data'] = $validator->errors()->all();
            } else {
                $user = Sentinel::findByCredentials($data);
                if(!$user){
                    $response['data'] ='Email not found';
                }else{
                    if($user->status == 'INACTIVE'){
                        $response['data'] = 'User is not activated!';
                    }else{
                        ($reminder = Reminder::exists($user)) || ($reminder = Reminder::create($user));

                        $data['code'] = $reminder->code;
                        $data['id'] = $reminder->user_id;


                        // TODO: More elegant way to set this mail config
                        // Set config value for mail
                        $config = [
                            'mail.driver'       => 'smtp',
                            'mail.host'         => 'smtp.gmail.com',
                            'mail.port'         => 465,
                            'mail.encryption'   => 'ssl',
                            'mail.from'         => ['address' => 'hello@eyesimple.us', 'name' => 'Eyesimple'],
                            'mail.username'     => 'hello@eyesimple.us',
                            'mail.password'     => 'Fortune=2014',
                        ];

                        config($config);

                        Mail::send('users::emails.forgotPass', $data, function($message) use ($user) {
                            $message->from('hello@eyesimple.us', 'Eyesimple');
                            $message->to($user->email, $user->first_name)->subject('Password Reset on Indoraven');
                        });

                        $response['status'] = 'success';
                        $response['code'] = '200';
                        $response['data'] = 'Email has been sent';
                    }
                }
            }
        }catch(\Exception $e) {
            $response = array('status' => 'error', 'code' => 500, 'data' => $e->getMessage());
        }

        return Response::json($response);
    }

    /**
     * @SWG\Post(
     *   path="/auth/logout",
     *   summary="Logout endpoint for user",
     *   description="-",
     *   tags={"user"},
     *   @SWG\Parameter(
     *       name="user_id",
     *       in="formData",
     *       description="user ID to logout",
     *       required=true,
     *       type="integer",
     *       format="string",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="array of status",
     *	   @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/User")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="400",
     *     description="User not found",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/User")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="401",
     *     description="User is not logged in",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/User")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Fatal Error - reason will be supplied",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/User")
     *     ),
     *   ),
     * )
     */
    public function logout(Request $request){
        try{
            $data = $request->all();

            $response = array('status' => 'error', 'code' => 400, 'data' => false);

            $rules = array(
                'user_id'         => 'numeric|required',
            );

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                $response['data'] = $validator->errors()->all();
            } else {
                $user = Sentinel::findById($data['user_id']);
                if(!$user){
                    $response['data'] ='Email not found';
                }else{
                    if($user->status == 'INACTIVE'){
                        $response['data'] = 'User is not activated!';
                    }else{
                        if ($user = Sentinel::check())
                        {
                            //$user->fcm_token = null;
                            $user->save();

                            \Sentinel::logout();

                            $response['status'] = 'success';
                            $response['code'] = '200';
                            $response['data'] = 'Logout success';
                        }
                        else
                        {
                            $response = array('status' => 'error', 'code' => 401, 'data' => 'User is not logged in');
                        }
                    }
                }
            }
        }catch(\Exception $e) {
            $response = array('status' => 'error', 'code' => 500, 'data' => $e->getMessage());
        }

        return Response::json($response);
    }

    /**
     * @SWG\Get(
     *   path="/user/getInfo",
     *   summary="get user info based on ID",
     *   description="-",
     *   tags={"user"},
     *   @SWG\Parameter(
     *       name="user_id",
     *       in="query",
     *       description="user ID to get the info",
     *       required=true,
     *       type="integer",
     *       format="string, email address",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="array of status",
     *	   @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/User")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="400",
     *     description="User not found",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/User")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="401",
     *     description="User is not logged in",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/User")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Fatal Error - reason will be supplied",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/User")
     *     ),
     *   ),
     * )
     */
    public function getInfo(Request $request){
        try{
            $data = $request->all();

            $response = array('status' => 'error', 'code' => 400, 'data' => false);

            $rules = array(
                'user_id'         => 'numeric|required',
            );

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                $response['data'] = $validator->errors()->all();
            } else {
                $currentUser = \Sentinel::findUserById($data['user_id']);


				/*$user = array();
                if(isset($currentUser) && $currentUser->inRole('customer')){
                    $user = User::with('customerDetail')->find($data['user_id']);
                }elseif(isset($currentUser) && $currentUser->inRole('sales')){
                    $user = User::with('salesDetail')->find($data['user_id']);
                }*/

               

                    $response['status'] = 'success';
                    $response['code'] = '200';
                    $response['data'] = $currentUser;

            }
        }catch(\Exception $e) {
            $response = array('status' => 'error', 'code' => 500, 'data' => $e->getMessage());
            return Response::json($response);
        }

        return Response::json($response);
    }

    

    /**
     * @SWG\Get(
     *   path="/auth/checkLoginStatus",
     *   summary="Check Login status endpoint for user",
     *   description="-",
     *   tags={"user"},
     *   @SWG\Response(
     *     response=200,
     *     description="array of status",
     *	   @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/User")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="400",
     *     description="User not found",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/User")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="401",
     *     description="User is not logged in",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/User")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Fatal Error - reason will be supplied",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/User")
     *     ),
     *   ),
     * )
     */
    public function checkLoginStatus(Request $request){
        try{
            $data = $request->all();

            $response = array('status' => 'error', 'code' => 400, 'data' => false);

            $rules = array(
                //'user_id'         => 'numeric|required',
            );

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                $response['data'] = $validator->errors()->all();
            } else {
                /*$user = Sentinel::findById($data['user_id']);
                if(!$user){
                    $response['data'] ='Email not found';
                }else{
                    if($user->status == 'INACTIVE'){
                        $response['data'] = 'User is not activated!';
                    }else{*/
                        if ($user = Sentinel::check())
                        {
                            $response['status'] = 'success';
                            $response['code'] = '200';
                            $response['data'] = $user;
                        }
                        else
                        {
                            $response = array('status' => 'error', 'code' => 401, 'data' => 'User is not logged in');
                        }
                    /*}
                }*/
            }
        }catch(\Exception $e) {
            $response = array('status' => 'error', 'code' => 500, 'data' => $e->getMessage());
        }

        return Response::json($response, $response['code']);
    }


    /**
     * @SWG\Post(
     *   path="/auth/editInfo",
     *   summary="edit user info on ID",
     *   description="-",
     *   tags={"user"},
     *   @SWG\Parameter(
     *       name="user_id",
     *       in="formData",
     *       description="user ID to edit the info",
     *       required=true,
     *       type="integer",
     *       format="string, email address",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="email",
     *       in="formData",
     *       description="user email",
     *       required=false,
     *       type="integer",
     *       format="string, email address",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="password",
     *       in="formData",
     *       description="user password",
     *       required=false,
     *       type="array",
     *       format="string",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="confirm_password",
     *       in="formData",
     *       description="confirm user password",
     *       required=false,
     *       type="array",
     *       format="string",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="phone",
     *       in="formData",
     *       description="user phone",
     *       required=false,
     *       type="array",
     *       format="string",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="first_name",
     *       in="formData",
     *       description="user first name",
     *       required=false,
     *       type="array",
     *       format="string",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="last_name",
     *       in="formData",
     *       description="user last name",
     *       required=false,
     *       type="array",
     *       format="string",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="array of status",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/User")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="400",
     *     description="User not found",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/User")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="401",
     *     description="User is not logged in",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/User")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Fatal Error - reason will be supplied",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/User")
     *     ),
     *   ),
     * )
     */
    public function editInfo(Request $request){
        \DB::beginTransaction();
        try{
            $data = $request->all();

            $response = array('status' => 'error', 'code' => 400, 'data' => false);

            $rules = array(
                'user_id'               => 'numeric|required',
                'email'                 => 'email',
                'phone'                 => 'numeric',
                'first_name'            => 'string',
                'last_name'             => 'string'
            );

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                $response['data'] = $validator->errors()->all();
            } else {
                $user = Sentinel::findById($data['user_id']);

                if(!$user){
                    $response['data'] ='User not found';
                }else{

                    $newData = array();
                    if(!empty($data['email'])){
                        if($user->email != $data['email']){
                            $email = Sentinel::findByCredentials(['email' => $data['email']]);
                            if(!empty($email)){
                                $response['data'] = ['Email already Exists'];
                                return Response::json($response,$response['code']);
                            }
                            $newData['email'] = $data['email'];
                        }
                    }
                    if(!empty($data['phone']))
                        $newData['phone'] = $data['phone'];
                    if(!empty($data['first_name']))
                        $newData['first_name'] = $data['first_name'];
                    if(isset($data['last_name']))
                        $newData['last_name'] = $data['last_name'];

                    if(!empty($data['password'])) {

                        if($data['confirm_password'] == $data['password']){
                            $newData['password'] = $data['password'];
                        }else{
                            $response['data'] = 'password not match';
                            return Response::json($response);
                        }

                    }

                    if(empty($newData))
                        $response['data'] = ['No Data is supplied'];
                    else{
                        $user = Sentinel::update($user, $newData);
                        if(!empty($data['phone'])){
                            $user->phone = $data['phone'];
                            $user->save();
                        }
                        $response['status'] = 'success';
                        $response['code'] = '200';
                        $response['data'] = $user;
                        \DB::commit();
                    }
                }
            }
        }catch(\Exception $e) {
            \DB::rollback();
            $response = array('status' => 'error', 'code' => 500, 'data' => $e->getMessage());
        }

        return Response::json($response);
    }

    /**
     * @SWG\Get(
     *   path="/debtCollectors/getAllUserDebt",
     *   summary="List of all customer with debt collector role",
     *   description="-",
     *   tags={"Debt Collector"},
     *   @SWG\Response(
     *     response=200,
     *     description="True / False after submit",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Error Message",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   )
     * )
     */
    public function getAllUserDebt(){
        try{

            $response = array('status' => 'error', 'code' => 400, 'data' => false);

            $usersData = User::all();
            $users = array();
            foreach($usersData as $row){
                $check = false;
                $check = \Sentinel::findById($row->id)->inRole('debtCollector');
                if($check == true){
                    $users[]= $row;
                }
            }
            

            if(isset($users)){
                $response['status'] = 'success';
                $response['code'] = '200';
                $response['data'] = $users;
            }else{
                $response['data'] = 'no customer data';
            }
            
        }catch(\Exception $e) {
            $response = array('status' => 'error', 'code' => 500, 'data' => $e->getMessage());
        }

        return Response::json($response);
    }

    /**
     * @SWG\post(
     *   path="/debtCollectors/customerInvoiceInfo",
     *   summary="getting customer history orders and status orders",
     *   description="-",
     *   tags={"Debt Collector"},
     *   @SWG\Parameter(
     *       name="customer_id",
     *       in="query",
     *       description="customer id",
     *       required=true,
     *       type="integer",
     *       format="integer, customer id",
     *       @SWG\Items(type="integer"),
     *       collectionFormat="multi"
     *  ),
     *   @SWG\Response(
     *     response=200,
     *     description="True / False after submit",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Error Message",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   )
     * )
     */
     public function customerInvoiceInfo(Request $request){
        \DB::beginTransaction();
        try{
            $data = $request->all();
            $response = array('status' => 'error', 'code' => 400, 'data' => false);
            $rules = array(
                'customer_id'     => 'numeric|required'
            );

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                $response['data'] = $validator->errors()->all();
            } else {
                $customer = Customer::with(array('invoices' => function($query){
                    $query->where('status','=',Invoice::INDEBT);
                },'invoices.payments'))->find($data['customer_id']);

                $response['status'] = 'success';
                $response['code'] = 200;
                $response['data'] = $customer;
            }            
        }catch(\Exception $e) {
            \DB::rollback();
            $response = array('status' => 'error', 'code' => 500, 'data' => $e->getMessage());
        }
        \DB::commit();

        return Response::json($response);
     }

    /**
     * @SWG\post(
     *   path="/debtCollectors/addPaymentImage",
     *   summary="add Image to Payment",
     *   description="-",
     *   tags={"Debt Collector"},
     *   @SWG\Parameter(
     *       name="payment_id",
     *       in="query",
     *       description="Payment ID to attach the image (get one from previous payment)",
     *       required=true,
     *       type="integer",
     *       format="integer, schedule id",
     *       @SWG\Items(type="integer"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="image",
     *       in="query",
     *       description="image to save - note this is only for test, and swagger cannot replicate image file type, so this is only for mockup reference and only temporary",
     *       required=false,
     *       type="string",
     *       format="string, payment method",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="True / False after submit",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Error Message",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   )
     * )
     */
    public function addPaymentImage(Request $request){
        \DB::beginTransaction();
        try{
            $data = $request->all();
            $response = array('status' => 'error', 'code' => 400, 'data' => false);
            $rules = array(
                'payment_id'     => 'numeric|required',
            );

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                $response['data'] = $validator->errors()->all();
            } else {
                $payment = Payment::find($data['payment_id']);

                /*******************************************
                 * BEGIN FILE PROCESSING
                 ******************************************/
                if(empty($_FILES)){
                    throw new \Exception('Camera Image not Uploaded');
                }

                if($_FILES['file']['error'] <> 0){
                    throw new \Exception('Something Went wrong when uploading the image');
                }

                $uploads_dir = public_path().DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'payment'.DIRECTORY_SEPARATOR;
                $tmp_name = $_FILES["file"]["tmp_name"];
                // basename() may prevent filesystem traversal attacks;
                // further validation/sanitation of the filename may be appropriate
                $name = 'payment-image-'.$payment->id.'.jpg';
                move_uploaded_file($tmp_name, $uploads_dir.$name);

                $feat_image = URL::to('/').'/uploads/payment/'.$name;
                $payment->feat_image = $feat_image;
                $payment->save();

                /*******************************************
                 * END FILE PROCESSING
                 ******************************************/

                $response['status'] = 'success';
                $response['code'] = '200';
                $response['data'] = $payment;
                $response['all'] = $data;
                $response['file'] = $_FILES;

            }
        }catch(\Exception $e) {
            \DB::rollback();
            $response = array('status' => 'error', 'code' => 500, 'data' => $e->getMessage());
        }
        \DB::commit();

        return Response::json($response);
    }


     /**
     * @SWG\post(
     *   path="/debtCollectors/addPayment",
     *   summary="add new payment to order debt",
     *   description="-",
     *   tags={"Debt Collector"},
     *   @SWG\Parameter(
     *       name="schedule_id",
     *       in="query",
     *       description="schedule id",
     *       required=true,
     *       type="integer",
     *       format="integer, schedule id",
     *       @SWG\Items(type="integer"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="payment_method",
     *       in="query",
     *       description="payment method for order",
     *       required=true,
     *       type="string",
     *       format="string, payment method",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="payment",
     *       in="query",
     *       description="payment value",
     *       required=true,
     *       type="integer",
     *       format="integer, payment value",
     *       @SWG\Items(type="integer"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="reason",
     *       in="query",
     *       description="reason value",
     *       required=false,
     *       type="string",
     *       format="string, reason value",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="coords",
     *       in="query",
     *       description="payment value",
     *       required=true,
     *       type="string",
     *       format="string, coords",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="True / False after submit",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Error Message",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   )
     * )
     */
     public function addPayment(Request $request){
        \DB::beginTransaction();
        try{
            $data = $request->all();
            $response = array('status' => 'error', 'code' => 400, 'data' => false);
            $rules = array(
                'schedule_id'     => 'numeric|required',
                'payment_method' => 'string',
                'payment' => 'integer',
                'reason'    =>  'string',
                'coords'    =>  'string'
            );

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                $response['data'] = $validator->errors()->all();
            } else {

                $schedule = Schedule::with('invoice.payments')->find($data['schedule_id']);
                /*$response['data'] = $schedule;
                return Response::json($response);*/
               //$invoice = Invoice::find($schedule->invoice->id);

                if($schedule->invoice->status = Invoice::INDEBT){
                    $payment = new Payment();
                    $payment->invoice_id = $schedule->invoice->id;
                    $payment->payment_method = $data['payment_method'];
                    $payment->payment = $data['payment'];
                    if(isset($data['reason'])){
                        $payment->reason = $data['reason'];
                    }
                    $payment->coords = $data['coords'];
                    $payment->save();

                    /*******************************************
                     * BEGIN FILE PROCESSING
                     ******************************************/
                    if(empty($_FILES)){
                        throw new \Exception('Camera Image not Uploaded');
                    }

                    if($_FILES['file']['error'] <> 0){
                        throw new \Exception('Something Went wrong when uploading the image');
                    }

                    $uploads_dir = public_path().DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'payment'.DIRECTORY_SEPARATOR;
                    $tmp_name = $_FILES["file"]["tmp_name"];
                    // basename() may prevent filesystem traversal attacks;
                    // further validation/sanitation of the filename may be appropriate
                    $name = 'payment-image-'.$payment->id.'.jpg';
                    move_uploaded_file($tmp_name, $uploads_dir.$name);

                    $feat_image = URL::to('/').'/uploads/payment/'.$name;
                    $payment->feat_image = $feat_image;
                    $payment->save();

                    /*******************************************
                     * END FILE PROCESSING
                     ******************************************/


                    $totalDebt = $schedule->invoice->total_debt;
                    $totalPaid = $schedule->invoice->total_paid;

                    $newPayment = $totalPaid + $data['payment'];

                    $debt = $totalDebt - $newPayment;

                    if($debt <= 0 ){
                        $schedule->invoice->status = Invoice::PAID;
                    }
                    $schedule->invoice->total_paid = $newPayment;
                    $schedule->invoice->save();

                    $schedule->status = Schedule::VISITED;
                    $schedule->save();

                    // add payment info for attaching image later
                    $schedule->payment = $payment;

                   // $invoice = Invoice::with('payments')->find($invoice->id);

                    $response['status'] = 'success';
                    $response['code'] = '200';
                    $response['data'] = $schedule;

                }else{
                    $response['data'] = 'this invoice already paid';
                }

            }            

            
        }catch(\Exception $e) {
            \DB::rollback();
            $response = array('status' => 'error', 'code' => 500, 'data' => $e->getMessage());
        }
        \DB::commit();

        return Response::json($response);
     }


}
