<?php

Route::group(['middleware' => 'web', 'namespace' => 'Modules\Users\Http\Controllers'], function()
{
	//Route::get('/', 'UsersController@index');
	Route::group(['middleware' => ['apiAuth'], 'prefix' => 'api/v1'], function() {
		Route::post('/auth/login', 'ApiController@login');
		Route::post('/auth/forgot', 'ApiController@forgotPassword');
        Route::post('/auth/logout', 'ApiController@logout');
        Route::get('/user/getInfo', 'ApiController@getInfo');
        Route::get('/auth/checkLoginStatus', 'ApiController@checkLoginStatus');
        Route::post('/auth/editInfo','ApiController@editInfo');

        Route::get('/debtCollectors/getAllUserDebt','ApiController@getAllUserDebt');
        Route::post('/debtCollectors/customerInvoiceInfo','ApiController@customerInvoiceInfo');
        Route::get('/debtCollectors/orderDetailsInfo','ApiController@orderDetailsInfo');
        Route::post('/debtCollectors/addPayment','ApiController@addPayment');
        Route::post('/debtCollectors/addPaymentImage','ApiController@addPaymentImage');
	});
});