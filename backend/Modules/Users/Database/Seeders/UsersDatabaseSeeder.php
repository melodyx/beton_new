<?php

namespace Modules\Users\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Users\Entities\User;

class UsersDatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
		$this->call(ApiKeysSeederTableSeeder::class);

		//sales data
		$email = 'sales@eyesimple.us';
		$credentials = [
			'email'    => $email,
			'password' => '123123',
			'first_name' => 'Sales',
			'last_name' => 'Oke'
		];
		$user = \Sentinel::register($credentials,true);
		$user->status = User::ACTIVE;
		$user->save();
		$role = \Sentinel::findRoleByName('sales');
		$role->users()->attach($user->id);

		//debt data
		$email = 'debt@eyesimple.us';
		$credentials = [
			'email'    => $email,
			'password' => '123123',
			'first_name' => 'Debt Collector',
			'last_name' => 'Oke'
		];
		$user = \Sentinel::register($credentials,true);
		$user->status = User::ACTIVE;
		$user->save();
		$role = \Sentinel::findRoleByName('debtCollector');
		$role->users()->attach($user->id);

		//gm data
		$email = 'gm@eyesimple.us';
		$credentials = [
			'email'    => $email,
			'password' => '123123',
			'first_name' => 'GM',
			'last_name' => 'Oke'
		];
		$user = \Sentinel::register($credentials,true);
		$user->status = User::ACTIVE;
		$user->save();
		$role = \Sentinel::findRoleByName('gm');
		$role->users()->attach($user->id);

		//bm data
		$email = 'bm@eyesimple.us';
		$credentials = [
			'email'    => $email,
			'password' => '123123',
			'first_name' => 'BM',
			'last_name' => 'Oke'
		];
		$user = \Sentinel::register($credentials,true);
		$user->status = User::ACTIVE;
		$user->save();
		$role = \Sentinel::findRoleByName('bm');
		$role->users()->attach($user->id);

		// $this->call("OthersTableSeeder");
	}
}
