<?php

namespace Modules\Users\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ApiKeysSeederTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
		\DB::table('api_keys')->insert([
			'key' => '17504c7f35fb02c950cae88a42ebb0f168f87629',
			'level'              =>  '10',
            'ignore_limits'              =>  '1',
            'created_at'        =>  Carbon::now()->format('Y-m-d H:i:s')
		]);
		// $this->call("OthersTableSeeder");
	}
}
