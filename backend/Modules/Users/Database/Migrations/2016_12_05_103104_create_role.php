<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRole extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table)
        {
            if (!Schema::hasColumn('users', 'fcm_token'))
            {
                $table->text('fcm_token')->default(null);
            }

            if (!Schema::hasColumn('users', 'phone'))
            {
                $table->text('phone')->default(null);
            }
        });

        $role = \Sentinel::getRoleRepository()->createModel()->create([
                'name' => 'sales',
                'slug' => 'sales',
        ]);
        

        $role = \Sentinel::getRoleRepository()->createModel()->create([
                'name' => 'debtCollector',
                'slug' => 'debtCollector',
        ]);

        $role = \Sentinel::getRoleRepository()->createModel()->create([
                'name' => 'bm',
                'slug' => 'bm',
        ]);

        $role = \Sentinel::getRoleRepository()->createModel()->create([
                'name' => 'gm',
                'slug' => 'gm',
        ]);

        $role = \Sentinel::getRoleRepository()->createModel()->create([
                'name' => 'Admin Sales',
                'slug' => 'admin-sales',
        ]);

        $role = \Sentinel::getRoleRepository()->createModel()->create([
                'name' => 'Admin Debt',
                'slug' => 'admin-debt',
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table)
        {
            if (Schema::hasColumn('users', 'fcm_token'))
            {
                $table->dropColumn('fcm_token');
            }

            if (Schema::hasColumn('users', 'phone'))
            {
                $table->dropColumn('phone');
            }

        });
    }

}
