<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateRolePermisions extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $role = \Sentinel::findRoleBySlug('admin-sales');
        $role->permissions =[
            'expeditions' => true,
            'products' => true,
            'salesReports' => true,
            'orders' => true,
            'trackLogs' => true,
            'schedules' => false,
            'invoiceReports' => false,
            'invoice' => true,
        ];
        $role->save();

        $role = \Sentinel::findRoleBySlug('admin-debt');
        $role->permissions =[
            'expeditions' => false,
            'products' => false,
            'salesReports' => false,
            'orders' => false,
            'trackLogs' => false,
            'schedules' => true,
            'invoiceReports' => true,
            'invoice' => true,
        ];
        $role->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }

}
