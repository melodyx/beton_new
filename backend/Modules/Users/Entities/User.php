<?php

namespace Modules\Users\Entities;

use Illuminate\Database\Eloquent\Model;

class User extends \App\User
{
    protected $fillable = [];

    /*public function customerDetails(){
    	$this->hasMany('\Modules\Customers\Entities\CustomerDetail','user_id');
    }*/

    public function approvals(){
    	return $this->hasMany('\Modules\Customers\Entities\Approval','author');
    }

    public function schedules(){
        return $this->hasMany('\Modules\Schedules\Entities\Schedule','debt_collector');
    }

    public function tracklogs(){
        return $this->hasMany('\Modules\TrackLogs\Entities\Tracklog','user_id');
    }

    public function aprrovalDetailsAuthor(){
        return $this->hasMany('\Modules\Customers\Entities\ApprovalDetail','author_id');
    }

    public function orderAuthor(){
        return $this->hasMany('\Modules\Transactions\Entities\Order','author_id');
    }
}
