<?php

namespace Modules\Invoices\Entities;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
	const INDEBT = 'in debt';
	const PAID = 'paid';

    protected $fillable = [];

    public function schedules(){
    	return $this->hasMany('\Modules\Schedules\Entities\Schedule','invoice_id');
    }

    public function order(){
    	return $this->belongsTo('\Modules\Transactions\Entities\Order','order_id');
    }

    public function customer(){
    	return $this->belongsTo('\Modules\Customers\Entities\Customer','customer_id');
    }

    public function payments(){
    	return $this->hasMany('Modules\Transactions\Entities\Payment','invoice_id');
    }

    public static function saveInvoice($id,$data){
    	\DB::beginTransaction();
    	try {
    		if(is_null($id)){
    			$invoice = new Invoice();
    		}else{
    			$invoice = Invoice::find($id);
    		}

    		$invoice->invoice_number = $data['invoice_number'];
    		$invoice->customer_id = $data['customer_id'];
    		$invoice->total_debt = $data['total_debt'];
    		$invoice->status = Invoice::INDEBT;

    		if(isset($data['order_id'])){
    			$invoice->order_id = $data['order_id'];
    		}
    		
    		$invoice->save();

    	} catch (\Exception $e) {
    		\DB::rollback();
    		return redirect()->route('invoices.index')->with('success','fail add new invoice');
    	}
    	\DB::commit();

    	return $invoice;
    }
}
