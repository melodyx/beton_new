<?php

Route::group(['middleware' => 'web','namespace' => 'Modules\Invoices\Http\Controllers'], function()
{
	//Route::get('/', 'InvoicesController@index');

	Route::resource('invoices','InvoicesController');
	Route::post('invoices/ajaxDetailPayment','InvoicesController@ajaxDetailPayment')->name('invoices.ajaxDetailPayment');
});