<?php

namespace Modules\Invoices\Http\Controllers;

use Nwidart\Modules\Routing\Controller;
use Modules\Invoices\Entities\Invoice;
use Modules\Schedules\Entities\Schedule;
use Modules\Transactions\Entities\Order;
use Modules\Customers\Entities\Customer;
use Modules\Transactions\Entities\Payment;
use Illuminate\Http\Request;

class InvoicesController extends Controller {

	public function index()
	{
		//Checking role for access customer page
		$current_user_role = $this->checkRole();
		if($current_user_role == 'notLogin'){
			return redirect()->route('login')->with('success','you dont have any authorization for access this page');
		}
		//=====================================
		$invoices = Invoice::with('customer','order.orderDetails')->get();

		return view('invoices::index',['invoices' => $invoices]);
	}

	public function create(){
		//Checking role for access customer page
		$current_user_role = $this->checkRole();
		if($current_user_role == 'notLogin'){
			return redirect()->route('login')->with('success','you dont have any authorization for access this page');
		}
		//=====================================

		$customers = Customer::all();
		return view('invoices::invoices.create',['customers' => $customers]);
	}


	public function store(Request $request){
		$data = $request->all();

		$invoice = Invoice::saveInvoice(null, $data);

		return redirect()->route('invoices.index')->with('success','successfully save invoice');
	}

	public function edit($id){
		//Checking role for access customer page
		$current_user_role = $this->checkRole();
		if($current_user_role == 'notLogin'){
			return redirect()->route('login')->with('success','you dont have any authorization for access this page');
		}
		//=====================================

		$invoice = Invoice::with('customer','order.orderDetails')->find($id);
		$customers = Customer::all();

		return view('invoices::invoices.edit',['invoice'=>$invoice, 'customers' => $customers]);
	}

	public function update($id,Request $request){
		$data = $request->all();
		$invoice = Invoice::saveInvoice($id,$data);

		return redirect()->route('invoices.index')->with('success','successfully edit invoice');
	}

	public function destroy($id){
		$invoice = Invoice::find($id);
		$invoice->delete();

		 return array(
            'url' => route('invoices.index'),
            'message' => 'Something went wrong while trying to remove the customer!'
        );
	}

	public function show($id){
		$invoice = Invoice::with(array('customer','payments'))->find($id);
		return view('invoices::invoices.details',['invoice' => $invoice]);
	}

	public function ajaxDetailPayment(Request $request){
		$data = $request->all();

		$payment = Payment::find($data['paymentId']);

		$payments['data'] = $payment;
		$payments['coords'] = json_decode($payment->coords);
		return $payments;
	}

	public function checkRole(){
		$user = \Sentinel::getUser();
		if($user){
            $current_user['superadmin'] = \Sentinel::inRole('superadmin');
            $current_user['admin'] = \Sentinel::inRole('admin');
            $current_user['sales'] = \Sentinel::inRole('sales');
            $current_user['gm'] = \Sentinel::inRole('gm');
            $current_user['bm'] = \Sentinel::inRole('bm');
            $current_user['debtCollector'] = \Sentinel::inRole('debtCollector');
            $current_user['customer'] = \Sentinel::inRole('customer');
			
			if($current_user['superadmin'] == true){
				return 'superadmin';
			}elseif($current_user['admin'] == true){
				return 'admin';
			}elseif($current_user['sales'] == true){
				return 'sales';
			}elseif($current_user['gm'] == true){
				return 'gm';
			}elseif($current_user['bm'] == true){
				return 'bm';
			}elseif($current_user['debtCollector'] == true){
				return 'debtCollector';
			}

		}else{
			return 'notLogin';
		}
	}

}
