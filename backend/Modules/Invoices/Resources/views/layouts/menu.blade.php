<?php $user = \Sentinel::getUser(); ?>
<?php //if($user == \Sentinel::inRole('superadmin') || $user == \Sentinel::inRole('admin')){?>
<?php if($user->hasAccess('admin.login') ||$user->hasAccess('invoice') ){ ?>
<li>
	<a><i class="icon-users"></i> <span>Invoices</span></a>
	<ul>
			<li><a href="{{route('invoices.index')}}">Invoices view</a></li>
			<li><a href="{{route('invoices.create')}}">Add Invoice</a></li>
	</ul>
</li>
<?php } ?>
