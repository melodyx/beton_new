@extends('layouts.app')
@section('header')

@parent
	
	<?php $success_message = session('success'); if($success_message){?>
		<script>
		jQuery(document).ready(function(){
			alert('<?php echo $success_message; ?>');
		});
		</script>
	<?php session()->forget('success'); } ?>
	
@endsection
@section('content')
				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Invoices</span></h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="{{route('dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
							<li class="active">Invoices</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->
				<div class="content">
					<div class="panel panel-flat">
					<table class="table datatable-basic">
					<thead>
						<tr>
							<th style="text-align:center;" >Id</th>
							<th style="text-align:center;" >Invoice Number</th>
							<th style="text-align:center;" >Customer</th>
							<th style="text-align:center;" >Total Debt</th>
							<th style="text-align:center;" >Total Paid</th>
							<th style="text-align:center;" >Status</th>
							<th style="text-align:center;" >Action</th>
						</tr>
					</thead>
					<tbody>	
						<?php if(isset($invoices)){ ?>
							<?php foreach($invoices as $invoice){?>
							<tr>
								<td style="text-align:center;" ><?php if($invoice->id){echo $invoice->id;}else{ echo '-';}?></td>
								<td style="text-align:center;" ><?php if($invoice->invoice_number){echo $invoice->invoice_number;}else{ echo '-';}?></td>
								<td style="text-align:center;" ><?php if($invoice->customer->name){echo $invoice->customer->name;}else{ echo '-';}?></td>
								<td style="text-align:center;" ><?php if($invoice->total_debt){echo number_format($invoice->total_debt,2);}else{ echo '-';}?></td>
								<td style="text-align:center;" ><?php if($invoice->total_paid){echo number_format($invoice->total_paid,2);}else{ echo '-';};?></td>
								<td style="text-align:center;" class="status"><?php if($invoice->status){echo $invoice->status;}else{ echo '-';}?></td>
								<td>
									<ul class="icons-list">
										<li >
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">
												<i class="icon-cog7"></i>
												<span class="caret"></span>
											</a>
											<ul class="dropdown-menu dropdown-menu-right">
												<li class="dropdown-header">Options</li>
													<li><a href="{{route('invoices.show',$invoice->id)}}"><i class="icon-pencil7"></i>Details</a></li>
													<li><a href="{{route('invoices.edit',$invoice->id)}}"><i class="icon-pencil7"></i>Edit entry</a></li>
													<li><a href="{{ route('invoices.destroy', $invoice->id)}}" data-method="delete" data-token="{{ csrf_token() }}"><i class="icon-bin"></i>Remove entry</a></li>
											</ul>
										</li>
									</ul>
								</td>
							</tr>
							<?php } ?>
						<?php } ?>
					</tbody>
					</table>	
					</div>
				</div>

@endsection