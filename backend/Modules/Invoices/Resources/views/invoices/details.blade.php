@extends('layouts.app')
@section('header')

    @parent
	<link rel="stylesheet" type="text/css" href="<?php echo Module::asset('invoices:css/colorbox.css')?>">
	<script type="text/javascript" src="<?php echo Module::asset('invoices:js/jquery.colorbox-min.js'); ?>"></script>

	<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBdmLJ80f1HUSAG0YxQthnZ2hd_p60nnT4&v=3.exp"></script>

 <script type="text/javascript" src="{{asset('assets/js/maps/google/basic/basic.js')}}"></script>
 <script type="text/javascript" src="{{asset('assets/js/maps/google/basic/geolocation.js')}}"></script>
 <script type="text/javascript" src="{{asset('assets/js/maps/google/basic/coordinates.js')}}"></script>
 <script type="text/javascript" src="{{asset('assets/js/maps/google/basic/click_event.js')}}"></script>

	<?php $success_message = session('success'); if($success_message){?>
		<script>
		jQuery(document).ready(function(){
			alert('<?php echo $success_message; ?>');
		});
		</script>
	<?php session()->forget('success'); } ?>
	

<style>
	.clear{ clear:both }
	label{ width:150px; display:inline-block; }

	.wrapper.detail-payment{width:100%;}
	.wrapper.detail-payment .nota-image{margin-bottom:50px;}
	.wrapper.detail-payment img{width:100%;}
</style>


<script>
	jQuery(document).ready(function($){
		$('html').on('click','a.pop-up-detail',function(e){
			var paymentId = $(this).parents('tr').find('td.id').text();
			$.ajax({
				url:'<?php echo route("invoices.ajaxDetailPayment")?>',
				type:'post',
				data:{
					paymentId : paymentId
				}
			}).done(function(data){

				var html = '<div class="wrapper detail-payment">\
								<div class="nota-image"><img src="'+data.data.feat_image+'"></div>\
								<div class="map-container map-marker-simple"></div>\
							</div>';
				$.colorbox({
					html:html,
					width:'80%',
					onComplete:function(){
						//console.log(data);
						initialize(data);
					}
				});
			});
		});
	});

	function initialize(arrayDum) {
				// console.log(arrayDum);
				
				// Set coordinates
				var myLatlng = new google.maps.LatLng(arrayDum.coords.latitude,arrayDum.coords.longitude);

				// Options
				var mapOptions = {
					zoom: 15,
					center: myLatlng
				};

				// Apply options
				var map = new google.maps.Map($('.map-marker-simple')[0], mapOptions);
				var contentString = 'Surabaya';
				
				

				// Add info window
				var infowindow = new google.maps.InfoWindow({
					content: contentString
				});

				// Add marker
				var marker
				marker = new google.maps.Marker({
				position: new google.maps.LatLng(arrayDum.coords.latitude,arrayDum.coords.longitude),
				
				map: map,
				title: 'Hello World!'
				});			
				
				

				function placeMarker(location) {
					marker.setMap();
					marker = new google.maps.Marker({
					position: location, 
					map: map
				   });
				}
				 
			};
</script>
@endsection
@section('content')
				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Datatables</span> - Invoice</h4>
						</div>

					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="{{route('dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
							<li class="active" href="{{route('invoices.index')}}">Invoice</li>
							<li class="active">Detail</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->
				<div class="content">
					<div class="row">
						<div class="panel panel-flat">
							<div class="panel-body">
								<legend>Invoice data</legend>
								<div class="form-group">
									<label>Customer Name</label>: <?php echo $invoice->customer->name;?>
									<div class="clear"></div>
								</div>
								<div class="form-group">
									<label>Customer Email</label>: <?php echo $invoice->customer->email;?>
									<div class="clear"></div>
								</div>
								<div class="form-group">
									<label>Customer Address</label>: <?php echo $invoice->customer->address;?>
									<div class="clear"></div>
								</div>
								<div class="form-group">
									<label>Customer Email</label>: <?php echo $invoice->customer->email;?>
									<div class="clear"></div>
								</div>
								<legend>Payment Details</legend>
								<div class="form-group">
									<label>Total debt</label>: <?php echo number_format((float)$invoice->total_debt,2);?>
									<div class="clear"></div>
								</div>
								<table class="table datatable-basic">
									<thead>
										<tr>
											<th style="text-align:center">ID</th>
											<th style="text-align:center">Payment Date</th>
											<th style="text-align:center">Payment Method</th>
											<th style="text-align:center">Payment Value</th>
											<th style="text-align:center">Reason</th>
											<th style="text-align:center">Action</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach($invoice->payments as $payment){?>
										<tr>
											<td class="id" style="text-align:center">{{$payment->id}}</td>
											<td style="text-align:center">{{$payment->created_at}}</td>
											<td style="text-align:center">{{$payment->payment_method}}</td>
											<td style="text-align:center">{{(isset($payment->payment)?number_format((float)$payment->payment):'-')}}</td>
											<td style="text-align:center">{{$payment->reason}}</td>
											<td style="text-align:center">
												<ul class="icons-list">
													<li >
														<a href="#" class="dropdown-toggle" data-toggle="dropdown">
															<i class="icon-cog7"></i>
															<span class="caret"></span>
														</a>
														<ul class="dropdown-menu dropdown-menu-right">
															<li class="dropdown-header">Options</li>
															<li><a class="pop-up-detail"><i class="icon-pencil7"></i>Details</a></li>
														</ul>
													</li>
												</ul>
											</td>
										</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<!--<div class="row">
						<div class="panel panel-flat">
							<div class="panel-body">
								<legend></legend>
							</div>
						</div>
					</div>-->
				</div>

@endsection