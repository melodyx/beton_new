@extends('layouts.app')
@section('header')

@parent
<link rel="stylesheet" type="text/css" href="<?php echo Module::asset('invoices:css/colorbox.css')?>">
<script type="text/javascript" src="<?php echo Module::asset('invoices:js/jquery.colorbox-min.js'); ?>"></script>

<?php /*if(!isset($orders)){?>
<script>
jQuery('html').on('focus','input.customer-name',function(e){
	var html = jQuery('.popUp.customer-data').html();
	jQuery.colorbox({
		html:html,
		fixed:true,
		maxHeight:'70%',
		onComplete: function(){
			jQuery('#cboxContent table').on('click','tr',function(e){
				e.stopPropagation();
				var customerId = jQuery(this).children('td.customer-id').text();
				var customerName = jQuery(this).children('td.customer-name').text();
				jQuery('input.customer-id').val(customerId);
				jQuery('input.customer-name').val(customerName);
				jQuery.colorbox.close()
			});
		}
	});
});
	
</script>
<?php } */?>
<script>
jQuery('html').on('focus','input.customer-name',function(e){
	var html = jQuery('.popUp.order-data').html();
	jQuery.colorbox({
		html:html,
		fixed:true,
		maxHeight:'70%',
		onComplete: function(){
			jQuery('#cboxContent table').on('click','tr',function(e){
				e.stopPropagation();
				var customerId = jQuery(this).children('td.customer-id').text();
				var customerName = jQuery(this).children('td.customer-name').text();
				jQuery('input.customer-id').val(customerId);
				jQuery('input.customer-name').val(customerName);
				jQuery.colorbox.close()
			});
		}
	});
});


</script>


@endsection
@section('content')
<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Datatables</span> - Invoices</h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="{{route('dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="{{route('invoices.index')}}">Invoices</a></li>
							<li class="active">Create</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->
				<div class="content">
					<?php /*<div class="row">
						<div class="col-md-6">*/?>
							<div class="panel panel-flat">
								<div class="panel-body">
									@include('invoices::invoices._form', ['action' => '\Modules\Invoices\Http\Controllers\InvoicesController@store'])
								</div>
							</div>
						<?php /*</div>
					</div> */?>
				</div>
@endsection