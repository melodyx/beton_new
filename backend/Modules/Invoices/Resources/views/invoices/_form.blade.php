<?php
	use \Illuminate\Support\Facades\Input;
	if(!isset($method)){
		$method = 'post';
	}

?>
<script type="text/javascript" src="<?php echo asset('assets/js/plugins/forms/styling/uniform.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo asset('assets/js/pages/form_layouts.js') ?>"></script>


{{ Form::open(array('action' => $action, 'method' => $method, 'class' => 'form-horizontal addSchedule')) }}
<fieldset class="content-group">
							<legend class="text-bold"><?php if(isset($invoice)){ echo 'Edit invoice'; }else{ echo 'Create invoice';}?></legend>
								<div class="form-group">
									<label class="control-label col-md-2">Invoice number</label>
									<div class="col-md-10">
										{{Form::text('invoice_number',isset($invoice->invoice_number)?$invoice->invoice_number:null, array('class' => 'form-control'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Customer Name</label>
									<div class="col-lg-10">
										<div class="popUp order-data" style="display:none;">
											<div class="order-data-table" style="padding:20px;">
												<table>
													<thead>
														<tr>
															<th style="padding:20px; background-color:#e1e1e1; text-align:center;">Customer Id</th>
															<th style="padding:20px; background-color:#e1e1e1; text-align:center;">Customer Name</th>
															<th style="padding:20px; background-color:#e1e1e1; text-align:center;">Address</th>
														</tr>
													</thead>
													<tbody>
														<?php foreach($customers as $customer){ ?>
														<tr>
															<td class="customer-id" style="text-align:center; padding:10px 5px; cursor:pointer;"><?php echo $customer->id; ?></td>
															<td class="customer-name" style="text-align:center; padding:10px 5px; cursor:pointer;"><?php echo $customer->name; ?></td>
															<td class="customer-address" style="text-align:center; padding:10px 5px; cursor:pointer;"><?php echo $customer->addressline1; ?></td>
														</tr>
														<?php } ?>
													</tbody>
												</table>
											</div>
										</div>
										<?php if(isset($order)){?>
											<input type="hidden" name="customer_id" class="customer-id" value="<?php echo $order->customer->id ?>" >
											{{Form::text('customer_name', $order->customer->name,array('class' => 'form-control', 'readonly' => 'readonly', 'style' => 'cursor:pointer;'))}}
										<?php }else{ ?>
											<input type="hidden" name="customer_id" class="customer-id" <?php if(isset($invoice->customer->id)){?>value="<?php echo $invoice->customer->id;?>"<?php } ?>>
											{{Form::text('customer_name', isset($invoice->customer->name)?$invoice->customer->name:null,array('class' => 'form-control customer-name', 'readonly' => 'readonly', 'style' => 'cursor:pointer;'))}}
										<?php } ?>
									</div>
								</div>	
								<div class="form-group">
									<label class="control-label col-lg-2">Total Debt</label>
									<div class="col-lg-10">
									<?php if(isset($totalDebt)){ ?>
										{{Form::text('total_debt', $totalDebt,array('class' => 'form-control total-debt','readonly' => 'readonly'))}}
									<?php }else{ ?>
										{{Form::text('total_debt', isset($invoice->total_debt)?$invoice->total_debt:null,array('class' => 'form-control total-debt'))}}
									<?php } ?>
									</div>
								</div>
								<?php if(isset($order)){?>
									<input name="order_id" type="hidden" value="{{$order->id}}">
								<?php } ?>
								<div class="text-right">
									<input type="submit" class="btn btn-primary" value="Submit">
								</div>
						</fieldset>
{{ Form::close() }}