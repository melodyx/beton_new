<?php

namespace Modules\Schedules\Entities;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
	const INPROGRESS = 'in progress';
	const VISITED = 'visited';
    protected $fillable = [];

    public function invoice(){
    	return $this->belongsTo('\Modules\Invoices\Entities\Invoice','invoice_id');
    }

    public function debtCollector(){
    	return $this->belongsTo('\Modules\Users\Entities\User','debt_collector');
    }

    public function payments(){
    	return $this->hasMany('\Modules\Transactions\Entities\Payment','schedule_id');
    }
}
