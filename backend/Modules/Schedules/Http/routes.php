<?php

Route::group(['middleware' => 'web', 'namespace' => 'Modules\Schedules\Http\Controllers'], function()
{
	//Route::get('/', 'SchedulesController@index');
	Route::resource('schedules','SchedulesController');
	Route::group(['middleware' => ['apiAuth'], 'prefix' => 'api/v1'], function() {
		Route::post('/schedules/getScheduleDebtCollector','ApiController@getScheduleDebtCollector');
		Route::post('/schedules/registerSchedule','ApiController@registerSchedule');
		Route::post('/schedules/editSchedule','ApiController@editSchedule');
		Route::post('/schedules/checkInSchedule','ApiController@checkInSchedule');
	});
});