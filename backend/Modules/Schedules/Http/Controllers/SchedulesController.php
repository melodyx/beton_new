<?php

namespace Modules\Schedules\Http\Controllers;

use Nwidart\Modules\Routing\Controller;
use Modules\Schedules\Entities\Schedule;
use Modules\Invoices\Entities\Invoice;
use Modules\Users\Entities\User;
use Modules\Transactions\Entities\Order;
use Illuminate\Http\Request;

class SchedulesController extends Controller {

	public function index()
	{
		$schedules = Schedule::with('invoice.customer')->get();
		return view('schedules::index',['schedules'=> $schedules]);
	}

	public function create(){
		//$orders = Order::with('customer','orderDetails.product')->where('status','=',Order::INPROGRESS)->get();

		$invoices = Invoice::with('customer','order.orderDetails')->where('status','=',Invoice::INDEBT)->get();

		$users = User::all();
		$debtCollectors = array();
		foreach($users as $user){
			if(\Sentinel::findById($user->id)->inRole('debtCollector')){
				$debtCollectors[] = $user;
			}
		}
		//dd($debtCollectors);
		return view('schedules::schedules.create',['invoices' => $invoices,'debtCollectors' => $debtCollectors]);
	}
	
	public function store(Request $request){
		\DB::beginTransaction();
		try {
			$data = $request->all();
			$schedule = new Schedule();
			$schedule->invoice_id = $data['invoice_id'];
			$schedule->debt_collector = $data['debtCollector_id'];
			$schedule->visit_date = $data['visit_date'];
			$schedule->status = Schedule::INPROGRESS;
			$schedule->save();
		} catch (\Exception $e) {
			\DB::rollback();
			return redirect()->route('schedules.index')->with('success','fail');
		}
		\DB::commit();

		return redirect()->route('schedules.index')->with('success','successfully add customer data');
		
	}

	public function edit($id, Request $request){
		//$orders = Order::with('customer','orderDetails.product')->where('status','=',Order::INPROGRESS)->get();

		$invoices = Invoice::with('customer','order.orderDetails')->where('status','=',Invoice::INDEBT)->get();

		$users = User::all();
		$debtCollectors = array();
		foreach($users as $user){
			if(\Sentinel::findById($user->id)->inRole('debtCollector')){
				$debtCollectors[] = $user;
			}
		}

		$schedule = Schedule::with('invoice.customer','debtCollector')->find($id);

		return view('schedules::schedules.edit',['schedule' => $schedule, 'invoices' => $invoices, 'debtCollectors' => $debtCollectors]);

	}

	public function update($id, Request $request){
		\DB::beginTransaction();
		try {
			$data = $request->all();
			$schedule = Schedule::find($id);
			$schedule->invoice_id = $data['invoice_id'];
			$schedule->debt_collector = $data['debtCollector_id'];
			$schedule->visit_date = $data['visit_date'];
			$schedule->save();
		} catch (\Exception $e) {
			\DB::rollback();
			return redirect()->route('schedules.index')->with('success','fail');
		}
		\DB::commit();

		return redirect()->route('schedules.index')->with('success','successfully edit customer data');
	}

	public function destroy($id){
		$schedule = Schedule::find($id);
		$schedule->delete();

		 return array(
            'url' => route('schedules.index'),
            'message' => 'Something went wrong while trying to remove the customer!'
        );
	}

}
