<?php

namespace Modules\Schedules\Http\Controllers;

use Nwidart\Modules\Routing\Controller;
use Chrisbjr\ApiGuard\Http\Controllers\ApiGuardController;
use Modules\Users\Entities\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Modules\Schedules\Entities\Schedule;
use Modules\Transactions\Entities\Order;
use Modules\Transactions\Entities\OrderDetail;
use Modules\Invoices\Entities\Invoice;
use Validator;
use Sentinel;
use Reminder, Mail;

class ApiController extends ApiGuardController {

    
     /**
     * @SWG\Post(
     *   path="/schedules/getScheduleDebtCollector",
     *   summary="List of all customer with discount",
     *   description="-",
     *   tags={"schedules"},
     *   @SWG\Response(
     *     response=200,
     *     description="True / False after submit",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Schedule")
     *     ),
     *   ),
     *   @SWG\Parameter(
     *       name="debt_collector",
     *       in="formData",
     *       description="debt collector id",
     *       required=true,
     *       type="integer",
     *       format="integer, debt collector",
     *       @SWG\Items(type="integer"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="visit_date",
     *       in="formData",
     *       description="schedule visit date",
     *       required=false,
     *       type="string",
     *       format="string, visit date",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Error Message",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Schedule")
     *     ),
     *   )
     * )
     */
    public function getScheduleDebtCollector(Request $request){
        try{
            $data = $request->all();
            $response = array('status' => 'error', 'code' => 400, 'data' => false);
             // Validation
            $rules = array(
                'debt_collector'       => 'numeric|required',
            );
            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
                $response['data'] = $validator->errors()->all();
            } else {
                /*$schedules = Schedule::with(['order.orderDetails.product','order.customer'])->where('debt_collector','=',$data['debt_collector'])->get();

                foreach($schedules as $schedule){
                    $totalDebt = 0;
                    foreach($schedule->order->orderDetails as $orderDetail){
                        if($orderDetail->status == OrderDetail::INPROGRESS){
                            $totalDebt = $totalDebt + $orderDetail->price;
                        }
                    }
                    $schedule->order->totalDebt = $totalDebt;
                }*/
                if(isset($data['visit_date'])){
                    $schedules = Schedule::with('invoice.customer','invoice.payments')->where('debt_collector','=',$data['debt_collector'])->where('status','=',Schedule::INPROGRESS)->where('visit_date','=',date('Y-m-d',strtotime($data['visit_date'])))->get();
                }else{
                    $schedules = Schedule::with('invoice.customer','invoice.payments')->where('debt_collector','=',$data['debt_collector'])->where('status','=',Schedule::INPROGRESS)->get();
                }

                if(isset($schedules)){
                    $response['status'] = 'success';
                    $response['code'] = '200';
                    $response['data'] = $schedules;
                }else{
                    $response['data'] = 'no schedules data';
                }
            }
            
        }catch(\Exception $e) {
            $response = array('status' => 'error', 'code' => 500, 'data' => $e->getMessage());
        }

        return Response::json($response);
    }

    /**
     * @SWG\Post(
     *   path="/schedules/registerSchedule",
     *   summary="Register schedule for sales",
     *   description="-",
     *   tags={"schedules"},
     *   @SWG\Parameter(
     *       name="invoice_id",
     *       in="formData",
     *       description="invoice id from registred invoices",
     *       required=true,
     *       type="integer",
     *       format="integer, customer id",
     *       @SWG\Items(type="integer"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="debt_collector",
     *       in="formData",
     *       description="debt collector id for visiting schedule task",
     *       required=true,
     *       type="integer",
     *       format="integer, sales id",
     *       @SWG\Items(type="integer"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="visit_date",
     *       in="formData",
     *       description="customer id for schedule",
     *       required=true,
     *       type="string",
     *       format="date, visit date",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="True / False after submit",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Schedule")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Error Message",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Schedule")
     *     ),
     *   )
     * )
     */
    public function registerSchedule(Request $request){
        try{
            $data = $request->all();

            $response = array('status' => 'error', 'code' => 400, 'data' => false);
             // Validation
            $rules = array(
                'invoice_id'       => 'numeric|required',
                'debt_collector' => 'numeric|required',
                'visit_date'    => 'date'
            );
            $validator = Validator::make($data, $rules);
            if ($validator->fails()) {
                $response['data'] = $validator->errors()->all();
            } else {
               $schedule = new Schedule();
               $schedule->invoice_id = $data['invoice_id'];
               $schedule->debt_collector = $data['debt_collector'];
               $schedule->visit_date = date('Y-m-d', strtotime($data['visit_date']));
               $schedule->status = Schedule::INPROGRESS;
               $schedule->save();

                $response['status'] = 'success';
                $response['code'] = '200';
                $response['data'] = $schedule;
            }
            
        }catch(\Exception $e) {
            $response = array('status' => 'error', 'code' => 500, 'data' => $e->getMessage());
        }

        return Response::json($response);
    }

    /**
     * @SWG\Post(
     *   path="/schedules/editSchedule",
     *   summary="Edit schedule",
     *   description="-",
     *   tags={"schedules"},
     *   @SWG\Parameter(
     *       name="schedule_id",
     *       in="formData",
     *       description="schedule id for change info",
     *       required=true,
     *       type="integer",
     *       format="integer, schedule id",
     *       @SWG\Items(type="integer"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="debt_collector",
     *       in="formData",
     *       description="debt collector id for schedule",
     *       required=false,
     *       type="integer",
     *       format="integer, customer id",
     *       @SWG\Items(type="integer"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="visit_date",
     *       in="formData",
     *       description="visit date for schedule",
     *       required=false,
     *       type="string",
     *       format="date, visit date",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="True / False after submit",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Schedule")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Error Message",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Schedule")
     *     ),
     *   )
     * )
     */
    public function editSchedule(Request $request){
        try{
            $data = $request->all();

            $response = array('status' => 'error', 'code' => 400, 'data' => false);
            
               $schedule = Schedule::find($data['schedule_id']);
               if(isset($data['debt_collector'])){
                    $schedule->debt_collector = $data['debt_collector'];
                    $newData['debt_collector'] = $schedule->debt_collector;
                }

                if(isset($data['visit_date'])){
                    $schedule->visit_date = date('Y-m-d',strtotime($data['visit_date']));
                    $newData['visit_date'] = $schedule->visit_date;
                }

                if(isset($newData)){
                    $schedule->save();
                }
               
                $response['status'] = 'success';
                $response['code'] = '200';
                $response['data'] = $schedule;
            
            
        }catch(\Exception $e) {
            $response = array('status' => 'error', 'code' => 500, 'data' => $e->getMessage());
        }

        return Response::json($response);
    }


    /**
     * @SWG\Post(
     *   path="/schedules/checkInSchedule",
     *   summary="Confirm schedule",
     *   description="-",
     *   tags={"schedules"},
     *   @SWG\Parameter(
     *       name="schedule_id",
     *       in="formData",
     *       description="schedule id for confirmation",
     *       required=true,
     *       type="integer",
     *       format="integer, schedule id",
     *       @SWG\Items(type="integer"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="True / False after submit",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Schedule")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Error Message",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Schedule")
     *     ),
     *   )
     * )
     */
    public function checkInSchedule(Request $request){
       \DB::beginTransaction();
        try{
            $data = $request->all();

            $response = array('status' => 'error', 'code' => 400, 'data' => false);
            
            $schedule = Schedule::with('invoice')->find($data['schedule_id']);

           /* $newPayment = $schedule->invoice->total_paid + $data['payment'];
            $schedule->invoice->total_paid = $newPayment;

            $totalDebt = $schedule->invoice->total_debt - $newPayment;

            if($totalDebt <= 0){
               $schedule->invoice->status = Invoice::PAID;
               
            }*/

            $schedule->status = Schedule::VISITED;
           // $schedule->invoice->save();
            $schedule->save();
            
                $response['status'] = 'success';
                $response['code'] = '200';
                $response['data'] = $schedule;
            
            
        }catch(\Exception $e) {
            \DB::rollback();
            $response = array('status' => 'error', 'code' => 500, 'data' => $e->getMessage());
        }
        \DB::commit();
        return Response::json($response);
    }
   
}
