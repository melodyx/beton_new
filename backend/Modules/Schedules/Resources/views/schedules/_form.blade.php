<?php
	use \Illuminate\Support\Facades\Input;
	if(!isset($method)){
		$method = 'post';
	}

?>
<script type="text/javascript" src="<?php echo asset('assets/js/plugins/forms/styling/uniform.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo asset('assets/js/pages/form_layouts.js') ?>"></script>


{{ Form::open(array('action' => $action, 'method' => $method, 'class' => 'form-horizontal addSchedule')) }}
<fieldset class="content-group">
							<legend class="text-bold"><?php if(isset($schedule)){ echo 'Edit schedule'; }else{ echo 'Create schedule';}?></legend>
								<div class="form-group">
									<label class="control-label col-lg-2">Invoice Customer</label>
									<div class="col-lg-10">
										<div class="popUp invoice-data" style="display:none;">
											<div class="invoice-data-table" style="padding:20px;">
												<table>
													<thead>
														<tr>
															<th style="padding:20px; background-color:#e1e1e1; text-align:center;">Invoice id</th>
															<th style="padding:20px; background-color:#e1e1e1; text-align:center;">Customer Name</th>
															<th style="padding:20px; background-color:#e1e1e1; text-align:center;">Total Debt</th>
															<th style="padding:20px; background-color:#e1e1e1; text-align:center;">Total Paid</th>
														</tr>
													</thead>
													<tbody>
														<?php foreach($invoices as $invoice){ ?>
														<tr>
															<td class="invoice-id" style="text-align:center; padding:10px 5px; cursor:pointer;"><?php echo $invoice->id; ?></td>
															<td class="customer-name" style="text-align:center; padding:10px 5px; cursor:pointer;"><?php echo $invoice->customer->name; ?></td>
															<td class="total-debt" style="text-align:center; padding:10px 5px; cursor:pointer;"><?php echo $invoice->total_debt; ?></td>
															<td class="total-paid" style="text-align:center; padding:10px 5px; cursor:pointer;"><?php echo $invoice->total_paid; ?></td>
														</tr>
														<?php } ?>
													</tbody>
												</table>
											</div>
										</div>
											{{Form::text('invoice_id', isset($schedule->invoice_id)?$schedule->invoice_id:null,array('class' => 'form-control invoice-id', 'readonly' => 'readonly', 'style' => 'cursor:pointer;'))}}
									</div>
								</div>	
								<div class="form-group">
									<label class="control-label col-lg-2">Debt collector</label>
									<div class="col-lg-10">
									<div class="popUp debtCollector-data" style="display:none;">
										<div class="debtCollecotr-data-table" style="padding:20px;">
											<table>
												<thead>
													<tr>
														<th style="padding:20px; background-color:#e1e1e1; text-align:center;">Id</th>
														<th style="padding:20px; background-color:#e1e1e1; text-align:center;">Name</th>
													</tr>
												</thead>
												<tbody>
													<?php foreach($debtCollectors as $debtCollector){ ?>
													<tr>
														<td class="debtCollector-id" style="text-align:center; padding:10px 5px; cursor:pointer;"><?php echo $debtCollector->id; ?></td>
														<td class="debtCollector-name" style="text-align:center; padding:10px 5px; cursor:pointer;"><?php echo $debtCollector->first_name; ?></td>
													</tr>
													<?php } ?>
												</tbody>
											</table>
										</div>
									</div>
									<input type="hidden" name="debtCollector_id" class="debtCollector-id" <?php if(isset($schedule->debt_collector)){?>value="<?php echo $schedule->debt_collector;?>"<?php } ?>>
									
									{{Form::text('debtCollector_name', isset($schedule->debtCollector->first_name)?$schedule->debtCollector->first_name.' '.$schedule->debtCollector->last_name:null,array('class' => 'form-control debtCollector-name', 'readonly' => 'readonly', 'style' => 'cursor:pointer;'))}}
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-2">Visit date</label>
									<div class="col-md-10">
										{{Form::date('visit_date',isset($schedule->visit_date)?$schedule->visit_date:null, array('class' => 'form-control visit-date', 'min' => date('Y-m-d')))}}
									</div>
								</div>
								<div class="text-right">
									<a class="btn btn-primary add">Submit <i class="icon-arrow-right14 position-right"></i></a>
								</div>
						</fieldset>
{{ Form::close() }}