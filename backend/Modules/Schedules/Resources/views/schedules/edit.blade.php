@extends('layouts.app')
@section('header');
@parent
<link rel="stylesheet" type="text/css" href="<?php echo Module::asset('schedules:css/colorbox.css')?>">
<script type="text/javascript" src="<?php echo Module::asset('schedules:js/jquery.colorbox-min.js'); ?>"></script>

<script>
jQuery('html').on('focus','input.invoice-id',function(e){
	var html = jQuery('.popUp.invoice-data').html();
	jQuery.colorbox({
		html:html,
		fixed:true,
		maxHeight:'70%',
		onComplete: function(){
			jQuery('#cboxContent table').on('click','tr',function(e){
				e.stopPropagation();
				var invoiceId = jQuery(this).children('td.invoice-id').text();
				jQuery('input.invoice-id').val(invoiceId);
				jQuery.colorbox.close()
			});
		}
	});
});

jQuery('html').on('focus','input.debtCollector-name',function(e){
	var html = jQuery('.popUp.debtCollector-data').html();
	jQuery.colorbox({
		html:html,
		fixed:true,
		maxHeight:'70%',
		onComplete: function(){
			jQuery('#cboxContent table').on('click','tr',function(e){
				e.stopPropagation();
				var debtCollectorId = jQuery(this).children('td.debtCollector-id').text();
				var debtCollectorName = jQuery(this).children('td.debtCollector-name').text();
				jQuery('input.debtCollector-id').val(debtCollectorId);
				jQuery('input.debtCollector-name').val(debtCollectorName);
				jQuery.colorbox.close()
			});
		}
	});
});
</script>


<script>
jQuery(document).ready(function($){
	$('html').on('click','a.add',function(e){
		var visitDate = $('form.addSchedule input.visit-date').val();
		if(visitDate){
			$('form.addSchedule').submit();
		}else{
			alert('please insert visit visit date');
		}
	});
});
</script>

@endsection
@section('content')
<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Schedules</h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="{{route('dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="{{route('customers.index')}}">Schedules</a></li>
							<li class="active">Edit</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->
				<div class="content">
				<div class="panel panel-flat">
				<div class="panel-body">
					@include('schedules::schedules._form', ['action' => array('\Modules\Schedules\Http\Controllers\SchedulesController@update',$schedule->id), 'method' => 'PUT'])
				</div>
				</div>
				</div>
@endsection