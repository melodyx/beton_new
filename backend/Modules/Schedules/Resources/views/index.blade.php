@extends('layouts.app')
@section('header')

@parent
	
	<?php $success_message = session('success'); if($success_message){?>
		<script>
		jQuery(document).ready(function(){
			alert('<?php echo $success_message; ?>');
		});
		</script>
	<?php session()->forget('success'); } ?>
	
@endsection
@section('content')
				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Schedules</span></h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="{{route('dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
							<li class="active">Schedules</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->
				<div class="content">
					<div class="panel panel-flat">
					<table class="table datatable-basic">
					<thead>
						<tr>
							<th style="text-align:center;" >Customer Id</th>
							<th style="text-align:center;" >Customer Name</th>
							<th style="text-align:center;" >Debt Collector Id</th>
							<th style="text-align:center;" >Debt Collector Name</th>
							<th style="text-align:center;" >Visit Date</th>
							<th style="text-align:center;" >Status</th>
							<th style="text-align:center;" >Action</th>
						</tr>
					</thead>
					<tbody>	
						<?php if(isset($schedules)){ ?>
							<?php foreach($schedules as $schedule){?>
							<tr>
								<td style="text-align:center;" ><?php if(isset($schedule->invoice->customer_id)){echo $schedule->invoice->customer_id;}else{ echo '-';}?></td>
								<td style="text-align:center;" ><?php if(isset($schedule->invoice->customer->name)){echo $schedule->invoice->customer->name;}else{ echo '-';}?></td>
								<td style="text-align:center;" ><?php if($schedule->debtCollector->id){echo $schedule->debtCollector->id;}else{ echo '-';}?></td>
								<td style="text-align:center;" ><?php if($schedule->debtCollector->first_name){echo $schedule->debtCollector->first_name;}else{ echo '-';}?></td>
								<td style="text-align:center;" ><?php if($schedule->visit_date){echo $schedule->visit_date;}else{ echo '-';};?></td>
								<td style="text-align:center;" class="status"><?php if($schedule->status){echo $schedule->status;}else{ echo '-';}?></td>
								<td>
									<ul class="icons-list">
										<li >
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">
												<i class="icon-cog7"></i>
												<span class="caret"></span>
											</a>
											<ul class="dropdown-menu dropdown-menu-right">
												<li class="dropdown-header">Options</li>
													<li><a href="{{route('schedules.edit',$schedule->id)}}"><i class="icon-pencil7"></i>Edit entry</a></li>
													<li><a href="{{ route('schedules.destroy', $schedule->id)}}" data-method="delete" data-token="{{ csrf_token() }}"><i class="icon-bin"></i>Remove entry</a></li>
											</ul>
										</li>
									</ul>
								</td>
							</tr>
							<?php } ?>
						<?php } ?>
					</tbody>
					</table>	
					</div>
				</div>

@endsection