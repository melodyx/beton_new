<?php $user = \Sentinel::getUser(); ?>
<?php //if($user == \Sentinel::inRole('superadmin') || $user == \Sentinel::inRole('admin')){?>
<?php if ($user->hasAccess(['schedules']) || $user->hasAccess('admin.login')){ ?>
<li>
	<a><i class="icon-users"></i> <span>Schedules</span></a>
	<ul>
			<li><a href="{{route('schedules.index')}}">Schedules view</a></li>
			<li><a href="{{route('schedules.create')}}">Add Schedule</a></li>
	</ul>
</li>
<?php } ?>
