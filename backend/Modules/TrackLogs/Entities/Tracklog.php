<?php

namespace Modules\TrackLogs\Entities;

use Illuminate\Database\Eloquent\Model;

class Tracklog extends Model
{
    protected $fillable = [];
    protected $table = 'track_logs';

    /*public function schedule(){
    	return $this->belongsTo('Modules\Schedules\Entities\Schedule','schedule_id');
    }*/

    public function user(){
    	return $this->belongsTo('Modules\Users\Entities\User','user_id');
    }
}
