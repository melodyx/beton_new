<?php

namespace Modules\TrackLogs\Http\Controllers;

use Nwidart\Modules\Routing\Controller;
use Modules\TrackLogs\Entities\Tracklog;
use Modules\Users\Entities\User;
use Modules\Schedules\Entities\Schedule;
use Illuminate\Http\Request;

class TrackLogsController extends Controller {

	public function index()
	{

		//Checking role for access customer page
		$current_user_role = $this->checkRole();
		if($current_user_role == 'notLogin' || $current_user_role == 'customer'){
			return redirect()->route('login')->with('success','you dont have any authorization for access this page');
		}
		//=====================================
		$current_date = date('Y-m-d');
		$trackLogsData = Tracklog::with('user')->whereDate('created_at','=', $current_date)->groupBy('user_id')->get();
		
		$trackLogs = $trackLogsData;
		/*$trackLogs = array();
		foreach($trackLogsData as $row){
			if( $schedule = Schedule::where('user_id','=',$row->user_id)->where('visit_date','=', $current_date)->first()){
				array_push($trackLogs, $row);
			}
		}*/
		//dd($trackLogs);
		return view('tracklogs::index', ['trackLogs' => $trackLogs,'date' => $current_date]);
	}

	public function searchDate(Request $request){
		$current_user_role = $this->checkRole();
		if($current_user_role == 'notLogin' || $current_user_role == 'customer'){
			return redirect()->route('login')->with('success','you dont have any authorization for access this page');
		}
		//=====================================
		$data = $request->all();
		$date = date('Y-m-d', strtotime($data['date']));
		$trackLogsData = Tracklog::with('user')->whereDate('created_at','=', $date)->groupBy('user_id')->get();
		$trackLogs = $trackLogsData;
		/*$trackLogs = array();
		foreach($trackLogsData as $row){
			if( $schedule = Schedule::where('sales_id','=',$row->sales_id)->where('visit_date','=', $date)->first()){
				array_push($trackLogs, $row);
			}
		}*/
		//dd($trackLogs);
		return view('tracklogs::index', ['trackLogs' => $trackLogs,'date' => $date]);
	}

	public function ajaxDetails(Request $request){
		$data = $request->all();
		$arrayDum = array();
		/*$arrayDum['schedule'] = Schedule::with('customer.customerDetail')->where('sales_id','=', $data['sales_id'])->where('visit_date','=',$data['date'])->get();*/

		$trackLogs = Tracklog::where('user_id','=', $data['user_id'])->whereDate('created_at','=', $data['date'])->get();

		$x = 0;

		foreach($trackLogs as $trackLog){
			//dd(json_decode($trackLog->latlng));
			$latlng = json_decode($trackLog->latlng);
			//dd($latlng);
			$arrayDum['latlng'][$x]['lat'] = $latlng->coords->latitude;
			$arrayDum['latlng'][$x]['lng'] = $latlng->coords->longitude;
			$x++;
		}
		//dd($arrayDum['latlng']);
		return $arrayDum;
	}

	public function checkRole(){
		$user = \Sentinel::getUser();
		if($user){
            $current_user['superadmin'] = \Sentinel::inRole('superadmin');
            $current_user['admin'] = \Sentinel::inRole('admin');
            $current_user['sales'] = \Sentinel::inRole('sales');
            $current_user['gm'] = \Sentinel::inRole('gm');
            $current_user['bm'] = \Sentinel::inRole('bm');
            $current_user['debtCollector'] = \Sentinel::inRole('debtCollector');
            $current_user['customer'] = \Sentinel::inRole('customer');

			if($current_user['superadmin'] == true){
				return 'superadmin';
			}elseif($current_user['admin'] == true){
				return 'admin';
			}elseif($current_user['sales'] == true){
				return 'sales';
			}elseif($current_user['customer'] == true){
				return 'customer';
			}
		}else{
			return 'notLogin';
		}
	}

}
