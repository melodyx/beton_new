<?php

namespace Modules\Tracklogs\Http\Controllers;

use Nwidart\Modules\Routing\Controller;
use Chrisbjr\ApiGuard\Http\Controllers\ApiGuardController;
use Modules\Users\Entities\User;
use Modules\Customers\Entities\CustomersDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Modules\Schedules\Entities\Schedule;
use Modules\TrackLogs\Entities\Tracklog;
use Validator;
use Sentinel;
use Reminder, Mail;

class ApiController extends ApiGuardController {

    /**
     * @SWG\Post(
     *   path="/trackLogs/saveLatLng",
     *   summary="Saving lat lng coordinate for track logs",
     *   description="-",
     *   tags={"TrackLogs"},
     *   @SWG\Parameter(
     *       name="user_id",
     *       in="formData",
     *       description="debtcollector id for saving in track logs",
     *       required=true,
     *       type="integer",
     *       format="integer, sales id",
     *       @SWG\Items(type="integer"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="tracklogs",
     *       in="formData",
     *       description="json tracklogs for lat and lng in array",
     *       required=true,
     *       type="string",
     *       format="string, tracklogs",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="True / False after submit",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Error Message",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   )
     * )
     */
    public function saveLatLng(Request $request){
    	$response = array('status' => 'error', 'code' => 400, 'data' => false);
        try{
        	$data = $request->all();
    		$newtracklog = new Tracklog();
    		$newtracklog->user_id = $data['user_id'];
    		$newtracklog->latlng = $data['tracklogs'];
    		$newtracklog->save();

    			$response['status'] = 'success';
                $response['code'] = '200';
                $response['data'] = 'success';
            
        }catch(\Exception $e) {
            $response = array('status' => 'error', 'code' => 500, 'data' => $e->getMessage());
        }

        return Response::json($response);
    }

    
}
