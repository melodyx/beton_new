<?php

Route::group(['middleware' => 'web', 'namespace' => 'Modules\TrackLogs\Http\Controllers'], function()
{
	Route::group(['middleware' => ['apiAuth'], 'prefix' => 'api/v1'], function() {
        Route::post('/trackLogs/saveLatLng', 'ApiController@saveLatLng');
    });

    Route::get('trackLogs','TrackLogsController@index')->name('tracklogs.index');

    Route::post('tracklogs/searchDate','TrackLogsController@searchDate')->name('tracklogs.searchDate');
    Route::post('tracklogs/ajaxDetails', 'TrackLogsController@ajaxDetails')->name('tracklogs.ajaxDetails');

});