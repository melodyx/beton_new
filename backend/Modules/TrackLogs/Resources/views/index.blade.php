@extends('layouts.app')
@section('header')

    @parent
	<script type="text/javascript" src="<?php echo asset('assets/js/plugins/forms/styling/uniform.min.js') ?>"></script>
	<script type="text/javascript" src="<?php echo asset('assets/js/pages/form_layouts.js') ?>"></script>

	<?php $success_message = session('success'); if($success_message){?>
		<script>
		jQuery(document).ready(function(){
			alert('<?php echo $success_message; ?>');
		});
		</script>
	<?php session()->forget('success'); } ?>

	<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBdmLJ80f1HUSAG0YxQthnZ2hd_p60nnT4&v=3.exp"></script>

	<script type="text/javascript" src="{{asset('assets/js/maps/google/basic/basic.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/js/maps/google/basic/geolocation.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/js/maps/google/basic/coordinates.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/js/maps/google/basic/click_event.js')}}"></script>

	<link rel="stylesheet" type="text/css" href="<?php echo Module::asset('tracklogs:css/colorbox.css')?>">
	<script type="text/javascript" src="<?php echo Module::asset('tracklogs:js/jquery.colorbox-min.js'); ?>"></script>
	
	<style>
		.clear{clear:both;}
		ul.list-sales{width:100%; display:block; margin:0; padding:0; list-style:none;}
		ul.list-sales>li{margin:0 20px 20px 0; padding:15px; width:calc(28% - 50px); height: 340px; display:inline-block; border:1px solid #e1e1e1; position:relative; float:left;}
		ul.list-sales>li:nth-child(4n){margin-right:0;}
		ul.list-sales>li .feat-image{width:100%; height:200px; display:inline-block; margin-bottom:15px;}
		ul.list-sales>li h4{font-size:14px; font-weight:400; margin-bottom:15px; text-align:center;}
		ul.list-sales>li .price{font-size:14px; color:#000; text-align:center;}
		ul.list-sales>li a.link-details{font-size:14px; background-color:#1cbbb4; color:#fff; text-align:center; position:absolute; bottom:0; left:0; width:100%; padding:14px 0; display:inline-block;}
		ul.list-sales>li a.remove{padding:0 5px; background-color:#1cbbb4; position:absolute; right:5px; top:5px; color:#fff; line-height:19px; border-radius:15px;}
		ul.list-sales>li a.remove:hover{background-color:#fa0000;}
	</style>


	<script>
		jQuery(document).ready(function($){
			$('ul.list-sales>li').on('click','a.link-details', function(e){
			var userId = $(this).data('user');
			var schedule;
			//console.log(salesId);
				// Setup map

			$.ajax({
				url:'<?php echo route("tracklogs.ajaxDetails"); ?>',
				type:'post',
				data:{
					user_id: userId,
					date : '<?php echo $date; ?>'
				}
			}).done(function(data){
				//console.log(data);
				$.colorbox({
					html:'<div class="map-container map-marker-simple"></div>',
					width: '70%',
					height: '80%',
					fixed:true,
					onComplete: function(){
						initialize(data);
					}
				});
			});
		
			function initialize(arrayDum) {
				// console.log(arrayDum);
				
				// Set coordinates
				var myLatlng = new google.maps.LatLng(arrayDum.latlng[0].lat, arrayDum.latlng[0].lng);

				// Options
				var mapOptions = {
					zoom: 15,
					center: myLatlng
				};

				// Apply options
				var map = new google.maps.Map($('.map-marker-simple')[0], mapOptions);
				var contentString = 'Surabaya';
				
				

				// Add info window
				var infowindow = new google.maps.InfoWindow({
					content: contentString
				});

				// Add marker
				var marker
				$.each(arrayDum.latlng,function(key,value){
					//console.log(value);
					marker = new google.maps.Marker({
					position: new google.maps.LatLng(value.lat, value.lng),
					map: map,
					title: 'Hello World!'
					});
				});

				var lineCoordinates = Array();
				var x = 1;
		          $.each(arrayDum.latlng,function(key,value){
		          	// console.log(value.lat);
		          	// console.log(value.lng);
		          	// if(x == 1){
			          	marker = new google.maps.Marker({
						position: new google.maps.LatLng(value.lat, value.lng),
						map: map,
						title: 'User'
						});
					// }
		          	lineCoordinates.push(
		  			{lat: parseFloat(value.lat), lng: parseFloat(value.lng)});
		  			x++;
		          });
		      

		        var linePath = new google.maps.Polyline({
		          path: lineCoordinates,
		          geodesic: true,
		          strokeColor: '#FF0000',
		          strokeOpacity: 1.0,
		          strokeWeight: 2
		        });

				linePath.setMap(map);

				// Attach click event
				google.maps.event.addListener(marker, 'click', function() {
					infowindow.open(map);
				});
				
				
				


				function placeMarker(location) {
					marker.setMap();
					marker = new google.maps.Marker({
					position: location, 
					map: map
				   });
				}
				 
			};

				/*$.colorbox({
					html:'<div class="map-container map-marker-simple"></div>',
					width: '70%',
					height: '80%',
					fixed:true,
					onComplete: function(){
						initialize();
					}
				});*/
				
			});
		});
	</script>
@endsection
@section('content')
				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Datatables</span> - Track Logs</h4>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="{{route('dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
							<li class="active">Track Logs</li>
						</ul>

					</div>
				</div>
				<!-- /page header -->
				<div class="content">
					<div class="row">
						<div class="panel panel-flat">
							<div class="panel-body">
								<legend>Periode time</legend>
								{{Form::open(array('method'=>'post', 'action' => '\Modules\TrackLogs\Http\Controllers\TrackLogsController@searchDate'))}}
								<div class="form-group">
									<label class="control-label col-lg-2">Date</label>
									<div class="col-lg-10">
										<input class="form-control" type="date" name="date" value='{{$date}}'>
									</div>								
								</div>
								<div class="clear"></div>
								<div class="form-group">
									<input type='submit' value='search' class='btn btn-primary'>
								</div>
								{{Form::close()}}
							</div>
						</div>
					</div>
					<div class="row">
						<div class="panel panel-flat">
							<div class="panel-body">
								<ul class="list-sales">
									<?php foreach($trackLogs as $trackLog){ ?>
									<li>
										<?php /*<div class="feat-image" style="background:url({{$trackLog->sales->salesDetail[0]->image_path}})no-repeat center / 100%"></div>*/?>
										<h4>{{$trackLog->user->first_name}}</h4>
										<a class="link-details" data-user="{{$trackLog->user->id}}">Show Track Logs</a>
									</li>
									<?php } ?>
								</ul>
							</div>
						</div>
					</div>
				</div>

@endsection