<?php $user = \Sentinel::getUser(); ?>
<?php //if($user == \Sentinel::inRole('superadmin') || $user == \Sentinel::inRole('admin')){ ?>
<?php if ($user->hasAccess(['trackLogs']) || $user->hasAccess('admin.login')){ ?>
<li>
	<a><i class="icon-users"></i> <span>Track Logs</span></a>
	<ul>
			<li><a href="{{route('tracklogs.index')}}">View track logs</a></li>
	</ul>
</li>
<?php } ?>
