@extends('layouts.app')
@section('header')

    @parent
	
	<?php $success_message = session('success'); if($success_message){?>
		<script>
		jQuery(document).ready(function(){
			alert('<?php echo $success_message; ?>');
		});
		</script>
	<?php session()->forget('success'); } ?>
	
	<style>
		.clear{clear:both;}
	</style>

@endsection
@section('content')
				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Datatables</span> - Orders</h4>
						</div>

					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="{{route('dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
							<li class="active" href="{{route('transactions.index')}}">Orders</li>
							<li class="active">Detail</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->
				<div class="content">
					<div class="row">
						<div class="panel panel-flat">
							<div class="panel-body">
								<legend>Info</legend>
								<div class="form-group">
									<label class="control-label col-lg-2">ID</label>
									<div class="col-lg-10">
										: <?php echo $order->id ?>
									</div>
									<div class="clear"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Sales Name</label>
									<div class="col-lg-10">
										: <?php echo $user->first_name.' '.$user->last_name ?>
									</div>
									<div class="clear"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Order Date</label>
									<div class="col-lg-10">
										: <?php echo date('d F Y G:i:s', strtotime($order->created_at)) ?>
									</div>
									<div class="clear"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Shipping Date</label>
									<div class="col-lg-10">
										:  <?php echo ( (strtotime($order->shipping_date) && strtotime($order->shipping_date)>0) ?date('d F Y G:i:s', strtotime($order->shipping_date)):'  -  '); ?>
									</div>
									<div class="clear"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Memo</label>
									<div class="col-lg-10">
										: <?php echo $order->memo ?>
									</div>
									<div class="clear"></div>
								</div>
							</div>
						</div>
					
						<div class="panel panel-flat">
							<div class="panel-body">
								<?php if(isset($order->billing_address) && $order->billing_address){?>
									<legend>Billing Data</legend>
									<div class="form-group">
										<label class="control-label col-lg-2">Name</label>
										<div class="col-lg-10">
											: <?php echo $order->billing_destination_name;?>
										</div>
										<div class="clear"></div>
									</div>
									<div class="form-group">
										<label class="control-label col-lg-2">Email</label>
										<div class="col-lg-10">
											: <?php echo $order->billing_email;?>
										</div>
										<div class="clear"></div>
									</div>
									<div class="form-group">
										<label class="control-label col-lg-2">Phone</label>
										<div class="col-lg-10">
											: <?php echo $order->billing_phone;?>
										</div>
										<div class="clear"></div>
									</div>
									<div class="form-group">
										<label class="control-label col-lg-2">Address</label>
										<div class="col-lg-10">
											: <?php echo $order->billing_address;?>
										</div>
										<div class="clear"></div>
									</div>
									<div class="form-group">
										<label class="control-label col-lg-2">City</label>
										<div class="col-lg-10">
											: <?php echo $order->billing_city;?>
										</div>
										<div class="clear"></div>
									</div>
									<div class="form-group">
										<label class="control-label col-lg-2">State</label>
										<div class="col-lg-10">
											: <?php echo $order->billing_state;?>
										</div>
										<div class="clear"></div>
									</div>
									<div class="form-group">
										<label class="control-label col-lg-2">Country</label>
										<div class="col-lg-10">
											: <?php echo $order->billing_country;?>
										</div>
										<div class="clear"></div>
									</div>
									<div class="form-group">
										<label class="control-label col-lg-2">Zip Code</label>
										<div class="col-lg-10">
											: <?php echo $order->billing_zip_code;?>
										</div>
										<div class="clear"></div>
									</div>
								<?php } ?>
							</div>
						</div>
								
						<div class="panel panel-flat">
							<div class="panel-body">	
								<legend>Shipping Data</legend>
								<div class="form-group">
									<label class="control-label col-lg-2">Destination Name</label>
									<div class="col-lg-10">
										: <?php echo $order->destination_name;?>
									</div>
									<div class="clear"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Email</label>
									<div class="col-lg-10">
										: <?php echo $order->email;?>
									</div>
									<div class="clear"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Phone</label>
									<div class="col-lg-10">
										: <?php echo $order->phone;?>
									</div>
									<div class="clear"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Shipping Address</label>
									<div class="col-lg-10">
										: <?php echo $order->address;?>
									</div>
									<div class="clear"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">City</label>
									<div class="col-lg-10">
										: <?php echo $order->city;?>
									</div>
									<div class="clear"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">State</label>
									<div class="col-lg-10">
										: <?php echo $order->state;?>
									</div>
									<div class="clear"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Country</label>
									<div class="col-lg-10">
										: <?php echo $order->country;?>
									</div>
									<div class="clear"></div>
								</div>
								<div class="form-group">
									<label class="control-label col-lg-2">Zip COde</label>
									<div class="col-lg-10">
										: <?php echo $order->zip_code;?>
									</div>
									<div class="clear"></div>
								</div>
							</div>
						</div>
								
						<div class="panel panel-flat">
							<div class="panel-body">
								<legend>Item Orders</legend>
								<table class="table datatable-basic">
									<thead>
										<tr>
											<th>Items</th>
											<th>Qty</th>
											<th>Price(Rp)</th>
											<th>Total(Rp)</th>
										</tr>
									</thead>
									<tbody>
										<?php $total_price = 0; foreach($order->orderDetails as $orderDetail){?>
										<tr>
											<td><?php echo $orderDetail->product->name ?></td>
											<td><?php echo $orderDetail->product_qty ?></td>
											<td>
											<?php 
												$total_price = $total_price + ($orderDetail->price * $orderDetail->product_qty); 
												echo number_format($orderDetail->price,2);
											?>
											</td>
											<td>
												<?php
												echo number_format($orderDetail->price * $orderDetail->product_qty,2);
												?>
											</td>
										</tr>
										<?php } ?>
									</tbody>
									<tfoot>
										<tr>
											<td></td>
											<td></td>
											<td style="text-align:right;"><b>Subtotal</b></td>
											<td><?php echo number_format((float)$total_price,2); ?></td>
										</tr>
										<tr>
											<td></td>
											<td></td>
											<td style="text-align:right;"><b>Disc</b></td>
											<td><?php echo number_format((float)$order->customer->defaultdisc,2); ?></td>
										</tr>
										<tr>
											<td></td>
											<td></td>
											<td style="text-align:right;"><b>Total</b></td>
											<td><?php 
//											if($order->customer->defaultdisc){
												$disc = (float) $order->customer->defaultdisc;
												$total_all = (float)$total_price - $disc;

												echo number_format((float)$total_all,2);
//											}
											 ?>
											</td>
										</tr>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
					@if($order->status == \Modules\Transactions\Entities\Order::INPROGRESS)
						<button id="detail-confirm-order" type="submit" class="btn btn-primary" data-link="{{route('transactions.orderConfirmation',$order->id)}}"><i class="icon-pencil7"></i>Confirm Order </button>
					@endif
					<!--<div class="row">
						<div class="panel panel-flat">
							<div class="panel-body">
								<legend></legend>
							</div>
						</div>
					</div>-->
				</div>

	<Script>
		$(document).ready(function(){
			$('.content').on('click','#detail-confirm-order', function(){
				window.location = $(this).data('link');
			});
		})
	</Script>
@endsection