@extends('layouts.app')
@section('header')

    @parent
	
	<?php $success_message = session('success'); if($success_message){?>
		<script>
		jQuery(document).ready(function(){
			alert('<?php echo $success_message; ?>');
		});
		</script>
	<?php session()->forget('success'); } ?>

	<script>
		jQuery(document).ready(function(){
//			jQuery('#table-header-created').trigger('click');
//			jQuery('#table-header-created').trigger('click');
		});
	</script>
@endsection
@section('content')
				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Datatables</span> - Orders</h4>
						</div>

					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="{{route('dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
							<li class="active">Orders</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->
				<div class="content">
					<div class="row">
						<div class="panel panel-flat">
						<table class="table datatable-basic-order">
						<thead>
							<tr>
								<th style="text-align:center;">Id</th>
								<th style="text-align:center;">Customer name</th>
								<th style="text-align:center;">Destination name</th>
								<th style="text-align:center;">Shipping Address</th>
								<th style="text-align:center;">Email</th>
								<th style="text-align:center;">Phone</th>
								<th style="text-align:center;">Status</th>
								<th style="text-align:center;" id="table-header-created">Created</th>
								<th style="text-align:center;">Action</th>
							</tr>
						</thead>
						<tbody>	
							<?php if(isset($orders)){ ?>
								<?php foreach($orders as $order){?>
								<tr>
									<td style="text-align:center;"><?php if($order->id){echo $order->id;}else{ echo '-'; } ?></td>
									<td style="text-align:center;"><?php if($order->customer->name){echo $order->customer->name;}else{ echo '-'; } ?></td>
									<td style="text-align:center;"><?php if($order->destination_name){echo $order->destination_name;}else{ echo '-'; } ?></td>
									<td style="text-align:center;"><?php if(isset($order->address)){echo $order->address;}else{ echo '-'; } ?></td>
									<td style="text-align:center;"><?php if(isset($order->email)){echo $order->email;}else{ echo '-'; } ?></td>
									<td style="text-align:center;"><?php if(isset($order->phone)){echo $order->phone;}else{ echo '-'; } ?></td>
									<td style="text-align:center;"><?php if(isset($order->status)){echo $order->status;}else{ echo '-'; } ?></td>
									<td style="text-align:center;"><?php if(isset($order->created_at)){echo date('Y-m-d',strtotime($order->created_at)).'<br/>'.date('H:i:s',strtotime($order->created_at));}else{ echo '-'; } ?></td>
									<td>
										<ul class="icons-list">
											<li >
												<a href="#" class="dropdown-toggle" data-toggle="dropdown">
													<i class="icon-cog7"></i>
													<span class="caret"></span>
												</a>
												<ul class="dropdown-menu dropdown-menu-right">
													<li class="dropdown-header">Options</li>
													<li><a href="{{route('transactions.show',$order->id)}}"><i class="icon-pencil7"></i>Details</a></li>
													<?php if($order->status == \Modules\Transactions\Entities\Order::DONE){?>
													<li><a href="{{route('transactions.makeInvoice',$order->id)}}"><i class="icon-pencil7"></i>Make Invoice</a></li>
													<?php }else{ ?>
													<li><a href="{{route('transactions.orderConfirmation',$order->id)}}"><i class="icon-pencil7"></i>Confirm Order</a></li>
													<?php } ?>
												</ul>
											</li>
										</ul>
									</td>
								</tr>
								<?php } ?>
							<?php } ?>
						</tbody>
						</table>	
						</div>
					</div>
				</div>

@endsection