<?php

Route::group(['middleware' => 'web', 'namespace' => 'Modules\Transactions\Http\Controllers'], function()
{
	//Route::get('/', 'TransactionsController@index');
	Route::group(['middleware' => ['apiAuth'], 'prefix' => 'api/v1'], function() {
		Route::get('/transactions/getAll', 'ApiController@getAll');
		Route::get('/transactions/getAllInDebtOrder', 'ApiController@getAllInDebtOrder');
		Route::get('/transactions/checkOrderCustomers', 'ApiController@checkOrderCustomers');
		Route::get('/transactions/orderData','ApiController@orderData');
		Route::post('/transactions/registerOrder','ApiController@registerOrder');
		Route::post('/transactions/registerOrderDetails','ApiController@registerOrderDetails');
		Route::post('/transactions/confirmOrder','ApiController@confirmOrder');
    });

	Route::get('transactions/makeInvoice/{id}','TransactionsController@makeInvoice')->name('transactions.makeInvoice');
	Route::get('transactions/orderConfirmation/{id}','TransactionsController@orderConfirmation')->name('transactions.orderConfirmation');
	Route::resource('transactions','TransactionsController');
});