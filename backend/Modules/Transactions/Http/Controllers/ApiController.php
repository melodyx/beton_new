<?php

namespace Modules\Transactions\Http\Controllers;

use League\Flysystem\Exception;
use Nwidart\Modules\Routing\Controller;
use Chrisbjr\ApiGuard\Http\Controllers\ApiGuardController;
use Modules\Users\Entities\User;
use Modules\Customers\Entities\Customer;
use Modules\Customers\Entities\CustomerAddress;
use Modules\Expeditions\Entities\Expedition;
use Modules\Products\Entities\Product;
use Modules\Transactions\Entities\Order;
use Modules\Transactions\Entities\OrderDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Validator;
use Sentinel;
use Reminder, Mail;

class ApiController extends ApiGuardController {


    /**
     * @SWG\Get(
     *   path="/transactions/getAll",
     *   summary="List of all customer with discount",
     *   description="-",
     *   tags={"transactions"},
     *   @SWG\Response(
     *     response=200,
     *     description="True / False after submit",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Error Message",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   )
     * )
     */
    public function getAll(){
        try{

            $response = array('status' => 'error', 'code' => 400, 'data' => false);

            $orders = Order::all();

            if(isset($orders)){
                $response['status'] = 'success';
                $response['code'] = '200';
                $response['data'] = $orders;
            }else{
                $response['data'] = 'no customer data';
            }
            
        }catch(\Exception $e) {
            $response = array('status' => 'error', 'code' => 500, 'data' => $e->getMessage());
        }

        return Response::json($response);
    }

    /**
     * @SWG\Get(
     *   path="/transactions/getAllInDebtOrder",
     *   summary="List of all customer with discount",
     *   description="-",
     *   tags={"transactions"},
     *   @SWG\Response(
     *     response=200,
     *     description="True / False after submit",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Error Message",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   )
     * )
     */
    public function getAllInDebtOrder(){
        try{

            $response = array('status' => 'error', 'code' => 400, 'data' => false);

            $orders = Order::where('status','=',Order::INPROGRESS)->get();

            if(isset($orders)){
                $response['status'] = 'success';
                $response['code'] = '200';
                $response['data'] = $orders;
            }else{
                $response['data'] = 'no customer in debt';
            }
            
        }catch(\Exception $e) {
            $response = array('status' => 'error', 'code' => 500, 'data' => $e->getMessage());
        }

        return Response::json($response);
    }

     /**
     * @SWG\get(
     *   path="/transactions/checkOrderCustomers",
     *   summary="Checking customer order in status in progress",
     *   description="-",
     *   tags={"transactions"},
     *   @SWG\Parameter(
     *       name="customer_id",
     *       in="query",
     *       description="Customer id for change",
     *       required=true,
     *       type="integer",
     *       format="integer, sales id",
     *       @SWG\Items(type="integer"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="True / False after submit",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Error Message",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   )
     * )
     */
     public function checkOrderCustomers(Request $request){
        \DB::beginTransaction();
        try{
            $data = $request->all();
            $response = array('status' => 'error', 'code' => 400, 'data' => false);
            $rules = array(
                'customer_id'     => 'numeric|required'
            );

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                $response['data'] = $validator->errors()->all();
            } else {
               
                $orders = Order::where('customer_id','=',$data['customer_id'])->where('status','=',Order::INORDER)->get();
                if(isset($orders)){
                $response['status'] = 'success';
                $response['code'] = '200';
                $response['data'] = $orders;
                }else{
                    $response['data'] = 'this customer dont have order in progress';
                }

            }            

            
        }catch(\Exception $e) {
            \DB::rollback();
            $response = array('status' => 'error', 'code' => 500, 'data' => $e->getMessage());
        }
        \DB::commit();

        return Response::json($response);
     }

     /**
     * @SWG\get(
     *   path="/transactions/orderData",
     *   summary="Checking customer order in status in progress",
     *   description="-",
     *   tags={"transactions"},
     *   @SWG\Parameter(
     *       name="order_id",
     *       in="query",
     *       description="order id for getting order data",
     *       required=true,
     *       type="integer",
     *       format="integer, sales id",
     *       @SWG\Items(type="integer"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="True / False after submit",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Error Message",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   )
     * )
     */
     public function orderData(Request $request){
        \DB::beginTransaction();
        try{
            $data = $request->all();
            $response = array('status' => 'error', 'code' => 400, 'data' => false);
            $rules = array(
                'order_id'     => 'numeric|required'
            );

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                $response['data'] = $validator->errors()->all();
            } else {
               
                $order = Order::with('orderDetails.product','customer')->find($data['order_id']);
                if(isset($order)){
                $response['status'] = 'success';
                $response['code'] = '200';
                $response['data'] = $order;
                }else{
                    $response['data'] = 'this customer dont have order in progress';
                }

            }            

            
        }catch(\Exception $e) {
            \DB::rollback();
            $response = array('status' => 'error', 'code' => 500, 'data' => $e->getMessage());
        }
        \DB::commit();

        return Response::json($response);
     }

     /**
     * @SWG\post(
     *   path="/transactions/registerOrder",
     *   summary="Checking customer order in status in progress",
     *   description="-",
     *   tags={"transactions"},
     *   @SWG\Parameter(
     *       name="author_id",
     *       in="query",
     *       description="Author id for orders",
     *       required=true,
     *       type="integer",
     *       format="integer, author id",
     *       @SWG\Items(type="integer"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="customer_id",
     *       in="query",
     *       description="Customer id for billing data",
     *       required=true,
     *       type="integer",
     *       format="integer, sales id",
     *       @SWG\Items(type="integer"),
     *       collectionFormat="multi"
     *   ),
      *     @SWG\Parameter(
      *       name="customer_shipping_id",
      *       in="query",
      *       description="Customer Shipping id for shipping data",
      *       required=true,
      *       type="integer",
      *       format="integer, sales id",
      *       @SWG\Items(type="integer"),
      *       collectionFormat="multi"
      *   ),
     *   @SWG\Parameter(
     *       name="destination_name",
     *       in="query",
     *       description="Name of customer in shipping address",
     *       required=false,
     *       type="string",
     *       format="string, destination name",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="email",
     *       in="query",
     *       description="Email of customer in shipping address",
     *       required=false,
     *       type="string",
     *       format="string, destination name",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="phone",
     *       in="query",
     *       description="Phone number of customer in shipping address",
     *       required=false,
     *       type="string",
     *       format="string, phone number",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
	 *   @SWG\Parameter(
     *       name="shipping_date",
     *       in="query",
     *       description="Shipping date",
     *       required=false,
     *       type="string",
     *       format="string, shipping date",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="shipping_address",
     *       in="query",
     *       description="Shipping address",
     *       required=false,
     *       type="string",
     *       format="string, shipping address",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="city",
     *       in="query",
     *       description="City of customer in shipping address",
     *       required=false,
     *       type="string",
     *       format="string, city",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="state",
     *       in="query",
     *       description="State of customer in shipping address",
     *       required=false,
     *       type="string",
     *       format="string, state",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="country",
     *       in="query",
     *       description="Country of customer in shipping address",
     *       required=false,
     *       type="string",
     *       format="string, country",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="zip_code",
     *       in="query",
     *       description="Zip code of customer in shipping address",
     *       required=false,
     *       type="integer",
     *       format="string, zip code",
     *       @SWG\Items(type="integer"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="billing_destination_name",
     *       in="query",
     *       description="Name of customer in shipping address",
     *       required=false,
     *       type="string",
     *       format="string, destination name",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="billing_email",
     *       in="query",
     *       description="Email of customer in shipping address",
     *       required=false,
     *       type="string",
     *       format="string, destination name",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="billing_phone",
     *       in="query",
     *       description="Phone number of customer in shipping address",
     *       required=false,
     *       type="string",
     *       format="string, phone number",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="billing_address",
     *       in="query",
     *       description="Shipping address",
     *       required=false,
     *       type="string",
     *       format="string, shipping address",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="billing_city",
     *       in="query",
     *       description="City of customer in shipping address",
     *       required=false,
     *       type="string",
     *       format="string, city",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="billing_state",
     *       in="query",
     *       description="State of customer in shipping address",
     *       required=false,
     *       type="string",
     *       format="string, state",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="billing_country",
     *       in="query",
     *       description="Country of customer in shipping address",
     *       required=false,
     *       type="string",
     *       format="string, country",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="billing_zip_code",
     *       in="query",
     *       description="Zip code of customer in shipping address",
     *       required=false,
     *       type="integer",
     *       format="string, zip code",
     *       @SWG\Items(type="integer"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="due_date",
     *       in="query",
     *       description="Due date for order payment",
     *       required=false,
     *       type="string",
     *       format="string, zip code",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="memo",
     *       in="query",
     *       description="Zip code of customer in shipping address",
     *       required=false,
     *       type="string",
     *       format="string, zip code",
     *       @SWG\Items(type="string"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="True / False after submit",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Error Message",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   )
     * )
     */
     public function registerOrder(Request $request){
        \DB::beginTransaction();
        try{
            $data = $request->all();		   
            $response = array('status' => 'error', 'code' => 400, 'data' => false);
            $rules = array(
                'author_id' => 'numeric|required',
                'customer_id'     => 'numeric|required',
                'customer_shipping_id'     => 'numeric|required',
                'destination_name' => 'string',
                'email' => 'string',
                'phone' => 'string',
                'shipping_date' => 'string',
                'shipping_address' => 'string',
                'city' => 'string',
                'state' => 'string',
                'country' => 'string',
                'zip_code' => 'numeric',
                'billing_destination_name' => 'string',
                'billing_email' => 'string',
                'billing_phone' => 'string',
                'billing_address' => 'string',
                'billing_city' => 'string',
                'billing_state' => 'string',
                'billing_country' => 'string',
                'billing_zip_code' => 'numeric',
                'due_date' => 'string',
                'memo' => 'string'

            );

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                $response['data'] = $validator->errors()->all();
            } else {
                // Check if user is ACTIVE based on author_id
                $user = \Sentinel::findById($data['author_id']);
                if($user->status != User::ACTIVE)
                    throw new \Exception('Kamu tidak mempunyai ijin untuk melakukan hal ini, silahkan logout dan login kembali untuk mencoba lagi.');

                $order = new Order();
                $order->author_id = $data['author_id'];
                $order->customer_id = $data['customer_id'];
                if(isset($data['destination_name']) && isset($data['city'])){
                    $order->destination_name = $data['destination_name'];
                    $order->email = $data['email'];
                    $order->phone = $data['phone'];
                    $order->address = $data['shipping_address'];
                    $order->city = $data['city'];
                    $order->state = $data['state'];
                    $order->country = $data['country'];
                    $order->zip_code = $data['zip_code'];
                    $order->billing_destination_name = $data['billing_destination_name'];
                    $order->billing_email = $data['billing_email'];
                    $order->billing_phone = $data['billing_phone'];
                    $order->billing_address = $data['billing_address'];
                    $order->billing_city = $data['billing_city'];
                    $order->billing_state = $data['billing_state'];
                    $order->billing_country = $data['billing_country'];
                    $order->billing_zip_code = $data['billing_zip_code'];
                    $order->memo = $data['memo'];
                    $order->shipping_date = $data['shipping_date'];


                    /*$customerAddress = CustomerAddress::where('customer_id','=',$data['customer_id'])->where('address_type','=',CustomerAddress::SHIPPING)->first();
                    if(isset($customerAddress)){
                        $customerAddress->destination_name = $data['destination_name'];
                        $customerAddress->email = $data['email'];
                        $customerAddress->phone = $data['phone'];
                        $customerAddress->address = $data['shipping_address'];
                        $customerAddress->city = $data['city'];
                        $customerAddress->state = $data['state'];
                        $customerAddress->country = $data['country'];
                        $customerAddress->zip_code = $data['zip_code'];
                        $customerAddress->save();
                    }else{
                        $customerAddress = new CustomerAddress();
                        $customerAddress->customer_id = $data['customer_id'];
                        $customerAddress->destination_name = $data['destination_name'];
                        $customerAddress->email = $data['email'];
                        $customerAddress->phone = $data['phone'];
                        $customerAddress->address = $data['shipping_address'];
                        $customerAddress->city = $data['city'];
                        $customerAddress->state = $data['state'];
                        $customerAddress->country = $data['country'];
                        $customerAddress->zip_code = $data['zip_code'];
                        $customerAddress->address_type = CustomerAddress::SHIPPING;
                        $customerAddress->save();
                    }*/
                    
                }else{
                    $customer = Customer::with(array('customerAddresses'=>function ($query){
						$query->where('address_type','=',CustomerAddress::BILLING);
					}))->find($data['customer_id']);
					
                    /*$customerAddress = CustomerAddress::where('customer_id','=',$data['customer_id'])->where('address_type','=',CustomerAddress::SHIPPING)->first();
                    if(isset($customerAddress)){
                        $order->destination_name = $customerAddress->destination_name;
                        $order->email = $customerAddress->email;
                        $order->phone = $customerAddress->phone;
                        $order->address = $customerAddress->address;
                        $order->city = $customerAddress->city;
                        $order->state = $customerAddress->state;
                        $order->country = $customerAddress->country;
                        $order->zip_code = $customerAddress->zip_code;
                        
                    }else{*/
                        $order->destination_name = $customer->customerAddresses[0]->destination_name;
                        $order->email = $customer->customerAddresses[0]->email;
                        $order->phone = $customer->customerAddresses[0]->phone;
                        $order->address = $customer->customerAddresses[0]->address;
                        $order->city = $customer->customerAddresses[0]->city;
                        $order->state = $customer->customerAddresses[0]->state;
                        $order->country = $customer->customerAddresses[0]->country;
                        $order->zip_code = $customer->customerAddresses[0]->zip_code;
                        $order->billing_destination_name = $customer->customerAddresses[0]->destination_name;
                        $order->billing_email = $customer->customerAddresses[0]->email;
                        $order->billing_phone = $customer->customerAddresses[0]->phone;
                        $order->billing_address = $customer->customerAddresses[0]->address;
                        $order->billing_city = $customer->customerAddresses[0]->city;
                        $order->billing_state = $customer->customerAddresses[0]->state;
                        $order->billing_country = $customer->customerAddresses[0]->country;
                        $order->billing_zip_code = $customer->customerAddresses[0]->zip_code;

                        /*$customerAddress = new CustomerAddress();
                        $customerAddress->customer_id = $data['customer_id'];
                        $customerAddress->destination_name = $customer->name;
                        $customerAddress->email = $customer->email;
                        $customerAddress->phone = $customer->phone;
                        $customerAddress->address = $customer->addressline1;
                        $customerAddress->city = $customer->city;
                        $customerAddress->state = $customer->stateprov;
                        $customerAddress->country = $customer->country;
                        $customerAddress->zip_code = $customer->zipcode;
                        $customerAddress->address_type = CustomerAddress::SHIPPING;*/
                        // $response['data'] = $customerAddress;
                        // return Response::json($response);
                        //$customerAddress->save();
                    //}
                   
                }

                $customerAddress = CustomerAddress::where('customer_id','=',$data['customer_id'])->where('ref_id','=',$data['customer_shipping_id'])->first();

                if(empty($customerAddress)){
                    $customerAddress = new CustomerAddress();
                    $customerAddress->customer_id = $data['customer_id'];
                    $customerAddress->ref_id = $data['customer_shipping_id'];
                    $customerAddress->save();
                }

                $order->status = Order::INPROGRESS;

                $expedition = Expedition::where('destination','=',$order->city)->first();

                if(isset($expedition)){
                    $order->minimal_price = $expedition->dilivery_price;
                    $order->expedition_id = $expedition->id;
                }
                if(isset($data['due_date'])){
                    $order->due_date = date('Y-m-d', strtotime($data['due_date']));
                }
                
                $order->save();


				// initialize message
				$msg = '
Order #'.$order->id.' sudah direquest. 

Billing Info:
Destination: '.$order->billing_destination_name.'
Email: '.$order->billing_email.'
Phone: '.$order->billing_phone.'
Address: '.$order->billing_address.'
City: '.$order->billing_city.'
State: '.$order->billing_state.'
Country: '.$order->billing_country.'
Zip Code: '.$order->billing_zip_code.'

Shipping Info:
Destination: '.$order->destination_name.'
Email: '.$order->email.'
Phone: '.$order->phone.'
Address: '.$order->address.'
City: '.$order->city.'
State: '.$order->state.'
Country: '.$order->country.'
Zip Code: '.$order->zip_code.'

Additional Info:
Shipping date: '.$order->shipping_date.'
Memo: '.$order->memo.'

Produk:
';
				
                // add order detail with product id
                if(isset($data['product'])){
                    foreach($data['product'] as $product){
                        $orderDetail = new OrderDetail();
                        $orderDetail->order_id = $order->id;
                        $orderDetail->product_id = $product['product_id'];
                        $orderDetail->product_qty = (float)$product['quantity'];
                        $orderDetail->price = $product['price'];
                        $orderDetail->status = OrderDetail::INCART;
                        $orderDetail->save();
						
						// get product data
						$pr = Product::find($product['product_id']);
						$msg = $msg. $pr->name.' : '.(float)$product['quantity']. '(m3/sak) : IDR'. $product['price']. '
';
                    }
                }

                // email sales for order confirm
                $sales = User::find($order->author_id);
				$msg = $msg . '
Silahkan tunggu approval dari pihak admin.';
				Mail::raw($msg, function($message) use ($sales, $order) {
                    $message->from('hello@eyesimple.us', 'Eyesimple');
                    $message->to($sales->email, $sales->first_name)->subject('[Mercusuar Beton] Waiting for Approval Order #'.$order->id);
                });

                $response['status'] = 'success';
                $response['code'] = 200;
                $response['data'] = $order;
            }            
        }catch(\Exception $e) {
            \DB::rollback();
            $response = array('status' => 'error', 'code' => 500, 'data' => $e->getMessage());
        }
        \DB::commit();

        return Response::json($response);
     }

     /**
     * @SWG\post(
     *   path="/transactions/registerOrderDetails",
     *   summary="register order details",
     *   description="-",
     *   tags={"transactions"},
     *   @SWG\Parameter(
     *       name="order_id",
     *       in="query",
     *       description="Order id generated from order",
     *       required=true,
     *       type="integer",
     *       format="integer, order id",
     *       @SWG\Items(type="integer"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="True / False after submit",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Error Message",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   )
     * )
     */
     public function registerOrderDetails(Request $request){
        \DB::beginTransaction();
        try{
            $data = $request->all();
            $response = array('status' => 'error', 'code' => 400, 'data' => false);
            $rules = array(
                'order_id'     => 'numeric|required'
            );

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                $response['data'] = $validator->errors()->all();
            } else {
                foreach($data['product'] as $product){
                    $orderDetail = new OrderDetail();
                    $orderDetail->order_id = $data['order_id'];
                    $orderDetail->product_id = $product['product_id'];
                    $orderDetail->product_qty = $product['quantity'];
                    $orderDetail->price = $product['price'];
                    $orderDetail->status = OrderDetail::INCART;
                    $orderDetail->save();
                }

                $response['status'] = 'success';
                $response['code'] = 200;
                $response['data'] = $orderDetail;
            }            
        }catch(\Exception $e) {
            \DB::rollback();
            $response = array('status' => 'error', 'code' => 500, 'data' => $e->getMessage());
        }
        \DB::commit();

        return Response::json($response);
     }

     /**
     * @SWG\post(
     *   path="/transactions/deleteOrderDetails",
     *   summary="register order details",
     *   description="-",
     *   tags={"transactions"},
     *   @SWG\Parameter(
     *       name="order_id",
     *       in="query",
     *       description="Order id generated from order",
     *       required=true,
     *       type="integer",
     *       format="integer, order id",
     *       @SWG\Items(type="integer"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Parameter(
     *       name="product_id",
     *       in="query",
     *       description="Order Product id",
     *       required=true,
     *       type="integer",
     *       format="integer, product id",
     *       @SWG\Items(type="integer"),
     *       collectionFormat="multi"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="True / False after submit",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Error Message",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   )
     * )
     */
     public function deleteOrderDetails(Request $request){
        \DB::beginTransaction();
        try{
            $data = $request->all();
            $response = array('status' => 'error', 'code' => 400, 'data' => false);
            $rules = array(
                'order_id'     => 'numeric|required',
                'product_id' => 'numeric|required'

            );

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                $response['data'] = $validator->errors()->all();
            } else {
                $orderDetail = OrderDetail::where('order_id','=',$data['order_id'])->where('product_id','=',$data['product_id']);
                $orderDetail->delete();

                $response['status'] = 'success';
                $response['code'] = 200;
                $response['data'] = 'succesfully delete order';
            }            
        }catch(\Exception $e) {
            \DB::rollback();
            $response = array('status' => 'error', 'code' => 500, 'data' => $e->getMessage());
        }
        \DB::commit();

        return Response::json($response);
     }
    
  /**
     * @SWG\post(
     *   path="/transactions/confirmOrder",
     *   summary="Confirm order in status in progress",
     *   description="-",
     *   tags={"transactions"},
     *   @SWG\Parameter(
     *       name="order_id",
     *       in="query",
     *       description="Order id generated from order",
     *       required=true,
     *       type="integer",
     *       format="integer, order id",
     *       @SWG\Items(type="integer"),
     *       collectionFormat="multi"
     *  ),
     *   @SWG\Response(
     *     response=200,
     *     description="True / False after submit",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   ),
     *   @SWG\Response(
     *     response="500",
     *     description="Error Message",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref="#/definitions/Customer")
     *     ),
     *   )
     * )
     */
     public function confirmOrder(Request $request){
        \DB::beginTransaction();
        try{
            $data = $request->all();
            $response = array('status' => 'error', 'code' => 400, 'data' => false);
            $rules = array(
                'order_id'     => 'numeric|required'
            );

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {
                $response['data'] = $validator->errors()->all();
            } else {
                
                $order = Order::with('orderDetails.product','customer')->find($data['order_id']);
                $order->status = Order::INPROGRESS;
                foreach($order->orderDetails as $orderDetail){
                    $orderDetail->status = OrderDetail::INPROGRESS;
                    $orderDetail->save();
                }
                $order->save();

                $response['status'] = 'success';
                $response['code'] = 200;
                $response['data'] = $order;
            }            
        }catch(\Exception $e) {
            \DB::rollback();
            $response = array('status' => 'error', 'code' => 500, 'data' => $e->getMessage());
        }
        \DB::commit();

        return Response::json($response);
     }

    
}
