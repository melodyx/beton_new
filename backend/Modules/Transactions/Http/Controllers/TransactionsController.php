<?php

namespace Modules\Transactions\Http\Controllers;

use Nwidart\Modules\Routing\Controller;
use Modules\Invoices\Entities\Invoice;
use Modules\Schedules\Entities\Schedule;
use Modules\Transactions\Entities\Order;
use Modules\Transactions\Entities\OrderDetail;
use Modules\Customers\Entities\Customer;

class TransactionsController extends Controller {

	public function index()
	{
		$orders = Order::with('customer')->get();
		return view('transactions::index',['orders' => $orders]);
	}

	public function show($id){
		$order = Order::with('customer.customerAddresses','orderDetails.product')->find($id);
		$user = \App\User::find($order->author_id);
		//dd($order);
		return view('transactions::transactions.details',['order'=>$order, 'user' => $user]);
	}

	public function makeInvoice($id){
		//Checking role for access customer page
		$current_user_role = $this->checkRole();
		if($current_user_role == 'notLogin'){
			return redirect()->route('login')->with('success','you dont have any authorization for access this page');
		}
		//=====================================

		$customers = Customer::all();
		$order = Order::with('customer','orderDetails')->find($id);
		$totalDebt = 0;
		foreach($order->orderDetails as $orderDetail){
			$price = $orderDetail->product_qty * $orderDetail->price;
			$totalDebt = $totalDebt + $price;
		}
		//dd($totalDebt);
		//dd($order);
		return view('invoices::invoices.create',['customers' => $customers,'order' => $order,'totalDebt'=>$totalDebt]);
	}

	public function orderConfirmation($id){
		 \DB::beginTransaction();
        try{
  
            $order = Order::with('orderDetails.product','customer')->find($id);
            $order->status = Order::DONE;
            foreach($order->orderDetails as $orderDetail){
                $orderDetail->status = OrderDetail::DONE;
                $orderDetail->save();
            }
            $order->save();

            // PUSH NOTIFICATION
            $notification = [
                'title' => 'Konfirmasi pesanan #'.$order->id,
                'body'  => 'Pesananan untuk pelanggan '.$order->customer->name.' telah disetujui',
            ];
            $data = [
                'title' => 'Konfirmasi pesanan #'.$order->id,
                'subtitle' => 'Konfirmasi pesanan pelanggan',
                'message' => 'Pesananan untuk pelanggan '.$order->customer->name.' telah disetujui',
                'data'  => [
                    'order' => $order = Order::find($id),
                ]
            ];
            $user = \Sentinel::findById($order->author_id);
            
            pushNotification($user->fcm_token, $notification['title'], $notification['body'], $data);
            // ===========================
                      
        }catch(\Exception $e) {
            \DB::rollback();
            return redirect()->route('transactions.index')->with('success',$e->getMessage());
        }
        \DB::commit();

        return redirect()->route('transactions.index')->with('success','successfully confirm order customer');
	}

	public function checkRole(){
		$user = \Sentinel::getUser();
		if($user){
			$current_user['superadmin'] = \Sentinel::inRole('superadmin');
			$current_user['admin'] = \Sentinel::inRole('admin');
			$current_user['sales'] = \Sentinel::inRole('sales');
            $current_user['gm'] = \Sentinel::inRole('gm');
            $current_user['bm'] = \Sentinel::inRole('bm');
            $current_user['debtCollector'] = \Sentinel::inRole('debtCollector');
			$current_user['customer'] = \Sentinel::inRole('customer');
			
			if($current_user['superadmin'] == true){
				return 'superadmin';
			}elseif($current_user['admin'] == true){
				return 'admin';
			}elseif($current_user['sales'] == true){
				return 'sales';
			}elseif($current_user['gm'] == true){
				return 'gm';
			}elseif($current_user['bm'] == true){
				return 'bm';
			}elseif($current_user['debtCollector'] == true){
				return 'debtCollector';
			}

		}else{
			return 'notLogin';
		}
	}

}
