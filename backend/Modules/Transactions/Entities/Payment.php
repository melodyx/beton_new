<?php

namespace Modules\Transactions\Entities;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [];

    /*public function order(){
    	return $this->belongsTo('\Modules\Transactions\Entities\Order','order_id');
    }*/

    public function invoice(){
    	return $this->belongsTo('\Modules\Invoices\Entities\Invoice','invoice_id');
    }

    public function schedule(){
    	return $this->belongsTo('\Modules\Schedules\Entities\Schedule','schedule_id');
    }
}
