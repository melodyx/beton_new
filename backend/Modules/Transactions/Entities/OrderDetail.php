<?php

namespace Modules\Transactions\Entities;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
	const INCART = 'in cart';
	const INPROGRESS = 'in progress';
    const DONE = 'done';
	const PAID ='paid';

    protected $fillable = [];
    protected $table = 'order_details';

    public function order(){
    	return $this->belongsTo('\Modules\Transactions\Entities\Order','order_id');
    }

    public function product(){
    	return $this->belongsTo('\Modules\Products\Entities\Product','product_id');
    }
}
