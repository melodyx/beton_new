<?php

namespace Modules\Transactions\Entities;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	const INORDER = 'in order';
	const INPROGRESS = 'in progress';
    const DONE = 'done';
	const PAID = 'paid';

    protected $fillable = [];

    public function orderDetails(){
    	return $this->hasMany('\Modules\Transactions\Entities\OrderDetail','order_id');
    }

    public function customer(){
    	return $this->belongsTo('\Modules\Customers\Entities\Customer','customer_id');
    }

    public function author(){
        return $this->belongsTo('\Modules\Users\Entities\User','author_id');
    }

    /*public function payments(){
    	return $this->hasMany('\Modules\Transactions\Entities\Payment','order_id');
    }*/

    public function invoices(){
        return $this->hasMany('\Modules\Invoices\Entities\Invoice','order_id');
    }

    public function expedition(){
        return $this->belongsTo('\Modules\Expeditions\Entities\Expedition','expedition_id');
    }
}
