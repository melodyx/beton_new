<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableFieldOrders extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        Schema::table('orders', function(Blueprint $table)
        {
            if (!Schema::hasColumn('orders', 'billing_destination_name'))
            {
                $table->string('billing_destination_name',255);
            }

            if (!Schema::hasColumn('orders', 'billing_email'))
            {
                $table->string('billing_email',255);
            }

            if (!Schema::hasColumn('orders', 'billing_phone'))
            {
                $table->string('billing_phone',255);
            }

            if (!Schema::hasColumn('orders', 'billing_address'))
            {
                $table->string('billing_address',255);
            }

            if (!Schema::hasColumn('orders', 'billing_city'))
            {
                $table->string('billing_city',255);
            }

            if (!Schema::hasColumn('orders', 'billing_state'))
            {
                $table->string('billing_state',255);
            }

            if (!Schema::hasColumn('orders', 'billing_country'))
            {
                $table->string('billing_country',255);
            }

            if (!Schema::hasColumn('orders', 'billing_zip_code'))
            {
                $table->integer('billing_zip_code');
            }

            if (!Schema::hasColumn('orders', 'memo'))
            {
                $table->longText('memo');
            }


                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function(Blueprint $table)
        {
            if (Schema::hasColumn('users', 'billing_destination_name'))
            {
                $table->dropColumn('billing_destination_name');
            }

        });
    }

}
