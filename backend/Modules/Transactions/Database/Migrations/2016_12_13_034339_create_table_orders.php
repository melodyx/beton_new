<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOrders extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('customer_id');
            $table->integer('expedition_id');
            $table->string('destination_name',255);
            $table->string('email',255);
            $table->string('phone',255);
            $table->string('address',255);
            $table->string('city',255);
            $table->string('state',255);
            $table->string('country',255);
            $table->integer('zip_code');
            $table->string('billing_destination_name',255);
            $table->string('billing_email',255);
            $table->string('billing_phone',255);
            $table->string('billing_address',255);
            $table->string('billing_city',255);
            $table->string('billing_state',255);
            $table->string('billing_country',255);
            $table->integer('billing_zip_code');
            $table->decimal('minimal_price',14,2);
            $table->date('due_date');
            $table->integer('author_id');
            $table->string('status',255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }

}
