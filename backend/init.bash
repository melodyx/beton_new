# Copy environment example and generate application key
cp .env.example .env; php artisan key:generate

# To generate API Documentation
./vendor/bin/swagger app/ -o storage/docs/api-docs.json