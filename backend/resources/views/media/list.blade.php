<script>
	$(document).ready(function(){
		$('body').on('click', '.addimg', function(e){
			e.preventDefault();
			$(this).parents('.thumb').toggleClass('active');
		});

		$('body').on('click', '#useimg', function(e){
			e.preventDefault();
			$('#'+media_field).val(null);
			var valueExist = false;
			$('.thumb.active').each(function(){
				var id = $(this).find('img').attr('data-id');
				if($('#'+media_field).val()){
					$('#'+media_field).val(id+','+$('#'+media_field).val());
				}else{
					$('#'+media_field).val(id);
				}
				valueExist = true;
			});
			if(!valueExist){
				$('#'+media_field).val(null);
			}

			$('#'+media_field).trigger('change');
		});
	});
</script>

<div class="modal-header bg-primary">
	<button type="button" class="close" data-dismiss="modal">×</button>
	<h5 class="modal-title">Media Library</h5>
</div>

<div class="modal-body" style="background: #f5f5f5;">
	<div class="row">
		<?php
			foreach($medias as $media){
		?>
		<div class="col-lg-3 col-sm-6">
			<div class="thumbnail">
				<div class="thumb" style="background: url(<?php echo url($media->guid) ?>) no-repeat;background-size:cover;height: 300px;">
					<img data-id="{{url($media->guid)}}" src="<?php echo url($media->guid) ?>" alt="" style="display: none;">
					<div class="caption-overflow">
						<span>
							<a href="#" title="Add Image" data-popup="lightbox" class="btn border-white text-white btn-flat btn-icon btn-rounded addimg"><i class="icon-plus3"></i></a>
						</span>
					</div>
				</div>

				<div class="caption">
					<h6 class="no-margin">
						<a href="#" class="text-default"><?php echo $media->name ?></a> 
						<!-- <a href="#" class="text-muted"><i class="icon-three-bars pull-right"></i></a> -->
					</h6>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</div>

<div class="modal-footer">
	<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
	<button type="button" id="useimg" data-dismiss="modal" class="btn btn-primary">Use Image(s)</button>
</div>