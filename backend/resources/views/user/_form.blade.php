<?php
	if(!isset($method)){
		$method = 'post';
	}
?>

{{ Form::open(array('action' => $action, 'method' => $method, 'class' => 'form-horizontal')) }}
	<fieldset class="content-group">
		<legend class="text-bold">General Info</legend>
		<?php /*<div class="form-group">
			{{ Form::label('avatar', 'Avatar', array('class' => 'control-label col-lg-2')) }}
			<div class="col-lg-10">
				@include('media._popup', ['target_id' => 'mypopup'])
				<a href="{{ route('media.list') }}" class="btn btn-default btn-sm" data-toggle="modal" data-type="medialib" data-field="avatarimg" data-target="#mypopup">Upload Avatar</i></a>
				{{ Form::hidden('avatar', '', array('id' => 'avatarimg')) }}
			</div>
		</div>*/ ?>
		<div class="form-group">
			{{ Form::label('first_name', 'First Name', array('class' => 'control-label col-lg-2')) }}
			<div class="col-lg-10">
				{{ Form::text('first_name', isset($user->first_name)?$user->first_name:'', array('class' => 'form-control')) }}
			</div>
		</div>
		<div class="form-group">
			{{ Form::label('last_name', 'Last Name', array('class' => 'control-label col-lg-2')) }}
			<div class="col-lg-10">
				{{ Form::text('last_name', isset($user->last_name)?$user->last_name:'', array('class' => 'form-control')) }}
			</div>
		</div>
		<div class="form-group">
			{{ Form::label('email', 'Email', array('class' => 'control-label col-lg-2')) }}
			<div class="col-lg-10">
				{{ Form::email('email', isset($user->email)?$user->email:'', array('class' => 'form-control')) }}
			</div>
		</div>
		<div class="form-group">
			{{ Form::label('password', 'Password', array('class' => 'control-label col-lg-2')) }}
			<div class="col-lg-10">
				{{ Form::text('password', '', array('class' => 'form-control')) }}
			</div>
		</div>
		<div class="form-group">
			{{ Form::label('role', 'Role', array('class' => 'control-label col-lg-2')) }}
			<div class="col-lg-10">
				<?php
					$arr = array(
						'Status' => array()
					);
					foreach($roles as $role){
						$arr['Status'][$role->slug] = $role->name;
					}
				?>
					{{ Form::select('role', $arr, isset($currentRole)?$currentRole:'', array('class' => 'form-control')) }}

			</div>
		</div>
		<div class="form-group">
			{{ Form::label('status', 'Status', array('class' => 'control-label col-lg-2')) }}
			<div class="col-lg-10">
				<?php
					$arr = array(
						'User' => array(
							\App\User::ACTIVE => \App\User::ACTIVE,
							\App\User::PENDING => \App\User::PENDING,
							\App\User::BANNED => \App\User::BANNED,
							\App\User::SUSPENDED => \App\User::SUSPENDED,
						)
					);
				?>
				{{ Form::select('status', $arr,  isset($user->status)?$user->status:'', array('class' => 'form-control')) }}
			</div>
		</div>
	</fieldset>

	<?php if(isset($currentRole)){?>
	<input type="hidden" name="old_role" value="<?php echo $currentRole?>">
	<?php } ?>
	<div class="text-right">
		<button type="submit" class="btn btn-primary">Submit <i class="icon-arrow-right14 position-right"></i></button>
	</div>
{{ Form::close() }}