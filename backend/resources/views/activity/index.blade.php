@extends('layouts.app')

@section('content')
<!-- Page header -->
<div class="page-header">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Activity</span> Log</h4>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="#"><i class="icon-home2 position-left"></i> Home</a></li>
			<li class="active">Activity Log</li>
		</ul>
	</div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">
	<div class="panel panel-flat">
		<table class="table datatable-basic">
			<thead>
				<tr>
					<th>Type</th>
					<th>Description</th>
					<th>Causer</th>
					<th>Created At</th>
					<th>Updated At</th>
					<th class="text-center">Actions</th>
				</tr>
			</thead>
			<tbody>
				<?php 
					foreach($activities as $activity){ 
					if(isset($activity->causer_type)){
				?>
				<tr>
					<td><?php echo $activity->log_name; ?></td>
					<td><?php echo $activity->description; ?></td>
					<td>
						<?php
						  eval('$type = '.$activity->causer_type.'::find('.$activity->causer_id.');');
						  $arr = $type->toArray();
						  reset($arr);
						  next($arr);
						  $val = key($arr);
						  eval('echo($type->'.$val.');');
						?>
					</td>
					<td><?php echo $activity->created_at; ?></td>
					<td><?php echo $activity->updated_at; ?></td>
					<td class="text-center">
					-
					<!--
						<ul class="icons-list">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-menu9"></i>
								</a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-file-pdf"></i> Export to .pdf</a></li>
									<li><a href="#"><i class="icon-file-excel"></i> Export to .csv</a></li>
									<li><a href="#"><i class="icon-file-word"></i> Export to .doc</a></li>
								</ul>
							</li>
						</ul>
						-->
					</td>
				</tr>
					<?php }} ?>
			</tbody>
		</table>
	</div>
</div>
<!-- /content area -->
@endsection
